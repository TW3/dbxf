

![DBXF Logo](../images/dbxf/dbxf.png)


# DBXF Documentation

###  Overview:

This document aims to record some of the more notable events occurring during DBXF's development.

### Quick links:
* Grab the latest release from [here](https://tw3.gitlab.io/dbxf)
* Read the quick start guide [here](https://gitlab.com/TW3/dbxf/blob/master/docs/Quick%20start.pdf)

**[User Guide](https://gitlab.com/TW3/dbxf/blob/master/docs/User%20guide.pdf)**  |  **[Screenshots](https://gitlab.com/TW3/dbxf/blob/master/docs/Screenshots.pdf)**
::|::
**[FAQ](https://gitlab.com/TW3/dbxf/blob/master/docs/src/FAQ.md)**  |  **[Pixel Shader Reference](https://gitlab.com/TW3/dbxf/blob/master/docs/Pixel%20Shaders.pdf)**
**[Building DBXF](https://gitlab.com/TW3/dbxf/blob/master/docs/Building%20DBXF.pdf)**  |  **[Recommended Games](https://i2.wp.com/information2share.files.wordpress.com/2012/06/list-of-worthwhile-dos-games.jpg)**
**[Developer Guidelines](https://gitlab.com/TW3/dbxf/blob/staging/docs/src/Developer%20guidelines.md)**  |  [![Build status](https://ci.appveyor.com/api/projects/status/gnll1vuslxxdlwa0/branch/master?svg=true)](https://ci.appveyor.com/project/heydojo/dbxf/branch/master)

<div class="page-break"></div>

### News:

**15/10/2019:** DBXF website and a new release.

The **NEW!** DBXF website can be found at [https://tw3.gitlab.io/dbxf](https://tw3.gitlab.io/dbxf) as can the latest release archive.

Changes include:

- Built using Visual Studio 2019
- Lowered the default master volume to an acceptable level
- A few tiny updates to internals

Releases will no longer follow a numbering scheme and will instead be labelled using a tag containing the release date.

**22/04/2019:** DBXF 0.5.0 released!

**10/06/2018:** DBXF has migrated away from Github and has found a new home at **[Gitlab](https://gitlab.com/)**.

The new DBXF code repository and project page can be found [here](https://gitlab.com/TW3/dbxf).

**04/06/2018:** With immediate effect - **Development of DBXF on Github will cease**.

Microsoft have confirmed their [acquisition of Github](https://news.microsoft.com/2018/06/04/microsoft-to-acquire-github-for-7-5-billion/).
Based on Microsoft's track record, Github is no longer a suitable platform for DBXF's development.

**03/06/2018:** [DBXF v0.4.0](https://gitlab.com/TW3/dbxf/tags/0.4.0) released!

In the event that [early reports of Github's acquisition by Microsoft](https://news.ycombinator.com/item?id=17221527) turn out to be true, this will likely be the very last DBXF release made using Github **#movingtogitlab**

**18/05/2018:** DBXF v0.4.0 release delayed due to an issue beyond control.

**04/05/2018:** [DBXF v0.3.0](https://gitlab.com/TW3/dbxf/tags/0.3.0) is released!

**27/04/2018:** [DBXF v0.2.13](https://gitlab.com/TW3/dbxf/tags/0.2.13) is released!

**14/04/2018:** [Build documentation](https://gitlab.com/TW3/dbxf/blob/master/docs/Building%20DBXF.pdf) has been added.

**13/04/2018:** [DBXF v0.1](https://gitlab.com/TW3/dbxf/tags/0.1) is released!

**09/04/2018:** DBXF is forked from Dosbox-X https://github.com/heydojo/dbxf/commit/5a62d4a21f02e71affb68b794ed2d3d4e11beddd with an initial release to follow shortly. Commit history has been preserved for historical reasons.
