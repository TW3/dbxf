

![DBXF Logo](../images/dbxf/dbxf.png)


# DBXF Documentation

## Developer Guidelines

### Topics:

This document will cover the following :

* **Overview**
* **General info**
* **Coding conventions**
* **Git**
* **Documentation**
* **Testing**
* **Installation**
* **Features**
* **Submitting your changes to the project**
* **Automated builds**


###  Overview:

Patches are always welcome, merge requests should be proposed to the staging branch.
Assistance in any of the following areas of DBXF's development would be really neat:

* **Testing**
* **Features**
* **Hardware emulation implementation**
* **Software emulation implementation**
* **Games, applications and demoscene executables**
* **Retro development**
* **Bug fixes, patches, improvements and refinements**
* **Suggestions, ideas and general discussion**
* **Documentation**
* **Notes regarding games, applications, hacks and weird MS-DOS tricks, etc.**

<div class="page-break"></div>

### General info:

DBXF is largely written in C++ with some C. It is expected that you have at least a basic understanding of both of these programming languages before attempting to develop code for DBXF.
If you are inexperienced in either of these languages and you want to learn using DBXF, please feel free. However, your learning and understanding may benefit more by looking at simpler, more streamlined code.

*Please note that Dosbox-X and in turn, DBXF have a code base which is significantly diverged from the main dosbox project and patches which apply to dosbox's code will most likely not just "drop in"
and significant work may be required to get them into DBXF's tree. Please be mindful of this when submitting patches to the project.*

DBXF inherits some very old code from both DOSBox and Dosbox-X. Some very complex problems have been solved over the years within the code and the legacy of this fact is that rudimentary cleaning up of the code is a common task.
Any help streamlining and cleaning up the code is welcome.

Windows support, usability and "Just make it work®" are the main focuses of DBXF development.
Patches which deliberately degrade user experience in favour of emulation accuracy will be removed and or refused.

DBXF source code was originally hosted on github.com and has since moved.
The code is now hosted at [Gitlab.com][0624779d] and will remain so for the foreseeable future.

  [0624779d]: https://gitlab.com/ "Gitlab"

Unlike Dosbox-X (where it is supported,) PC98 support is experimental. May not work at all and is not the focus of the project. Issues marked against
anything PC98 related will be treated with the lowest possible priority unless part of a security vulnerability.

Compiling DBXF using sdl2 instead of the supplied custom fork of sdl1 is currently entirely untested and unsupported. Many key features are missing in DBXF's sdl2 implementation and there are no immediate plans to add them.

<div class="page-break"></div>

### Coding conventions:

C++ code in DBXF attempts to conform to the C++ 14 standard.

Although not strict, code committed to DBXF should at least follow some resemblance of the guidelines found here:

[https://google.github.io/styleguide/cppguide.html](https://google.github.io/styleguide/cppguide.html)

Microsoft specific methods and conventions should be avoided as they are considered bad practice. Please do not use them. And please. No [Hungarian notation](https://en.wikipedia.org/wiki/Hungarian_notation). Thank You.

When commenting code; please prefer :

``` // This is a comment. ``` over ``` /* This is also a comment. */ ```

The reason for preference over the first example is simple. Entire blocks of code can be commented out using the second example but only if that block of code does not contain any comments which also use a hash star escape.
Double slash comments are single line comments and are fine within hash starred comments. It is much better practice (and a good habit to get into;) to comment out code instead of deleting it whilst you are rewriting it.
Using double slashes to add a comment is much quicker than the alternative when using a keyboard.
Hash star comments are generally for large, multi line comment blocks which traditionally reside above the code they are associated with.

There is a fairly comprehensive set of guidelines [here](https://www.viva64.com/en/b/0391/) regarding good practices when writing C++ applications.

<div class="page-break"></div>

### Git:

It is outside the scope of this document to deliver a crash course on using git. Working on DBXF requires that you at least have a basic understanding of how to pull, push, commit, branch and merge using git. These fundamental basics are important to learn and for most tasks; you will rarely venture further than those basic commands during your time using git.

For quick reference, if your git foo is rusty please reference this [cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf) to refresh your skills.

To be able to work on the latest code, the very first thing you should do (after successfully following the Building DBXF document) is to switch to the staging branch.
All of the code which will go into the next version of DBXF belongs within staging. This ensures that there is always a current version of the source code available which successfully builds.

By using a staging tree, developers can propose code for testing before it is merged into the master tree and used to create future releases.
If you use Gitlab to propose a merge request of your code; please only propose merges into the staging branch. Merges proposed into master will be flat out rejected.
If the code is good enough, it will go from staging to master at the end of a release cycle. If your code is rejected or reverted, please don't give up. Revise your code and try again until it is accepted.

If you are interested in working on DBXF's latest code, you should fork the project on Gitlab and switch to the staging branch. Use the following git commands inside a folder which contains the DBXF source code to do so:

```
git remote add origin https://gitlab.com/YOURUSERNAME/YOURFORK
git pull
git checkout staging
```
Replacing **YOURUSERNAME** with your Gitlab username and **YOURFORK** with the name of your DBXF fork on Gitlab (likely dbxf.git)

<div class="page-break"></div>

You can then use git to push and pull to and from your own fork. This also enables you to propose merge requests into the DBXF staging branch using Gitlab.

The DBXF source code is available to download from the Gitlab project page.
If you want an archived copy for historical or reference reasons, visit the following link :
[https://gitlab.com/TW3/dbxf/-/archive/master/dbxf-master.zip](https://gitlab.com/TW3/dbxf/-/archive/master/dbxf-master.zip) using a web browser.
(For the most part, you likely won't need to do so if you have used git to pull your own copy of the tree.)

Please be as informative as you can when creating your git commit messages. A short brief summary, followed by a quick explanation of the change is ideal.

DBXF was forked at Dosbox-X commit eadabc720bdadb0cd89f546db54995689e690360
[https://github.com/joncampbell123/dosbox-x/commit/eadabc720bdadb0cd89f546db54995689e690360](https://github.com/joncampbell123/dosbox-x/commit/eadabc720bdadb0cd89f546db54995689e690360)

Cherry picking commits from Dosbox-X and proposing them to DBXF is fine. It is recommended that you try to improve on any code which you pull from Dosbox-X if at all possible.

If you want less than ten commits from Dosbox-X's tree, you can do the following:

From a command prompt, create a new directory and move into it.

```git init && git fetch https://github.com/joncampbell123/dosbox-x```

Move the HEAD to the commit holding the commit you want to pull.

```git merge eadabc720bdadb0cd89f546db54995689e690360```

(replace eadabc720bdadb0cd89f546db54995689e690360 with the hash of the commit you want to pull) Next, use git to create a patch file containing the commit.

```git format-patch -M -C master -1 -o patches```

Copy the newly created patch file from the folder named patches into the folder containing DBXF's source code and move into the directory using the command prompt.

```git apply --check some-patch-file.patch```

Use git apply to check that the patch will apply cleanly.

```git am --signoff < some-patch-file.patch```

Merge the patch into the DBXF tree by signing it off using git am.

Repeat the lasts steps from the git merge step onwards (instead using the hash of the next commit you want to pull) if there are more patches you want to pull.

Instead, if you want to merge a large number of commits:

Pull the source code into a newly created empty directory.

```git init && git pull https://github.com/joncampbell123/dosbox-x```

Use git to create patches from the oldest commit you want to merge, all the way through up the HEAD.

```git format-patch eadabc720bdadb0cd89f546db54995689e690360  -o ../patches```

Sort through the patches by looking on github and reviewing each one to see if they should be applied. Delete any which do not apply.

Copy the folder patches into the directory above the folder containing DBXF's source code and test that the patch applies.

```git apply --check ../patches/some-patch-file.patch```

If there is no feedback that the patch will not apply then apply the patch.

```git am --signoff < ../patches/some-patch-file.patch```

If there were no errors, delete the patch file. If there were errors, deal with them. Repeat the process for the next patch file until all the patch files have been processed.

If the patch does not apply, manually make the changes and then commit them before applying the next patch. Please try to preserve the commit's comment. Linking to the original commit on github is also helpful.

There is a handy script inside the folder vs2017\scripts\ named **PATCH-HELPER.CMD** which you can use to speed up and make the process easier.


### Documentation:

DBXF documentation is written using markdown which is finally parsed as pdf documents and shipped with release archives.
This is done because markdown is the easiest text based file format to work with. And pdf documents are the most suitable document format for off line reading.

Useful tools for writing documentation include [Atom](https://atom.io/) and [marktext](https://github.com/marktext/marktext). Both are suitable for working with markdown documents but any modern text editor will do due to the simplicity of the format.

Why not html?

Html creates a needlessly raised barrier of entry for content creators who just want to get on and create content.
Using html is less productive than markdown because of the large number of rules and conventions it includes.

By using markdown, we focus on the quality of the content and leave the work involved in creating layouts and js code to web developers.
Markdown can be easily parsed to html if so required but pdf is a much more suitable document format for the purposes of sharing documentation with users.
Gitlab natively supports presenting markdown documents as readable content, so there is no need to present html versions.

As an aside, many html documents available on line include abusive js files which track users' activity and perform other needless tasks. This erodes the trust of users.
When writing documentation, we are more interested in the fact that readers can trust the content and the format of the document than having a fancy layout or tracking a user's interaction with the document.

_Please note that DBXF documentation in pdf format will never include any js code. It is recommended that you completely disable the ability to run scripts by your pdf reader to avoid exploits from any pdf documents you may read from foreign sources. There are exactly zero justifiable reasons for adding js code to pdf documents (unless your skill level as a programmer is low, your moral compass is broken or you just don't understand the appropriate use of a file format. Sorry - These are the times we live in..)_

<div class="page-break"></div>

### Testing:

You can find a log of each build you create inside the .log files inside the vs2017 directory. It is recommended that you check these files for errors or warnings before you sign off on your final commit before proposing a merge into the staging branch. This ensures that your code meets or exceeds high quality standards.

DBXF has no tests, unit or otherwise.
If you are interested in implementing any tests, please feel free. Please ensure that they are not added to the build process and that they are optional.

Remember that if you change a step in the build process, clean the build first and then try to build (to avoid errors which could be caused by caching.)

The simplest test you can perform, is to just launch the application and ensure that it does not crash.
Beyond that, it is recommended that you test specifically the area of the application which you have been working on before submitting any related patches to the project.

A recommended test is to setup a __\RUN\DEBUG EXAMPLE.CMD__ which has a good test case for the changes you have made. By running the application or game using the debug example, a log is saved to the file log.txt. The file is useful for checking that the emulation is running correctly.

Testing helps prevents regressions. So the more testing that you do, the likelihood that the quality of your code consistently is of a high standard increases.

If you want to submit code to the project, please ensure that you have tested your changes to the best of your ability. If you require help testing, please open an issue on the bug tracker with a request.

### Installation:

There will never be an install program bundled with DBXF. Binary releases will be compressed to zip archive and all that is required to use DBXF is to download the archive, extract it and run DBXF.exe from within the extracted DBXF folder.

DBXF will not be wrapped using an installer because install packages have been abused by software developers for decades. Hence why Microsoft are driving their Windows store and the Windows S feature of Windows 10.
Unscrupulous software developers and third party distributors have been bundling viruses, spyware and other nasties within install binaries and installing them onto users' copies of Windows
without the user's consent and or knowledge for decades. Thus why we can't have nice things.
To add, many install routines do not correctly clean up when they are uninstalled. Further reducing trust by users.

<div class="page-break"></div>

### Features:

Code which adds new features to DBXF is always welcome (within reason) as long as it meets the following criteria:

* The code is well formatted, properly commented and compiles.

* The code does not touch or change the key mappings. The key maps are set in stone in DBXF and will never change. Never. Please do not submit code wanting
to change user input mappings. The last thing a user wants to do is setup custom key maps for games they want to play, only to have them break.
A user in the described situation would be completely in the right to never take an update again. And with good reason.
Please consider the key mappings a constant which has already been set. Proposed changes to the key mappings will 100% be refused.

* The code does not add any type of telemetry, usage statistics collection, adware, spyware or any other type of malware to the application or associated files. DBXF does not contain any malware and does not collect any data and then "phone home" with or without your consent. DBXF will never do any of these things. They are insipid.

* The code does not inflate the size of the source code and or the compiled binary files by a large amount.

* The code does not add a large number of dependencies to the build process (nobody wants to maintain other peoples code which belongs in a fork.)

* The copyright of the code belongs to you and that you have not simply copy and pasted existing code (proprietary or otherwise) of which you do not hold the copyright of as a submission. Submitting free or open source code which you do not have licence to is also not acceptable (although rare, some open source and free licenses are incompatible. If you are unsure, please ask or contact a lawyer.) Please read, understand and respect the GNU GPL V2 license which DBXF is released under.

<div class="page-break"></div>

### Submitting your changes to the project:

Please submit all merge requests into the staging branch.

Please try not to roll all of your changes into one single commit. Try to keep your commits short and sweet.

Please do not use profanity in comments. Programming can be a frustrating and unforgiving task. If you are angered enough to vent, please don't do it in the comments of your code. Stop. Take a breather, grab a cup of tea or coffee, pet your dog or cat (or reptile) and enjoy life. Life is too short to be mad all the time :)
Enjoy the time that you have and share it with the ones that you love the most.

If your code does not make it into the staging tree, please keep trying and respond positively to any feedback, constructive or otherwise.
If, eventually your code is flat out rejected for any reason, please feel free to create your own fork of the project.
And please do not take offence.
After all; this is how open source software development works.


### Automated builds:

DBXF now uses [Appveyor](https://www.appveyor.com/) to build release archives.
The main motivation for doing so is trust. Users of DBXF can trust that the binaries which they are downloading have been built by a trusted source and the build process is transparent. Appveyor provides build logs so that you can see how the binaries have been built. The following link provides the current build status of the code held within the master tree: [https://ci.appveyor.com/project/heydojo/dbxf/branch/master](https://ci.appveyor.com/project/heydojo/dbxf/branch/master).

The file appveyor.yml is used to control builds and it is recommended that you do not modify or delete this file. Doing so may render any code you propose as a merge request rejected.

There are no plans currently to use appveyor to handle continuous integration builds triggered by commits into the staging tree. Appveyor will only be used to build release archive artifacts.
