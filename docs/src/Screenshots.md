

![DBXF Logo](../images/dbxf/dbxf.png)


# DBXF Documentation

## Screenshots

###  Overview:

The following document contains screen capture images of DOS era video gaming history.

<div class="page-break"></div>

### Beneath a Steel Sky

![Beneath a Steel Sky screen capture](../images/screenshots/BASS.PNG)

Revolution Software's classic adventure game "[Beneath a Steel Sky](https://www.mobygames.com/game/beneath-a-steel-sky)"

<div class="page-break"></div>

![Beneath a Steel Sky screen capture A](../images/screenshots/BASS-A.PNG)

Set in a dystopian future, the player assumes the role of Robert Foster; a fugitive trying to escape from Union City.

<div class="page-break"></div>

![Beneath a Steel Sky screen capture B](../images/screenshots/BASS-B.PNG)

The game is considered a cult classic and is available to download for free on   [gog.com](https://www.gog.com/game/beneath_a_steel_sky).

<div class="page-break"></div>

### Ninja

![Ninja screen capture](../images/screenshots/NINJA.PNG)

Sculptured Software's beat 'em up title "[Ninja](https://www.mobygames.com/game/dos/ninja_)"

<div class="page-break"></div>

![Ninja screen capture A](../images/screenshots/NINJA-A.PNG)

The player controls a Ninja and the object of the game is to storm a fortress, defeating all enemies using punches, kicks, throwing stars and knives.

<div class="page-break"></div>

![Ninja screen capture B](../images/screenshots/NINJA-B.PNG)

Released in 1986 the game was one of the few PC titles which supported sixteen colours in Tandy graphics mode.

<div class="page-break"></div>

### Soccer Kid

![Soccer Kid screen capture](../images/screenshots/SOCCERKID.PNG)

Krisalis software's football themed platform game "[Soccer Kid](https://en.wikipedia.org/wiki/Soccer_Kid)"

<div class="page-break"></div>

![Soccer Kid screen capture A](../images/screenshots/SOCCERKID-A.PNG)

Saving the World Cup from evil aliens; Soccer Kid has a unique platform style and tricky to master control mechanics.

<div class="page-break"></div>

![Soccer Kid screen capture B](../images/screenshots/SOCCERKID-B.PNG)

The game was sponsored by Golden Grahams cereal and 500 copies were given away to customers in the UK.

<div class="page-break"></div>

### Xenon

![Xenon screen capture](../images/screenshots/XENON.PNG)

The Bitmap Brothers first video game; vertical scrolling shooter "[Xenon](https://www.mobygames.com/game/xenon)"

<div class="page-break"></div>

![Xenon screen capture A](../images/screenshots/XENON-A.PNG)

While controlling a tank which can transform into a fighter jet, the object of the game is to reach the end of each map without being destroyed. Defeated enemies can drop power ups which can change or increase fire power, speed or armour.

<div class="page-break"></div>

![Xenon screen capture B](../images/screenshots/XENON-B.PNG)

The DOS port of Xenon has no music. It was released before the first sound cards for PC systems became available. The Amiga port of [Xenon](http://www.bitmap-brothers.co.uk/our-games/past/xenon.htm) was extremely popular and praised especially for it's sound track.

<div class="page-break"></div>

### Tyrian

![Tyrian screen capture](../images/screenshots/TYRIAN.PNG)

Eclipse software's vertical scrolling space themed shooter "[Tyrian](https://www.mobygames.com/game/tyrian)"

<div class="page-break"></div>

![Tyrian screen capture A](../images/screenshots/TYRIAN-A.PNG)

With detailed pixel image sprites, a strategic element and a power up system, Tyrian is similar to another much loved classic "[Raptor: Call of the Shadows](https://3drealms.com/catalog/raptor_23/)."

<div class="page-break"></div>

![Tyrian screen capture B](../images/screenshots/TYRIAN-B.PNG)

The game is one of the few games of the era to offer net play and was officially released as freeware in 2004.

<div class="page-break"></div>

### The Legend of Kyrandia

![The Legend of Kyrandia screen capture A](../images/screenshots/KYRANDIA-A.PNG)

Westwood Studio's point and click adventure game "[The Legend of Kyrandia](https://www.mobygames.com/game/legend-of-kyrandia)"

<div class="page-break"></div>

![The Legend of Kyrandia screen capture](../images/screenshots/KYRANDIA.PNG)

The player assumes the role of Brandon who is thrust into a quest to save the land of Kyrandia when his Grandfather is turned to stone by an evil Jester named Malcom.

<div class="page-break"></div>

![The Legend of Kyrandia screen capture B](../images/screenshots/KYRANDIA-B.PNG)

The game is part of a trilogy and was originally given the title "Fables & Fiends."

<div class="page-break"></div>

### The Fool's Errand

![The Fool's Errand screen capture](../images/screenshots/THEFOOLSERRAND.PNG)

Miles Computing's puzzle game "[The Fool's Errand](https://www.mobygames.com/game/fools-errand)"

<div class="page-break"></div>

![The Fool's Errand screen capture A](../images/screenshots/THEFOOLSERRAND-A.PNG)

The game tells the story of the wandering fool in a picture book format. Each chapter expands into a puzzle which the player must first solve to advance the story.

<div class="page-break"></div>

![The Fool's Errand screen capture B](../images/screenshots/THEFOOLSERRAND-B.PNG)

The difficulty level of the puzzles can be fairly challenging and the game offers a certain charm which is hard to come by. The game is highly regarded by many and considered a classic.

<div class="page-break"></div>

### The Lost Vikings

![The Lost Vikings screen capture B](../images/screenshots/THELOSTVIKINGS-B.PNG)

Silicon & Synapse's platform strategy title "[The Lost Vikings](https://www.mobygames.com/game/lost-vikings)"

<div class="page-break"></div>

![The Lost Vikings screen capture A](../images/screenshots/THELOSTVIKINGS-A.PNG)

The player controls three vikings: Erik, Baleog and Olaf in this side scrolling platform adventure.

<div class="page-break"></div>

![The Lost Vikings screen capture](../images/screenshots/THELOSTVIKINGS.PNG)

Developer Silicon & Synapse later rebranded. They are now called [Blizzard Entertainment](https://www.blizzard.com/).

<div class="page-break"></div>

### Turrican 2

![Turrican 2 screen capture B](../images/screenshots/TURRICAN2-B.PNG)

Factor 5's arcade style platform shooter "[Turrican 2](https://www.mobygames.com/game/turrican-ii-the-final-fight)"

<div class="page-break"></div>

![Turrican 2 screen capture A](../images/screenshots/TURRICAN2-A.PNG)

This Metroid styled classic platformer is a tale of revenge. The player takes control of Bren McGuire - The last remaining survivor of The United Planets ship, Avalon 1. The player character is adorned with a Turrican fighting suit, must collect power ups and use the suit's abilities to progress through the game.

<div class="page-break"></div>

![Turrican 2 screen capture](../images/screenshots/TURRICAN2.PNG)

The game has a critically acclaimed musical score and was later rebranded as "Universal Soldier" when it was released for consoles.

<div class="page-break"></div>

### Soccer 2

![Soccer 2 screen capture](../images/screenshots/SOCCER2.PNG)

New Era Software's follow up to [The Soccer game](https://www.mobygames.com/game/dos/soccer-game) "Soccer 2"

<div class="page-break"></div>

![Soccer 2 screen capture A](../images/screenshots/SOCCER2-A.PNG)

As manager of a UK or European football team, the player's objective is to build a winning team without being sacked. This text based managerial simulator has a high level of detail and improves upon it's predecessor greatly.

<div class="page-break"></div>

![Soccer 2 screen capture B](../images/screenshots/SOCCER2-B.PNG)

The fictional European Super League is the top league in the game and winning it is just one of the accolades available.

<div class="page-break"></div>

### Dune 2

![Dune 2 screen capture B](../images/screenshots/DUNE2-B.PNG)

The Grand Father of modern real time strategy games; Westwood Studio's "[Dune 2](https://www.mobygames.com/game/dune-ii-the-building-of-a-dynasty)"

<div class="page-break"></div>

![Dune 2 screen capture](../images/screenshots/DUNE2.PNG)

In Dune 2 the player assumes a role as a military commander for one of three houses (Atreides, Harkonnen or Ordos.) The objective is to collect as many resources (spice) as possible and wipe out the opposing forces from the map.

<div class="page-break"></div>

![Dune 2 screen capture A](../images/screenshots/DUNE2-A.PNG)

The game features structure building and upgrade management, allowing the player to build utility structures and powerful units as each map unfolds. The established concepts of micro and macro management are very prevalent in this fun early example of real time strategy.

<div class="page-break"></div>

### Pharaoh's Tomb

![Pharaoh's Tomb screen capture](../images/screenshots/PHARAOHS.PNG)

Micro F/X Software's platformer "[Pharaoh's Tomb](https://www.mobygames.com/game/pharaohs-tomb)"

<div class="page-break"></div>

![Pharaoh's Tomb screen capture A](../images/screenshots/PHARAOHS-A.PNG)

This four part Indiana Jones styled platform game is challenging and fun. The player must unlock the exit to reach the end of the level and progress.

<div class="page-break"></div>

![Pharaoh's Tomb screen capture B](../images/screenshots/PHARAOHS-B.PNG)

The game uses CGA graphics and was released as freeware in 2009.

<div class="page-break"></div>

### Dune

![Dune screen capture A](../images/screenshots/DUNE-A.PNG)

Cryo Interactive's point and click adaptation of the book and movie of the same name; "[Dune](https://www.mobygames.com/game/dune)"

<div class="page-break"></div>

![Dune screen capture B](../images/screenshots/DUNE-B.PNG)

Loosely following the book by Frank Herbert and the film adaptation by David Lynch, Dune was cancelled during production when the electronic adaption rights holder pulled out of the project. The rights holder was eventually convinced to reverse their cancellation decision based on an early prototype of the game.

<div class="page-break"></div>

![Dune screen capture](../images/screenshots/DUNE.PNG)

In Dune the player assumes the role of Paul Atreides and gameplay consists of elements of point and click adventure and real time strategy. The overarching goal is to amass a military force sizeable enough to remove the house Harkonnen from Arrakis.

<div class="page-break"></div>

### Raptor: Call of the shadows

![Raptor screen capture A](../images/screenshots/RAPTOR-A.PNG)

Cygnus Multimedia's' vertical scrolling jet fighter shoot 'em up '"[Raptor: Call of the shadows](https://www.mobygames.com/game/raptor-call-of-the-shadows)"

![Raptor screen capture B](../images/screenshots/RAPTOR-B.PNG)

In Raptor the player controls a space flight capable fighter jet and assumes the role of a mercenary pilot. The main aim of the game is to destroy all of the enemies which appear and to collect currency to be able to purchase upgrades which increase the fighter jet's battle capabilities.

![Raptor screen capture](../images/screenshots/RAPTOR.PNG)

Raptor is regarded by many as one of the most memorable titles of it's time. The game set a high standard and is still fondly remembered by those who played the title when it was released. The MIDI sound track, VGA graphics and Gravis Ultrasound audio support made Raptor one of the most technologically complete examples of DOS gaming at the time.
