

![DBXF Logo](../images/dbxf/dbxf.png)

# DBXF Documentation

## User Guide

### Topics:

This document will cover the following :

* **Overview**
* **The basics**
* **The menu**
* **Short cut key combinations**
* **Command line options**
* **Configuration files**
* **Launch short cuts**
* **MENU.BAT**
* **NOMENU.BAT**
* **Tools**
* **Joystick input**
* **Pixel shaders**
* **Sound**
* **Gravis Ultrasound output**
* **MIDI**
* **Fluidsynth MIDI output**
* **MT32 MIDI output**
* **Commands**
* **Updating DBXF**
* **Compatibility & workarounds**
* **Unsupported features**
* **Reporting bugs**

The topics are covered in the order listed; for easy reference.


###  Overview

This document aims to guide users new to DBXF in using the application and it's features.
The information is broken into sections for ease of reference and aims to be as concise as possible.

If you have any suggestions or recommendations of how to improve this document, please propose a merge request into the staging branch of the project's source code with your changes.
This is fairly trivial to do using Gitlab and any help would be greatly appreciated.

<div class="page-break"></div>

### The basics

![DBXF Main application image](../images/dbxf/main.png)

When run without command line options, DBXF runs in windowed mode with an action menu at the top of the application above the view port.
The view port is the main area of interaction and displays the output of the emulation.

The welcome message is run every time DBXF is started. If you are unfamiliar with DOS, please take the time to explore the intro.

Running DBXF in this way is great for getting a general feel for the application and is also useful for testing purposes.
If you want the most convenient way to launch applications using DBXF, continue reading this document to learn how to use launch short cuts.

### The menu

![1](../images/guide/1.png)

The menu can be interacted with by clicking on the available options using the left mouse button.
Clicking on the menu items will reveal their sub menu items and mouse hovering over the subsequent expansions of the menu expands entries even further.

Clicking on most of the menu entries will introduce the change immediately into the application's running emulation, however it is usually more convenient to use the configuration file system instead to change emulation options before the application is run.

### Shortcut key combinations

DBXF supports the following short cut key combinations:

__```LEFT ALT + ENTER```__

_Toggles between full screen display output and windowed application mode_

__```RIGHT ALT + ENTER```__

_Toggles the display of the menu bar_

__```ALT + PAUSE```__

_Pauses the emulation_

<div class="page-break"></div>

__```CTRL + F1```__

_Displays the keymapper configuration_

__```CTRL + F4```__

_Refresh all drives mounted within DBXF_

__```CTRL + F9```__

_Close DBXF_

__```CTRL + F11```__

_Reduce the number of CPU cycles_

__```CTRL + F12```__

_Increase the number of CPU cycles_

<div class="page-break"></div>

### Command line options

Usage:

**DBXF.EXE** [options]

Where **[options]** is a combination of any of the following parameters:

``-h`` or ``-help``

Show the help.

``-nomenu``

Don't show the menu.

``-fullscreen``

Start DBXF in full screen mode.

``-conf config.conf``

Load the config file **config.conf**. Multiple config files can be specified and the contents of each will be overwritten by each successive entry, except for autoexec entries which will be appended in order.

``-c command-string``

Runs the specified command **command-string**. Multiple commands can be specified. Each command must begin with a **-c** parameter. A command can be either a built in command or a program or script file located within a mounted drive. Make sure to surround the command in quotes to cover spaces.

``-noautoexec``

Skip the **[autoexec]** section of the loaded configuration.

<div class="page-break"></div>

``-securemode``

Same as -noautoexec, but also adds CONFIG.COM -securemode at the bottom of AUTOEXEC.BAT (this disables any changes to how the drives are mounted inside DBXF.)

``-showcycles``

Show cycles and FPS count.

``-showrt``

Show emulation speed relative to real time.

``-startui``

Start DBXF with the configuration UI.

``-startmapper``

Launch the mapper immediately after starting DBXF. Useful to try to resolve keyboard or joystick problems. And remap key bindings.

``-nodpiaware``

Do not signal DPI awareness to the OS.

``-time-limit 10``

Close the emulator after '10' seconds.

``-version``

Output DBXF version information and then exit.

``-exit``

Exit after AUTOEXEC.BAT has run.

<div class="page-break"></div>

``-console``

Show the console.

``-log-con``

Log console output to a log file.

``-debug``

Set all logging levels to debug.

``-early-debug``

Log early initialization messages in DBXF (implies -console.)

``-break-start``

Break into debugger at startup.

``-keydbg``

Log all SDL key events.

``-disable-numlock-check``

Disable numlock check.

``-date-host-forced``

Force synchronization of date with host.

**The following is an example of how you can launch DBXF using a few command line options.**

_Example:_

__``` D:\DBXF\DBXF.EXE -c "MOUNT C D:\DBXF\C_DRIVE" -securemode -conf D:\DBXF\CONF\dbxf.conf ```__

<div class="page-break"></div>

Please take a look at the section title **Launch short cuts** for a simple way to use command line options to quickly launch games or applications.

### Configuration files

DBXF's emulation options can be configured using configuration files. The file extension .conf is used to denote a file of this type.
Multiple configuration files can be passed to the application to create tailored runtime instances suitable for individual applications.
A simple system for doing so is provided with DBXF via the launch short cut scripts. Launch scripts are covered in the next section of this document.

The default configuration file is named dbxf.conf and if the file is missing or not specified (using the -conf command line switch,) the only place DBXF will look for dbxf.conf is the current running directory.
If DBXF finds dbxf.conf in the running directory, it will load the file's contents automatically.

DBXF does not ship with an automatically loaded dbxf.conf because the application has a set of built in default settings which it will use instead if dbxf.conf is not found.

All paths inside configuration files are relative to the folder containing DBXF.exe.

There are a myriad of options available to change within DBXF's configuration and as a general rule, it is best to only make changes to the configuration if you understand the result of that change.

For reference, the file CONF\dbxf.reference.conf is provided. This file contains brief descriptions of all of the available options. Of which; there are many.


### Launch short cuts

Launch short cut scripts are windows command scripts which are used to run DBXF with custom configurations and options.
Some example command scripts can be found in DBXF's RUN directory.

The following is an example of how to create a launch script which launches a DOS application or game in full screen; straight from your desktop:

First, we need a DOS application or game to run. In this example we are going to use the Monkey Island 2 DOS demo available from the [ScummVM site](https://www.scummvm.org/).
The Monkey Island 2 DOS demo is a non interactive demo of Lucasarts' game _Monkey Island 2 - Le Chuck's Revenge._
The demo was made available as shareware to promote the game around the time of it's release.

To obtain a copy of the demo, visit [https://www.scummvm.org/demos](https://www.scummvm.org/demos) and click on the **Monkey Island 2 (DOS demo - not supported by ScummVM)** link.

The link should point to the file _monkey2-dos-ni-demo-en.zip_ which your browser should download automatically for you into your web browser's downloads folder.

Once you have obtained a copy of the file _monkey2-dos-ni-demo-en.zip_ extract the contents into a new folder named **DEMO**.

![2](../images/guide/2.png)

Copy the folder **DEMO** into DBXF's **C_DRIVE** folder. If you are prompted to provide Administrator permission to copy the folder, click on continue.

![3](../images/guide/3.png)

Inside the **C_DRIVE\DEMO** folder the following files should be present: **demo.rec** **mi2demo.000** **mi2demo.001** **mi2demo.002** **mi2demo.exe** and **null.ims**.

![4](../images/guide/4.png)

Next, move into DBXF's **RUN** directory and copy the file **DEFAULT EXAMPLE.CMD** and paste into the same folder, so that you have a copy named: **DEFAULT EXAMPLE - Copy.CMD**.

![5](../images/guide/5.png)

Rename the file **DEFAULT EXAMPLE - Copy.CMD** to **Monkey 2 Demo.CMD** and edit the file using a text editor (using a modern text editor such as [Notepad++](https://notepad-plus-plus.org/) or [Atom](https://atom.io/) here is recommended over Notepad.)

![6](../images/guide/6.png)

In this example we will use Notepad.

![7](../images/guide/7.png)

Change the comment text at the very top to: **Run Monkey 2 Demo**.

![8](../images/guide/8.png)

Change **SET UVW=folder** to **SET UVW=DEMO** and change **SET XYZ=game.exe** to **SET XYZ=mi2demo.exe** and save the file.

![9](../images/guide/9.png)

Return to the folder **DBXF\RUN** and right click on the file **Monkey 2 Demo.CMD**. Click on Open.

![10](../images/guide/10.png)

DBXF will run and the screen will remain black for a few seconds but after a brief pause, the demo should launch.

![11](../images/guide/11.png)

The demo is non interactive (sorry, you don't get to play) and has no sound but it shows off the game.

If you have not played Monkey Island 2 - Le Chuck's Revenge, then I highly recommend grabbing a copy of the [re-master](https://store.steampowered.com/app/32460/Monkey_Island_2_Special_Edition_LeChucks_Revenge/) from Steam, instead of trying to play the game using DBXF.

<div class="page-break"></div>

To close the demo, exit full screen using the key combination **ALT +ENTER** and close the application window.
(By default games and applications are launched in full screen when using launch scripts. Use the windowed configuration setting by looking at the file **WINDOWED EXAMPLE.CMD** if you want to change this behaviour.)

That's all there is to it. Adapt the example to any other applications or games you want to launch. And explore the other examples in the **RUN** folder to learn how to change DBXF's configuration options.

Once you have created a launch script and you want quick access to it, right click on the script, move down the menu which appears until you get to the entry **Send to** and then click on **Desktop**.
A desktop short cut will be created on your desktop. For further customisation, right click on the new short cut on your desktop, click on properties, select the **Shortcut** tab and click on **Change Icon.** There you can set a custom icon for the short cut if you like to establish visual clues on your desktop.

<div class="page-break"></div>

### MENU.BAT

Located inside the folder **C_DRIVE** in the DBXF directory is a batch script file named **MENU.BAT**.

![12](../images/guide/12.png)

**MENU.BAT** is a looping script which you can use to launch games or other applications from command scripts and it produces the choice to restart the game (or application) or exit DBXF once the game (or application) has closed. It is provided as a convenience and compliments the launch script system.

If you choose not to use **MENU.BAT** (it is entirely optional) the running application will simply exit to the DOS shell instead.

Feel free to edit and adapt **MENU.BAT** in any way you see fit for your own use.


### NOMENU.BAT

Similar to the script **MENU.BAT** is the script **NOMENU.BAT**.
**NOMENU.BAT** can be used instead of **MENU.BAT** and it simply exits DBXF without providing a menu when you close the running application or game.

Please adapt **NOMENU.BAT** for your own use in anyway you see fit.

### Tools

Inside the **TOOLS** directory, there are a number of command scripts which provide quick access to some useful ways to run DBXF.

The tool command scripts perform the following functions:

**DBXF DIAGNOSTICS.CMD**

_Launch DBXF using the optimized settings. Useful for configuring application installers and general testing._

**DBXF.CMD**

_Launch DBXF using the reference default settings. Useful to test differences in emulation based on configuration changes._

**GRAVIS ULTRASOUND EMU MANAGER.CMD**

_Launch the Gravis Ultrasound emulation manager. Use this to manage GUS emulation. The Gravis Ultrasound drivers must first be installed._

**GRAVIS ULTRASOUND MIDI DEMO.CMD**

_Launch the Gravis Ultrasound midi demo. Use this to test that GUS MIDI is working correctly. The Gravis Ultrasound drivers must first be installed._

<div class="page-break"></div>

**REMAP DEFAULT KEYS.CMD**

_Change the default input mapping._

**REMAP JOY2KEY.CMD**

_Change the input mapping where keys are mapped to a joystick. This script loads the map file which has keystrokes mapped to joystick inputs. This is useful for some games which only support keyboard input but you want to use a controller instead._

**REMAP JOYSTICK.CMD**

_Change the joystick input mapping. The controller or joystick must be connected first before launching. If you are attempting to get an xbox 360 controller to work with DBXF and it appears not to work, you may need to first change the timed= configuration option in the [joystick] section of your configuration file to false._

**REMAP KEY2JOY.CMD**

_Change the input mapping where a joystick is mapped to key inputs. This script loads the map file which has joystick inputs mapped as key presses. This is useful for some games which only support joystick input but there is no controller attached._

<div class="page-break"></div>

### Joystick input

Inside the folder KEYMAPS there are three key maps provided for convenience.

**default.map**

Is loaded by default when the configuration files dbxf.conf or dbxf.reference.conf are loaded using the -conf option.

Can be reconfigured using the file **REMAP DEFAULT KEYS.CMD** found in the TOOLS directory.

**joy2key.map**

Is loaded when the configuration file CONF/IO/joy2key.conf is loaded using the -conf option.

Can be reconfigured using the file **REMAP JOY2KEY.CMD** found in the TOOLS directory.

**joystick.map**

Is loaded when the configuration file CONF/IO/joystick.conf is loaded using the -conf option.

Can be reconfigured using the file **REMAP JOYSTICK.CMD** found in the TOOLS directory.

**key2joy.map**

Is loaded when the configuration file CONF/IO/key2joy.conf is loaded using the -conf option.

Can be reconfigured using the file **REMAP KEY2JOY.CMD** found in the TOOLS directory.

<div class="page-break"></div>

****The following is a simple guide to the mapper:****

The mapper is logical but not very intuitive.
In the following example; we will bind the **w** key to the **up arrow** key. The result of doing so is that when pressing the up arrow key, the w key is also pressed at the same time.

![13](../images/guide/13.png)

First, run the **REMAP DEFAULT KEYS.CMD** script in the **TOOLS** directory. This will start the mapper application.

![14](../images/guide/14.png)

Using the mouse, click on the key you want to bind or change. In this example we will click on the up arrow. The key will turn green.

![15](../images/guide/15.png)

Click on the **Add** button.

![16](../images/guide/16.png)

Click on **Capt**.

![17](../images/guide/17.png)

Press the key on your keyboard which you want to bind to the key you first selected (in this case press **w**.) Then press the **ESC** key on your keyboard. This releases the capture.

![18](../images/guide/18.png)

Finally, click on **Save** to permanently store the new setting.

In the example, the outcome is that every time the up arrow key is pressed, the up arrow and the w key input are sent to DBXF.

The mapper will give you a visual clue to any bound events when you press a key and there is a notification displaying any bindings to the left hand side of the mapper, displayed next to the text BIND.

When clicking on save, the key assignment change is saved to the mapper file specified in the loaded configuration file. You can create multiple mapper files for any number of input configurations as you so wish. The mapper files supplied with DBXF are provided for your convenience. Please configure them however you want.

**To remove an assignment:**

Click on the key which you want to change.

Click on **Del**.

Click on **Save**.

**To close the mapper**

Click on Exit (DBXF will restart and the application can be closed.)

**To reset the mapper file**

If you incorrectly assign a key and cannot undo the assignment, copy and replace the mapper file you have made changes to (inside the KEYMAPS directory) with a copy from the release archive and try again.

<div class="page-break"></div>

### Pixel shaders

Direct3d pixel shaders (.fx files) can only be used if sdl output is set to _direct3d_ and render scaler is either *hardware_none, hardware2x, hardware3x, hardware4x or hardware5x*.

For example:

```
[render]
scaler=hardware5x

[sdl]
pixelshader=Lanczos12.fx
output=direct3d
```

To view the effects a pixel shader has on the emulation output, please take a look at the examples inside the [Pixel Shader](https://gitlab.com/TW3/dbxf/blob/master/docs/Pixel%20Shaders.pdf) document or [gulikoza's gallery page](http://www.si-gamer.net/gulikoza/gallery).

The available pixel shaders can be found inside DBXF's **SHADERS** directory.


### Sound

The default sound emulation in DBXF is Soundblaster 16 (sb16.)

For most applications, sb16 will be sufficient or the only option available but for many games, the sound quality can be improved by using the Gravis Ultrasound output instead and\or fluidsynth MIDI output. The manual for the game or application you are trying to run using DBXF, should provide you with a list of supported options, including the supported sound cards. The sound output of the game is almost always configured by the setup application when the game is installed. It is almost always possible to run DOS setup or install routines again after an install to reconfigure and change the options.

<div class="page-break"></div>

### Gravis Ultrasound output

Due to licensing incompatibilities, the Gravis Ultrasound drivers are not installed into DBXF by default. If you want Gravis Ultrasound (gus) emulation to work in DBXF, then you must first install the drivers.

To learn more visit: [https://www.dosbox.com/wiki/Sound#Gravis_Ultrasound](https://www.dosbox.com/wiki/Sound#Gravis_Ultrasound)

DBXF's default gus directory is:

```
[gus]
ultradir=C:\ULTRASND
```

This translates to *C_DRIVE\ULTRASND\* inside the DBXF application folder.
You can change the directory to any location you prefer as long as you change the path within the *ultradir* configuration option.

If you setup gus emulation, you can use the scripts **GRAVIS ULTRASOUND EMU MANAGER.CMD** and **GRAVIS ULTRASOUND MIDI DEMO.CMD** to test and configure the emulation.

Remember to run the setup routine of the program you want to run and select Gravis Ultrasound as the sound output device, before running the application, if applicable.

Some more information about gus emulation can be found here:

[https://www.ufopaedia.org/index.php?title=GUS_Music_in_Dosbox](https://www.ufopaedia.org/index.php?title=GUS_Music_in_Dosbox)

[http://www.gravisultrasound.com/index.htm](http://www.gravisultrasound.com/index.htm)

<div class="page-break"></div>

### MIDI

For applications and games which send MIDI output, DBXF has various ways to deal with the data produced by the emulated MPU-401 MIDI device. Using the mididevice configuration option within the [midi] section of a configuration file, the type of MIDI emulation can be selected.
The type can be one of the following:

**win32** : Use Window's default MIDI system to handle MIDI data.

**mt32** : Turn MIDI output data into music using libmt32emu (experimental.)

**synth** : Turn MIDI output data into music using libfluidsynth.

**none** : MIDI data will not be handled.

For some reference MIDI information related to some well known game titles, including the bank type to use, visit: [https://www.vogons.org/viewtopic.php?t=35811](https://www.vogons.org/viewtopic.php?t=35811) and check to see if there is any relevant information or patches in the list at the top of the thread.

MIDI data can also be sent out from DBXF to an external MIDI synthesizer application. Please look at the information regarding the command **MIXER** (further on in this document) if you want to process MIDI data in this way.


### Fluidsynth MIDI output

DBXF has built in support for DOS applications which output general MIDI data by leveraging libfluidsynth ([http://www.fluidsynth.org/](http://www.fluidsynth.org/)) to turn the data into sound. Fluidsynth not only allows you to customise the sound output by loading different sound fonts but it also permits tuning of chorus and reverb effects which can be applied to the sound.
fluidsynth output can also be used to produce sound output from Roland gs MIDI data.

To use fluidsynth MIDI support in DBXF, load your chosen sound font by editing **CONF\SOUND\synth.conf** to reflect the location of the sound font you want to use. For example use the following inside the **[midi]** section of your configuration:

```
synth.soundfont=.\SOUNDFONTS\WeedsGM3.sf2
```
Ensure that you have a copy of the sound font you want to use inside the **SOUNDFONTS** directory.

When looking for sound fonts, your favourite search engine will assist you, just ensure that the sound font you want to load is of .sf2 format.
A popular freely available sound font can be fount at: [https://www.un4seen.com/download.php?extra/WeedsGM3](https://www.un4seen.com/download.php?extra/WeedsGM3)
and a high quality large sound font can be found at: [http://midkar.com/soundfonts/](http://midkar.com/soundfonts/) but feel free to experiment and try any others which you may find.

Set the mididevice option to synth inside the **[midi]** section of your configuration. For example:

```
mididevice=synth
```

Once you have a sound font and have set up your synth.conf file, copy the file **SYNTH EXAMPLE.CMD** into the same directory, rename it and modify it's contents to launch the game or application you want to run.

Finally, ensure that the music output of the program or application you want to launch is set to output general MIDI (also abbreviated to gm.)

With those options set, you should hear the synthesized sound output through your headphones or speakers when the application is running.

If the game or application you want to run sends Roland gs MIDI data output, change the synth.bank configuration option to gs; like so:

```
synth.bank=gs
```

### MT32 MIDI output

DBXF can emulate the sound output of a Roland MT32 MIDI device.

Emulating MT32 sound output in DBXF is made possible via the included libmt32emu library. libmt32emu is a component of munt ([https://github.com/munt/munt](https://github.com/munt/munt)) and some amount of set up is required before the emulation can be used.

For legal reasons, you should only be attempting MT32 emulation using the MT32 ROM files if you actually own a Roland MT32.

MT32 emulation is in no way supported or endorsed by the Roland Corporation.

<div class="page-break"></div>

The Roland MT32 has two revision sets of it's ROM files:

MT32 (rev. 0) Old: **MT32_CONTROL.ROM** & **MT32_PCM.ROM**

MT32 (rev. 1) New: **CM32L_CONTROL.ROM** & **CM32L_PCM.ROM**

The ROM set you will need will depend upon the game or application which you want to run and also which type of MT32 it supports (new or old.)
The ROM files are not shipped with DBXF because they are copyright Roland Corp. Please do not request information of how to obtain these files. Please dump the ROMs of the MT32 unit which you own yourself (if you are unable to obtain copies elsewhere.)

Not many DOS games properly supported MT32 MIDI output. [https://en.wikipedia.org/wiki/List_of_MT-32-compatible_computer_games#IBM_PC_compatibles](https://en.wikipedia.org/wiki/List_of_MT-32-compatible_computer_games#IBM_PC_compatibles)
Is a list of known MT32 supported games and their type of support (new or old.)
Before attempting MT32 emulation using DBXF, please use this list to ensure that you load the correct set of ROMs.

*A word of warning here: Many games which advertised MT32 support did not actually have proper MT32 support and some floppy disk releases even had MT32 sound options within the install routine which only worked with the contents of the CD release. The gold standard for MT32 output of games of this era is likely [Space Quest III](https://en.wikipedia.org/wiki/Space_Quest_III) (if you are interested in a good test case.)*

To enable MT32 support in DBXF; you must set the **mididevice** within the [midi] section of your configuration to **mt32** like so:

```
mididevice=mt32
```

Then you must specify the directory containing the mt32 ROM files using the mt32.romdir configuration option like this:

```
mt32.romdir=.\MT32\NEW\
```
Finally, you should run the installation routine for the game or application you are attempting to run using DBXF and ensure that the music or sound output option is set Roland MT32.

It is recommended that you use a launch script to do so, on a per application basis. This allows you to use the correct ROM set for the game or application which supports it.
The file **CONF\SOUND\mt32.conf** is provided to make doing so simple.

If using an MT32 beyond DBXF is something which you are interested in; [http://www.midimusicadventures.com/queststudios/mt32-resource/utilities/](http://www.midimusicadventures.com/queststudios/mt32-resource/utilities/) and [http://www.polynominal.com/roland-mt-32/index.html](http://www.polynominal.com/roland-mt-32/index.html) are great online resources which may be of use to you.


### Commands

The DBXF shell provides access to many common DOS commands.

At the prompt append ```/?``` to a command to view information about accepted parameters. For example: ``` COPY /? ```

The following commands are available from the DBXF prompt:

##### BOOT #####

```
BOOT [disk1.img] -l [driveletter]
```
Example: ``` BOOT C:\disk1.img -l X ```

Boot floppy or hard disk images inside DBXF. Useful if you want to try to install an operating system.

Please see [https://www.dosbox.com/wiki/BOOT](https://www.dosbox.com/wiki/BOOT) for further information.

##### CD #####

```
CD [PATH]
```

Examples: ``` CD FILES ``` , ``` CD .. ``` , ``` CD C:\FILES ```

Change directory. Relative paths are relative to the current directory displayed at the prompt (left of the cursor.)

<div class="page-break"></div>

##### CHOICE #####

```
CHOICE [options] "Some text"
```

Pauses the prompt and will not return back until keyboard input is received. Displays a Y/N prompt for yes or no responses. Useful for batch scripting.

Example (*file ASK.BAT*) :

```
@ECHO OFF
CHOICE "Do you like peas?"
IF %ERRORLEVEL% == y (ECHO "Y was selected.") ELSE ECHO "N was selected."
```

##### CLS #####

```
CLS
```
Clear the screen.

##### CONFIG #####

```
CONFIG [options]
```

Get or set configuration options from within a running instance of DBXF.

Example: ``` CONFIG -set "dos ems=off" ```

<div class="page-break"></div>

##### COPY #####

```
COPY [path\filename] [newpath\newfilename]
```

Copy files.

Examples: ``` COPY file.txt newfile.txt ``` , ``` COPY C:\file.txt C:\NEW\file.txt ```

##### DEL #####

```
DEL [path]\[name]
```

Delete files.

Examples: ``` DEL file.txt ``` , ``` DEL C:\TEST\file.txt ```

DEL *does not delete directories. To delete directories use* **RD** *instead.*

##### DIR #####

```
DIR [options]
```

List the contents of directories.

Examples: ``` DIR /w ``` , ``` DIR /p ``` , ``` DIR /w /p C:\TEST\```

<div class="page-break"></div>

##### EXIT #####

```
EXIT
```

Close DBXF.

##### IMGMOUNT #####

```
IMGMOUNT [drive] [path]\[name] [options]
```

Mounts a floppy or compact disk image file within DBXF and makes the contents available as a local drive.

Examples: ``` IMGMOUNT x C:\PROGRAMS\DBXF\disk.iso -t iso ``` , ``` IMGMOUNT P "C:\floppy.img" -t floppy ```

Please see [https://www.dosbox.com/wiki/IMGMOUNT](https://www.dosbox.com/wiki/IMGMOUNT) for more detailed information.

##### IPXNET #####

```
IPXNET [options]
```

Provides management of client and server IPX protocol connections. IPX allows multiple instances of DBXF running on different network connected machines to communicate with each other. For clients to be able to communicate, they must be connected to an IPX server instance.

An example use for IPXNET is to play the game Warcraft 2 (online or using a LAN) with other players.

<div class="page-break"></div>

IPXNET is not available inside the shell unless ipx is set to **true** within the ipx section of a loaded configuration file, like so:

```
[ipx]
ipx=true
```

Please see: [https://www.dosbox.com/wiki/IPX](https://www.dosbox.com/wiki/IPX) for more information regarding IPX.

##### KEYB #####

```
KEYB [layout]
```

Change the keyboard layout to match the language of your choice (if available.)

Set the keyboard layout to United States English: ``` KEYB us ```

It may be more suitable to set the [dos] **keyboardlayout** configuration option instead:

```
[dos]
keyboardlayout=us
```

##### LOADFIX #####

```
LOADFIX
```

Loads a program above the first 64k of memory (useful for some early DOS applications which were compressed using a buggy version of [Microsoft EXEPACK](http://www.shikadi.net/moddingwiki/Microsoft_EXEPACK))

<div class="page-break"></div>

##### LOADHIGH #####

```
LOADHIGH
```

Loads a program into the upper memory. To use LOADHIGH the following configuration options be set:

```
[dos]
xms=true
umb=true
```

##### MD #####

```
MD [path]\[name]
```

Create a new directory (shorthand for **MKDIR**.)

Examples: ``` MD GAMES ``` , ``` MD C:\GAMES\ ```

*Due to the maximum eight character file and folder name length restriction apparent in DOS (without LFN support,) it is recommended that you use file and folder names of eight characters or less. You can reference long directory or file names by substituting* **~1** *as the seventh and eighth characters of the name. For example; this* **really-long-folder-name** *becomes* **really~1** *. You could delete it using:* ``` RD really~1 ```

##### MEM #####

```
MEM
```

Display information about available, free and used memory.

##### MIXER #####

```
MIXER [channel] [left:right] [/NOSHOW] [/LISTMIDI]
```

View or change the audio output volume. Or list available MIDI devices.

Examples: ``` MIXER ``` , ``` MIXER MASTER 74:74 ``` , ``` MIXER /LISTMIDI ```

The */NOSHOW* option prevents the resulting volume change being returned.

The */LISTMIDI* option lists available MIDI devices declared by Microsoft Windows. Use the list produced to choose where external MIDI data is sent by changing the [MIDI] section midiconfig option to the number of the MIDI device you want to use. For example:

```
[midi]
midiconfig=1
```

##### MOUNT #####

```
MOUNT [drive] [path] [options]
```

Mount an external folder as a drive within DBXF.

Example: ``` MOUNT x C:\PROGRAMS\DBXF\C_DRIVE\ ```

See [https://www.dosbox.com/wiki/MOUNT](https://www.dosbox.com/wiki/MOUNT) for further examples.

<div class="page-break"></div>

##### RD #####

```
RD [path]\[name]
```

Delete a directory.

Examples: ``` RD TEMP ``` , ``` RD X:\TEMP ```

##### REN #####

```
REN [path]\[name] [newname]
```
Rename a file.

Examples: ``` REN NOTES.TXT NEW.TXT ``` , ``` REN C:\NOTES.TXT NEW.TXT ```

##### RESCAN #####

```
RESCAN
```

Read the contents of all mounted drives. Useful if any of the contents have change outside of DBXF.

*RESCAN has the same effect as sending the CTRL+F4 key combination.*

<div class="page-break"></div>

##### TYPE #####

```
TYPE [path]\[name]
```

Display the contents of a text file.

Examples: ``` TYPE README.TXT ``` , ``` TYPE C:\README.TXT ```

##### SET #####

```
SET [VARNAME = DATA]
```

Display or set an environment variable.

Examples: ``` SET ``` , ``` SET GAMES=C:\GAMES\ ```

##### VER #####

```
VER
VER SET [minor] [major]
```

View the DBXF and reported DOS version numbers. Change the reported DOS version number.

Examples: ``` VER ``` , ``` VER SET 7 1 ```

<div class="page-break"></div>

### Updating DBXF

Updating DBXF is simple.

First ensure that you have obtained the latest release from [https://tw3.gitlab.io/dbxf/](https://tw3.gitlab.io/dbxf/) and extract the archive to a temporary location (the name of the release archive will be similar to dbxf-v0.4.0-win10-x86_64.zip and contains dbxf.exe.)

Next, delete all of the folders inside the extracted copy of the release archive.

Finally, copy the remaining files from the temporary location to the location which you have installed DBXF, overwriting all files when prompted.
Clean up by deleting the folder which was extracted into the temporary location.


### Compatibility & workarounds

Some applications or games may not work as expected "out of the box" using DBXF's default configuration. You may need to patch the application or set various configuration options to resolve problems which cause non-existent or degraded emulation.

[https://www.dosbox.com/comp_list.php?letter=a](https://www.dosbox.com/comp_list.php?letter=a) is a link to the DOSBox database which occasionally contains tips on launching specific DOS software titles. You can search for the title you are trying to run by name. Not all the recommendations will apply but it is a good place to look if you think you are experiencing an application related issue.

[https://www.dosbox.com/wiki/Software](https://www.dosbox.com/wiki/Software) has information relevant for specific DOS applications (not games.)

[https://www.vogons.org/index.php](https://www.vogons.org/index.php) Is the official DOSBox forum and contains lots of DOS emulation tips.

If you see the message *Packed file is corrupt* and an application fails to run, you will need to run LOADFIX before starting the application. An example of a game which will run only after LOADFIX is called is [Ninja](https://www.mobygames.com/game/dos/ninja_).

DOS typically only supports a maximum eight character file and folder name length (without LFN support.) It is recommended that you only use file and folder names of eight characters or less in length when dealing with files and folders within DBXF.

If you see the message: Divide Error, then the emulated CPU is causing a bug. To work around the issue, either change the ```[cpu]```
option ```cputype=pentium_mmx``` to ```cputype=auto``` or add **CONF\MACHINE\machine.conf** to your launch script.

<div class="page-break"></div>

### Unsupported features

The following features are unsupported and part of the legacy of the fork from Dosbox-X:

**PC98 emulation** - Supporting the emulation of the Japanese PC98 system is beyond the scope of DBXF's development goals. Please do not rely on DBXF for PC98 emulation.

**Screen, sound or any other form of built in media capture** - Please use [OBS Studio][3c1197b9] if your requirements are to record DBXF's sound and video output. Please use the print screen key or the snipping tool if you would like to create screen capture images.

  [3c1197b9]: https://obsproject.com/ "OBS Studio"

### Reporting bugs

Every care is taken to ensure that DBXF is bug free but if you do find a bug, please report the issue using the issue tracker at:  [https://gitlab.com/TW3/dbxf/issues](https://gitlab.com/TW3/dbxf/issues) and optionally include any patches or merge requests which fix the issue you are reporting.
When filling in a bug report, it is good practice to structure your post using the following headings:

* The issue
* How to reproduce the issue
* Expected behaviour
* Actual behaviour
* Summary (include any patches which fix the issue here)

Submitting a good bug report increases the likelihood that your issue will be fixed more quickly. And doing so also helps others to understand the issue you are experiencing, more easily.

Please be aware that the project's issue tracker is not a support forum and should not be treated as such. If in doubt, please refrain from asking questions there unless absolutely necessary and please also ensure that you communicate in an appropriate and polite manner at all times.

**Thanks!**
