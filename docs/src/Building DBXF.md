
![DBXF Logo](../images/dbxf/dbxf.png)

# DBXF Documentation

## Building DBXF

<div class="page-break"></div>

### Topics:

This document will cover the following :

* **Overview**
* **Prerequisites**
* **Obtaining the source code**
* **Setting up the build environment**
	1. **DXSDK**
	1. **MS Visual Studio**
	3. **Notepad++** (optional)
	4. **Git**
	5. **Pulling DBXF from Gitlab**
	7. **Starting your first build**
* **Testing**
* **Reporting bugs**

<div class="page-break"></div>

###  Overview:

This document aims to describe the steps needed to be taken in order to compile DBXF via Visual Studio 2019 using Microsoft Windows 10.

DBXF uses Microsoft's Visual Studio build chain.
You will need to install Microsoft Visual Studio IDE along with the following software:

* Microsoft DirectX SDK
* Git
* A modern text editor (Notepad++ or Visual Studio Code for example)

The steps involved in obtaining all of the required software will be covered within the contents of this document.
It is advised that you follow through this document and complete the steps in the order they are presented, to achieve a fully working build environment.

The estimated total time to complete all of the steps is approximately between one and two hours, depending upon your Internet connection speed and any number of other possible time relevant factors.

<div class="page-break"></div>

### Prerequisites

Compiling DBXF requires the use of a computer either meeting or exceeding the following minimum specs :

* Dual Core 1.6 GHz. 64 bit or greater CPU (Intel or AMD)
* 2 gigabytes or more RAM installed
* Approximately 20 gigabytes or more free hard disk space
* Microsoft Windows 10 (Home, Pro or Enterprise)
* A working Internet connection (the faster the better)

In most cases, a modern desktop or laptop computer which has Windows 10 64 bit installed can compile DBXF's source code.


### Obtaining the source code:

The DBXF source code is available for download from the Gitlab project page [https://gitlab.com/TW3/dbxf](https://gitlab.com/TW3/dbxf) but in this guide,
git will be used to pull the source code straight from Gitlab. This is done so that you can maintain your own fork and optionally submit your changes back to the project; should you wish to.

### Setting up the build environment:

Please follow each step listed below in order. If you get stuck and a step doesn't make sense or doesn't seem to work out correctly, stop and take a break.
Come back to the machine and try again when you feel rested and refreshed.

If you do find a genuine error in this document, please help by filing a bug report at [https://gitlab.com/TW3/dbxf/issues](https://gitlab.com/TW3/dbxf/issues)
so that we can improve this guide. Optionally, a pull request via Gitlab with a fix of your own creation may also help greatly.

**To setup the build environment; please carefully complete the following steps:**

<div class="page-break"></div>

#### DXSDK

![2](../images/dev/2.png)

Install the DXSDK (Microsoft DirectX Software Development Kit.)

Visit the following link: [https://www.microsoft.com/en-us/download/details.aspx?id=6812](https://www.microsoft.com/en-us/download/details.aspx?id=6812)
and click on the download link on the page.
Once downloaded, run the installer.
If prompted for .NET 3.5 to be installed, allow .NET 3.5 to be installed.
Continue on and complete the installation.

If you already have Visual Studio 2019 installed and you choose to skip the next step, you will need to reboot at this point before continuing.

<div class="page-break"></div>

#### MS Visual Studio

![3](../images/dev/3a.png)

Install Microsoft Visual Studio 2019 IDE (Requires 11GB of space.)

Visual Studio is available to download from Microsoft via: [https://www.visualstudio.com/](https://www.visualstudio.com/) and includes most of what you will need to build DBXF on Windows.

Click on the "**Download for Windows**" link and then choose "**Community 2019**." Save and run the file.

![4](../images/dev/4a.png)

Once the installer has fully loaded and you are presented with the workloads tab. Do the following:

Click on the "**Individual Components**" tab at the top of the installer.

<div class="page-break"></div>

Only the following items in the list of components need be installed:

* C# and Visual Basic Roslyn compilers
* MSBuild
* C++ core features
* Windows Universal C Runtime
* MSVC v142 - VS 2019 C++ x64/x86 build tools
* MSVC v142 - VS 2019 C++ x64/x86 Spectre-mitigation
* Windows 10 SDK (10.0.18362.0 or newer)

If there is a newer version of the Windows 10 SDK available; please feel free to select it instead of version 10.0.18362.0.

None of the other entries in the list of components need to be selected unless you specifically want to use any of them.

![5](../images/dev/5a.png)

At the bottom of the installer application, check that the installation directory is where you want Visual studio to be installed, click on close and then click on "**Install.**"

![6](../images/dev/6.png)

When the installation is complete, you will be prompted to restart.

![7](../images/dev/7.png)

Save and close any work you have open in the background and click on restart.

When the machine has restarted, run Visual Studio 2019 from the start menu.

The first time you run Visual Studio, it may ask you to sign in. Click on "**Not now, maybe later**" unless you have an account, in which case - Login.

![8](../images/dev/8.png)

Once the application has started, you can register to remove the 30 day trial by clicking on *Help* -> *Register product* using the menu at the top of the program, should you need to.

![9](../images/dev/9.png)

A menu will appear where you can create an account to register the product.

<div class="page-break"></div>

#### Notepad++ (optional)

![9a](../images/dev/9a.png)

Install Notepad++

When making commits using Git, a text editor application is used to sign them off. A recommended application for this task is Notepad++. This is because it integrates well with the Git installer.
If you already have an alternative text editor installed which you will use to make Git commits, you can skip this step.

Visit [https://notepad-plus-plus.org/](https://notepad-plus-plus.org/) and navigate to the downloads page.

The downloads page is very polluted with adverts, so you will want to scroll down to "**Download 64-bit x64**" and you likely want to download "**Notepad++ Installer 64-bit x64**."

Run the install program once it has finished downloading. When you have successfully installed Notepad++, continue onto the next step.

<div class="page-break"></div>

#### Git

![10](../images/dev/10.png)

Installing Git

![10-A](../images/dev/10-A.png)

Download the Git for Windows installer from: [https://git-scm.com/](https://git-scm.com/) by clicking on the Windows Build link on the home page.

The Git for Windows installer has several configuration options. The following steps should guide you through the process.

![10-B](../images/dev/10-B.png)

When running the installer, unless you know what they mean and want these options, un-check "**Windows explorer integration**" and "**Associate .sh files with Bash**."
Place a tick next to "**Use a true type font in all console windows**" and click on next.

![10-C](../images/dev/10-C.png)

When prompted to choose the default editor, select **Notepad++** (if you installed it previously,) otherwise choose Visual Studio Code or your editor of choice; if it is available.

![10-D](../images/dev/10-D.png)

"**Use git from the windows command prompt**" should already be selected for you (if not, select it.)

![10-E](../images/dev/10-E.png)

You probably want to select "**Use native Windows Secure Channel Library**" (unless you plan to update git a lot in the future.)

![10-F](../images/dev/10-F.png)

Leave "**Checkout Windows style, commit Unix-style line endings**" checked in the next menu.

![10-G](../images/dev/10-G.png)

Select "**Use Windows' default console window**" in the next menu.

![10-H](../images/dev/10-H.png)

You likely do not want to enable symbolic links in the next menu but file system caching and git credential manager should be enabled. Leave them checked and click on "**Next**."

![10-I](../images/dev/10-I.png)

Git will have finally been installed when you click on "**Finish**."

<div class="page-break"></div>

#### Pulling DBXF from Gitlab

Pull a copy of the project's source code from Gitlab.

Press the **Windows key + r** key combination.
Type: __``` cmd ```__ and press enter. A command prompt window should spawn.

At this point, you will need to decide on a location which used to save the source code. In the following example, the location **C:\DBXF** is used.
If you wish to use a location other than **C:\DBXF**, then remember to adapt the next steps accordingly by replacing **C:\DBXF** with the path you want to use instead.
It is recommended that if you do choose a different directory path, that you do not use any directory names which contain spaces.

![11](../images/dev/11.png)

At the command prompt type:

__``` MKDIR C:\DBXF ```__

Move into the directory using:

__``` CD C:\DBXF ```__

Create a new git repository in the current directory using:

__``` git init ```__

Pull the source code from your own fork (or from the main repository) into the current directory using something like:

__``` git pull https://gitlab.com/TW3/dbxf.git ```__

Once the command has successfully completed, type:

__``` DIR ```__

To confirm that you have now downloaded a copy of the tree into the current directory.

<div class="page-break"></div>

#### Starting your first build

First, make a local copy of the build script:

`COPY BUILD.CMD B.CMD`

Before you start a build for the first time, the build script needs to be made aware of where the Visual Studio script **vcvars64.bat** is. To do this, edit the file **C:\DBXF\B.CMD** using Notepad++ and modify the line:

`CALL "D:\Programs\MSVS2019\VC\Auxiliary\Build\vcvars64.bat"`

to point to the full path to **vcvars64.bat** on your system (the file can be found inside the Visual Studio installation directory.) Save and close the file.

From the open command prompt window, issue the command:

__``` B.CMD ```__

![17a](../images/dev/17a.png)

The build process will start and provided that all prerequisites and dependencies have been installed, once the application has completed building, a `Build succeeded` message will appear.

![28](../images/dev/28.png)

Once the build has completed successfully, the compiled output is created inside of the folder **DBXF\bin\x64\Release\DBXF**.

Copy the **DBXF** folder anywhere on your hard drive and you can begin testing/using the application.

__Congratulations!__ You have successfully __built__ DBXF!

<div class="page-break"></div>

### Testing:

The simplest test you can perform; is to just launch the application and ensure that it does not crash. Beyond that, it is recommended that you test specifically the area of the application which you have been working on before submitting any patches to the project.

If you want to submit code to the project, please ensure that you have tested your changes to the best of your ability. If you require help testing, please open an issue on the bug tracker with a request.

If you are interested in working with DBXF's latest code, you should fork the project on Gitlab and switch to the staging branch using the git commands:

```
git remote add origin https://gitlab.com/YOURUSERNAME/YOURFORK
git pull
git checkout staging
```
Replacing **YOURUSERNAME** with your Gitlab username and **YOURFORK** with the name of your DBXF fork on Gitlab (likely dbxf.git)

You can then git pull and push to your own fork and propose merge requests to DBXF staging using Gitlab.


### Reporting bugs:

Please report any bugs you find at [https://gitlab.com/TW3/dbxf/issues](https://gitlab.com/TW3/dbxf/issues) and optionally include any patches or merge requests which fix the issue you are reporting.
When filling in a bug report, it is good practice to structure your post using the following headings:

* The issue
* How to reproduce the issue
* Expected behaviour
* Actual behaviour
* Summary (include any patches which fix the issue here)

Submitting a good bug report increases the likelihood that your issue will be fixed more quickly and doing so also helps others to understand the issue you are experiencing more easily.
