

![DBXF Logo](../images/dbxf/dbxf.png)


# DBXF Documentation

## Pixel Shaders

###  Overview:

The following document contains screen capture images of DBXF's DirectX pixel shader output.

<div class="page-break"></div>

### 2xSaI

![2xSaI](../images/shaders/2xSaI.png)

Filename: 2xSaI.fx

<div class="page-break"></div>

### 2xSaI_sRGB

![2xSaI_sRGB](../images/shaders/2xSaI_sRGB.png)

Filename: 2xSaI_sRGB.fx

<div class="page-break"></div>

### 2xSaL

![2xSaL](../images/shaders/2xSaL.png)

Filename: 2xSaL.fx

<div class="page-break"></div>

### 2xSaL_Ls

![2xSaL_Ls](../images/shaders/2xSaL_Ls.png)

Filename: 2xSaL_Ls.fx

<div class="page-break"></div>

### 2xSaL2xAA

![2xSaL2xAA](../images/shaders/2xSaL2xAA.png)

Filename: 2xSaL2xAA.fx

<div class="page-break"></div>

### 2xSaLAA

![2xSaLAA](../images/shaders/2xSaLAA.png)

Filename: 2xSaLAA.fx

<div class="page-break"></div>

### 4xSaL

![4xSaL](../images/shaders/4xSaL.png)

Filename: 4xSaL.fx

<div class="page-break"></div>

### 4xSoft

![4xSoft](../images/shaders/4xSoft.png)

Filename: 4xSoft.fx

<div class="page-break"></div>

### 4xSoft_PS3.0

![4xSoft_PS3.0](../images/shaders/4xSoft_PS3.0.png)

Filename: 4xSoft_PS3.0.fx

<div class="page-break"></div>

### 5xBR-v3.5b

![5xBR-v3.5b](../images/shaders/5xBR-v3.5b.png)

Filename: 5xBR-v3.5b.fx

<div class="page-break"></div>

### 5xBR-v3.8a

![5xBR-v3.8a](../images/shaders/5xBR-v3.8a.png)

Filename: 5xBR-v3.8a.fx

<div class="page-break"></div>

### 5xBR-v3.8b

![5xBR-v3.8b](../images/shaders/5xBR-v3.8b.png)

Filename: 5xBR-v3.8b.fx

<div class="page-break"></div>

### 5xBR-v3.8c

![5xBR-v3.8c](../images/shaders/5xBR-v3.8c.png)

Filename: 5xBR-v3.8c.fx

<div class="page-break"></div>

### AdvancedAA

![AdvancedAA](../images/shaders/AdvancedAA.png)

Filename: AdvancedAA.fx

<div class="page-break"></div>

### bilinear

![bilinear](../images/shaders/bilinear.png)

Filename: bilinear.fx

<div class="page-break"></div>

### Cartoon

![Cartoon](../images/shaders/Cartoon.png)

Filename: Cartoon.fx

<div class="page-break"></div>

### ColorSketch

![ColorSketch](../images/shaders/ColorSketch.png)

Filename: ColorSketch.fx

<div class="page-break"></div>

### CRT.D3D.br

![CRT.D3D.br](../images/shaders/CRT.D3D.br.png)

Filename: CRT.D3D.br.fx

<div class="page-break"></div>

### CRT.D3D

![CRT.D3D](../images/shaders/CRT.D3D.png)

Filename: CRT.D3D.fx

<div class="page-break"></div>

### CRT-geom-blend

![CRT-geom-blend](../images/shaders/CRT-geom-blend.png)

Filename: CRT-geom-blend.fx

<div class="page-break"></div>

### CRT-geom-curved

![CRT-geom-curved](../images/shaders/CRT-geom-curved.png)

Filename: CRT-geom-curved.fx

<div class="page-break"></div>

### CRT-simple.D3D.br

![CRT-simple.D3D.br](../images/shaders/CRT-simple.D3D.br.png)

Filename: CRT-simple.D3D.br.fx

<div class="page-break"></div>

### CRT-simple.D3D

![CRT-simple.D3D](../images/shaders/CRT-simple.D3D.png)

Filename: CRT-simple.D3D.fx

<div class="page-break"></div>

### DotnBloom.D3D

![DotnBloom.D3D](../images/shaders/DotnBloom.D3D.png)

Filename: DotnBloom.D3D.fx

<div class="page-break"></div>

### EGAfilter

![EGAfilter](../images/shaders/EGAfilter.png)

Filename: EGAfilter.fx

<div class="page-break"></div>

### GS2x

![GS2x](../images/shaders/GS2x.png)

Filename: GS2x.fx

<div class="page-break"></div>

### GS2xFilter

![GS2xFilter](../images/shaders/GS2xFilter.png)

Filename: GS2xFilter.fx

<div class="page-break"></div>

### Gs2xLS

![Gs2xLS](../images/shaders/Gs2xLS.png)

Filename: Gs2xLS.fx

<div class="page-break"></div>

### Gs2xSmartFilter

![Gs2xSmartFilter](../images/shaders/Gs2xSmartFilter.png)

Filename: Gs2xSmartFilter.fx

<div class="page-break"></div>

### GS2xSuper

![GS2xSuper](../images/shaders/GS2xSuper.png)

Filename: GS2xSuper.fx

<div class="page-break"></div>

### GS2xTwo

![GS2xTwo](../images/shaders/GS2xTwo.png)

Filename: GS2xTwo.fx

<div class="page-break"></div>

### GS4x

![GS4x](../images/shaders/GS4x.png)

Filename: GS4x.fx

<div class="page-break"></div>

### GS4xColorScale

![GS4xColorScale](../images/shaders/GS4xColorScale.png)

Filename: GS4xColorScale.fx

<div class="page-break"></div>

### GS4xFilter

![GS4xFilter](../images/shaders/GS4xFilter.png)

Filename: GS4xFilter.fx

<div class="page-break"></div>

### GS4xHqFilter

![GS4xHqFilter](../images/shaders/GS4xHqFilter.png)

Filename: GS4xHqFilter.fx

<div class="page-break"></div>

### GS4xScale

![GS4xScale](../images/shaders/GS4xScale.png)

Filename: GS4xScale.fx

<div class="page-break"></div>

### GS4xSoft

![GS4xSoft](../images/shaders/GS4xSoft.png)

Filename: GS4xSoft.fx

<div class="page-break"></div>

### HQ2x

![HQ2x](../images/shaders/HQ2x.png)

Filename: HQ2x.fx

<div class="page-break"></div>

### Lanczos

![Lanczos](../images/shaders/Lanczos.png)

Filename: Lanczos.fx

<div class="page-break"></div>

### Lanczos12

![Lanczos12](../images/shaders/Lanczos12.png)

Filename: Lanczos12.fx

<div class="page-break"></div>

### Lanczos16

![Lanczos16](../images/shaders/Lanczos16.png)

Filename: Lanczos16.fx

<div class="page-break"></div>

### Matrix

![Matrix](../images/shaders/Matrix.png)

Filename: Matrix.fx

<div class="page-break"></div>

### MCAmber

![MCAmber](../images/shaders/MCAmber.png)

Filename: MCAmber.fx

<div class="page-break"></div>

### MCGreen

![MCGreen](../images/shaders/MCGreen.png)

Filename: MCGreen.fx

<div class="page-break"></div>

### MCHerc

![MCHerc](../images/shaders/MCHerc.png)

Filename: MCHerc.fx

<div class="page-break"></div>

### MCOrange

![MCOrange](../images/shaders/MCOrange.png)

Filename: MCOrange.fx

<div class="page-break"></div>

### none

![none](../images/shaders/none.png)

Filename: none.fx

<div class="page-break"></div>

### point

![point](../images/shaders/point.png)

Filename: point.fx

<div class="page-break"></div>

### scale2x

![scale2x](../images/shaders/scale2x.png)

Filename: scale2x.fx

<div class="page-break"></div>

### scale2x_ps14

![scale2x_ps14](../images/shaders/scale2x_ps14.png)

Filename: scale2x_ps14.fx

<div class="page-break"></div>

### Scale2xPlus

![Scale2xPlus](../images/shaders/Scale2xPlus.png)

Filename: Scale2xPlus.fx

<div class="page-break"></div>

### Scale4x

![Scale4x](../images/shaders/Scale4x.png)

Filename: Scale4x.fx

<div class="page-break"></div>

### SimpleAA

![SimpleAA](../images/shaders/SimpleAA.png)

Filename: SimpleAA.fx

<div class="page-break"></div>

### Sketch

![Sketch](../images/shaders/Sketch.png)

Filename: Sketch.fx

<div class="page-break"></div>

### Super2xSaI

![Super2xSaI](../images/shaders/Super2xSaI.png)

Filename: Super2xSaI.fx

<div class="page-break"></div>

### SuperEagle

![SuperEagle](../images/shaders/SuperEagle.png)

Filename: SuperEagle.fx

<div class="page-break"></div>

### Tv

![Tv](../images/shaders/Tv.png)

Filename: Tv.fx

<div class="page-break"></div>
