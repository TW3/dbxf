

![DBXF Logo](../images/dbxf/dbxf.png)


# DBXF Documentation

## FAQ - Frequently asked questions

###  Overview:

This document aims to answer frequently asked questions about DBXF.

<div class="page-break"></div>

### Topics:

This document will cover the following :

### DBXF related questions:

* **I see a Z instead of a C at the prompt.**
* **Do I always have to type these commands? Automation?**
* **How do I change to full screen?**
* **My CD-ROM doesn't work.**
* **The game\application can't find its CD-ROM.**
* **The mouse doesn't work.**
* **There is no sound.**
* **MIDI input or output does not work.**
* **The sound stutters or sounds stretched\weird.**
* **The keyboard lags.**
* **The cursor always moves into one direction!**
* **Can DBXF harm my computer?**
* **I would like to change the memory size, cpu speed, ems or soundblaster IRQ.**
* **What sound hardware does DBXF presently emulate?**
* **Great! But I still don't get it.**

### Project development related questions:

* **What advantages does DBXF have over Dosbox-X?**
* **Why create _another_ fork of DOSBox?**
* **How often will DBXF be released?**
* **I want to contribute to DBXF development. How can I help?**
* **Will DBXF be ported to other platforms?**
* **Why has DBXF moved away from Github?**

### DBXF related questions

At some point, this question and answer FAQ section was removed from Dosbox-X. Here it is restored; with only changes which are relevant to DBXF.

### I see a Z instead of a C at the prompt.

You have to make your directories available as drives in DBXF by using
the `mount` command. For example, in Windows `mount C D:\GAMES` will give
you a C drive in DBXF which points to your Windows D:\GAMES directory.
To move into the mounted drive, type `C:` If everything went
fine, DBXF will display `C:\>` at the prompt.

### Do I always have to type these commands? Automation?

Inside the DBXF configuration file is an [autoexec] section. The commands
present there are run when DBXF starts. You can place the mount command within the autoexec section and it will run every time DBXF is started.

### How do I change to full screen?

Press alt + enter. Alternatively; Edit DBXF's configuration file and
change the option fullscreen=false to fullscreen=true. If the full screen output looks wrong to you: Play with the option fullresolution inside the
configuration file. To return to windowed mode and leave full screen mode:
Press alt + enter again.

The technical term for this method is called _toggling_.

<div class="page-break"></div>

### My CD-ROM does not work.

To mount a physical (real) CD-ROM drive in DBXF you have to specify some additional options when mounting the device.

To enable CD-ROM support (includes MSCDEX):

```
mount d f:\ -t cdrom
```

To enable low-level CD-ROM-support (uses ioctl if possible):

```
mount d f:\ -t cdrom -usecd 0
```

To enable low-level SDL-support:

```
mount d f:\ -t cdrom -usecd 0 -noioctl
```

To enable low-level aspi-support (win98 with aspi-layer installed):

```
mount d f:\ -t cdrom -usecd 0 -aspi
```

In the above commands:

- **d** is the new drive letter you will allocate inside DBXF.
- **f:\** is the Windows device identifier of the CD-ROM on your PC (as displayed inside Windows Explorer or "My Computer.")
- **0** is the internal number allocated to the CD-ROM drive, (as reported by **mount -cd**)

### The game\application can't find its CD-ROM.

Be sure to mount the CD-ROM with the `-t cdrom` switch. This will enable the
MSCDEX interface required by DOS games to interface with CD-ROMs.
Also try adding the correct label. `-label LABEL` To enable lower-level
CD-ROM support, add the following switch to mount: `-usecd #`, where **#** is
the number of your CD-ROM drive reported by the DBXF command: `mount -cd`

### The mouse doesn't work.

The game or application you are trying to run using DBXF likely does not support mouse input.

### There is no sound.

First, ensure that sound has not been accidentally muted or disabled by the operating system or accidental key press.

Be sure that the sound is correctly configured within the game or using it's installation or setup program. This might be done during the installation or within a setup/setsound utility which accompanies the game. Check to see if an autodetection option is provided. If there is no autodetection, try selecting soundblaster or soundblaster16 with the default
settings being: `address=220 irq=7 dma=1` You might also want to select
midi at the address **330** as the music output device.

The parameters of emulated soundcards can be changed within the DOSBox
configuration file.
If you still don't get any sound, set the core setting to normal and use a lower fixed cycles value (such as `cycles=2000`)

<div class="page-break"></div>

### MIDI input or output does not work.

By default, DBXF will try to automatically select the first output device. To override this, try with `midiconfig=1` or use `MIXER /LISTMIDI` to locate and select the midi output device you require.

**WARNING:** MIDI input is a new experimental feature. It may or may not work at all. Please report any bugs you find. Thanks.

Input is disabled by default unless you change the `inconfig` parameter. If unsure, try with `inconfig=0` to enable MIDI input.

With the `midioptions` setting you can select an emulated MIDI device which receives MIDI input. Available devices are: **inputmpu401**, **inputsbuart**, **inputgus** and **inputauto**.
Other options are separated by a comma. The **norealtime** option disables output of realtime MIDI messages, the **passthrough** option enables MIDI passthrough and the **clockout** option allows output of MIDI clock real time messages (only works without **norealtime**).

### The sound stutters or sounds stretched\weird.

You could be using too much CPU power to keep DOSBox running at the current speed. You can lower the cycles, skip frames, reduce the sampling rate of
the respective sound device (see the DBXF reference configuration file.) You can also increase the prebuffer inside the config file.

If you are using `cycles=max` or `cycles=auto` then make sure that there are no rogue background processes interfering! (especially if they access a storage device.)

### The keyboard lags.

Lower the priority setting inside the DBXF configuration file
using the setting: `priority=normal` You might also want to
try lowering the number of cycles.

### The cursor always moves into one direction!

Test to see if it still happens if you disable the joystick emulation,
set `joysticktype=none` inside the **[joystick]** section of your DBXF
configuration file.

You can also try unplugging any joystick.
If you want to use the joystick in the game, try setting `timed=false`
and be sure to calibrate the joystick (both in your OS as well as
in the game or the game's setup).

### Can DBXF harm my computer?

DBXF cannot harm your computer any more than any other resource demanding
software application.

Increasing the cycles does not overclock your real CPU.
Setting the cycles too high has a negative performance effect upon the
software running inside DBXF.

### I would like to change the memory size, cpu speed, ems or soundblaster IRQ.

This is possible! Open the file CONF/dbxf.reference.conf using a text editor and search for the option(s) you would like to change.

### What sound hardware does DBXF presently emulate?

DBXF currently emulates the following legacy sound devices:

- **Internal PC speaker** :
This emulation includes both the tone generator and several forms of
digital sound output through the internal speaker.

- **Tandy 3 voice** :
The emulation of this sound hardware is complete with the exception of
the noise channel. The noise channel is not very well documented and as
such is only a best guess as to the sound's accuracy.

- **Tandy DAC** :
Emulation of the Tandy DAC utilizes the soundblaster emulation, thus
be sure the soundblaster is not disabled inside the DBXF configuration
file. The Tandy DAC is only emulated at the BIOS level.

- **Adlib** :
Borrowed from MAME, this emulation is almost perfect and includes the
Adlib's ability to almost play digitized sound.

- **SoundBlaster 16/ SoundBlaster Pro I & II /SoundBlaster I & II** :
By default DBXF provides Soundblaster 16 level 16-bit stereo sound.
You can select a different SoundBlaster version using a configuration file.

- **Creative CMS/Gameblaster** :
The default configuration places it onto port 0x220.  It should be noted that enabling this along with Adlib emulation may result in conflicts.

- **Disney Soundsource** :
Using the printer port, this sound device outputs digital sound only.

<div class="page-break"></div>

- **Gravis Ultrasound** :
The emulation of this hardware is nearly complete and can emulate midi output. Installing the GUS drivers for DOS into DBXF is required for this emulation to function.

- **MPU-401** :
A MIDI passthrough interface is also emulated.  This method of sound
output will only work when used with a General Midi device.

### Great! But I still don't get it.

Take a look at "The Newbie's pictorial guide to DOSBox" located at:
[https://www.vogons.org/viewtopic.php?t=2502](https://www.vogons.org/viewtopic.php?t=2502) might help you.

Also try the DOSBox wiki: [http://dosbox.sourceforge.net/wiki/](http://dosbox.sourceforge.net/wiki/)
or check DBXF's documentation: [https://gitlab.com/TW3/dbxf/tree/master/docs](https://gitlab.com/TW3/dbxf/tree/master/docs)

<div class="page-break"></div>

### Project development related questions

Information about the rationale behind some project decisions and such.

### What advantages does DBXF have over Dosbox-X?

- Working built-in multi-threaded fluidsynth midi audio output via [libfluidsynth](http://www.fluidsynth.org/).
- Working built-in multi-threaded MT32 midi audio output via [libmt32emu](https://github.com/munt/munt).
- Windows builds optimized for Windows 10 and built against the latest Windows 10 SDK.
- DBXF releases build clean (please check the [build log](https://ci.appveyor.com/project/heydojo/dbxf/branch/master) for warnings or errors.) DBXF attempts to not suppress compiler warnings to [hide hacks](https://github.com/joncampbell123/dosbox-x/blob/master/vs2015/config.h#L340) and [poor code choices](https://github.com/joncampbell123/dosbox-x/commit/65343fc3e06110e6a535cd1826301781238f1dd2).
- Binary releases are created in a transparent and reproducible way (via [Appveyor](https://www.appveyor.com/).)
- DBXF is fully documented, including [screen shots](https://gitlab.com/TW3/dbxf/blob/master/docs/Screenshots.pdf), [pixel shader reference](https://gitlab.com/TW3/dbxf/blob/master/docs/Pixel%20Shaders.pdf) and a [user guide](https://gitlab.com/TW3/dbxf/blob/master/docs/User%20guide.pdf).
- The DBXF application window spawns in the centre of the graphical view port of the display (as any decent application should.)
- DBXF ships with a simple system for running DOS applications using multiple configurations.
- The control scheme (input mapper) layout does not change "*[whenever the](https://github.com/joncampbell123/dosbox-x/commit/7eb7cdb557c324461ce16c9df89d0b65f8bcae5c#diff-3b1f3c818385e56283d07b63d93a2ee7) [wind blows](https://github.com/joncampbell123/dosbox-x/commit/012caca69d44ead2596dde0c54aa154f428d4fcd#diff-3b1f3c818385e56283d07b63d93a2ee7).*" DBXF will never change the default shortcut keys.
- Depreciation is rare and if necessary, there will always be a very long grace period before hand for the majority of instances.
- DBXF is built using Microsoft's mitigation against the [Spectre](https://blogs.msdn.microsoft.com/vcblog/2018/01/15/spectre-mitigations-in-msvc/) vulnerability.

<div class="page-break"></div>

### Why create _another_ fork of DOSBox?

DBXF started as a Windows 10 only fork of Dosbox-X, after discovering that both MT32 and Fluidsynth MIDI output had been disabled in Dosbox-X's windows builds. With Fluidsynth support added to the Windows build; a significant dependency tree had been created. The dependency tree and the changes required to enable fluidsynth in DBXF were large enough that it would be unrealistic to push the changes upstream to Dosbox-X. Doing so would have created an increased maintenance burden on Jon and his team.

Dosbox-X's source code was chosen instead of DOSBox SVN because:
- Dosbox-X's source code is very different from DOSBox.
- Dosbox-X does not use the [untrustworthy sourceforge](https://www.howtogeek.com/218764/warning-don%E2%80%99t-download-software-from-sourceforge-if-you-can-help-it/) to host code and releases.
- Dosbox-X does not use SVN and instead uses the modern and more widely deployed and understood Git source control management.
- Until 2018, a DOSBox binary release was last made previously in 2010.

Features such as fluidsynth output are often disabled in Dosbox-X Windows builds. The primary goal of Dosbox-X's emulation is accuracy. And to that end, absolute accuracy. What this means is that Dosbox-X includes undesirable bugs and behaviour which were found within the original hardware and software. This is deliberate and important to Dosbox-X's main developer.

While respected and most certainly admirable, DBXF does not inherit Dosbox-X's development focus. Windows support, usability and "Just make it work®" are (currently) the main focuses of DBXF development.

(What was unknown at the time of the fork was that Jon was about to make huge sweeping changes to the UI code, breaking compatibility with frontends and breaking
some parts of the application which would take months to fix. The timing of the DBXF fork's creation couldn't have been more important.)

### How often will DBXF be released?

The project is more or less in maintenance mode and is not accepting major patches. Patches which fix security vulnerabilities or major bugs
will be accepted within reason. Please send a merge request via your fork of the project on Gitlab.

Releases will be made "when ready." But ideally, depending on traction and code churn, a quarterly, half year or yearly release schedule may be kept.

Releases will (currently) only be made for Windows 10.

### I want to contribute to DBXF development. How can I help?

Simply fork [https://gitlab.com/TW3/dbxf.git](https://gitlab.com/TW3/dbxf.git) on Gitlab, switch to the staging branch, make your changes and then propose a merge request into the DBXF staging branch via Gitlab.
It would help if you also take the time to read the [Developer guidelines](https://gitlab.com/TW3/dbxf/blob/staging/docs/src/Developer%20guidelines.md), as it will ensure that your changes are expediated into the next release.

### Will DBXF be ported to other platforms?

Apple Macintosh OSX will never be supported. If your operating system is not 64bit Windows 10, then you want [Dosbox-X](http://dosbox-x.com/) and not DBXF.

Windows 10 - 64 bit is currently the only supported development platform. A Linux, [Wayland](https://wayland.freedesktop.org/) only, [Flatpak](https://www.flatpak.org/) version may be considered sometime in the future...

### Why has DBXF moved away from Github?

Firstly, it was simply the right time for a change.

Having evaluated [Gitlab](https://gitlab.com/), it is clear that it is a superior platform and the next logical step for the wider free and open source software eco-system to migrate to.

It has become apparent that Github is a social network for programmers, which also happens to host source code for them. The focus is obviously on the social features which are used to present a watered down version of dev ops work flows, resulting in a low barrier of entry to programmers who are new to dev ops.
Github is great. It is just primitive due to it's social focus.

On 04/06/2018 Microsoft confirmed their [acquisition of Github](https://news.microsoft.com/2018/06/04/microsoft-to-acquire-github-for-7-5-billion/).

Although DBXF targets Microsoft Windows and is built using Microsoft's Visual Studio build chain, DBXF's source code is free software. DBXF is licensed using the [GNU GPL](https://gitlab.com/TW3/dbxf/blob/master/LICENSE).
Due to the acquisition of Github by Microsoft a conflict of interest has been created.

Microsoft's history runs counter to the ideals, direction and leadership of DBXF. For historical examples of Microsoft and their _pedigree_ as a Corporation please research the following:

* **DirectX VS Open GL** (a developer lock in attempt which succeeded and arguably held cross platform PC gaming back for more than 10 years.)

* **Internet Explorer failing to follow web standards to try to create a vendor lock in** (only thwarted by the Mozilla Foundation and their open source web browser: Firefox.)

* **United States v. Microsoft Corporation antitrust law case** (Microsoft were convicted, found to have broken the law and forced to pay a hefty fine.)

<div class="page-break"></div>

* **Microsoft CEO Steve Ballmer publically called Linux "Cancer"** (and more specifically because of the license used by the Linux kernel. DBXF uses this exact same license.)

* **The re-engineering of Skype to the dismay of it's loyal user base** (it is hard to argue that anyone is using Skype nowadays unless they are a business customer and their IT department has mandated to do so.)

* **Collusion with Novell and others to create software patent pacts** (plenty of companies are doing this, it is a form of protection racket and customers should be fully aware that this company is engaging in this activity.)

* **Microsoft vs odf file format** (creating vendor lock in means people _have_ to buy your software, if they want to do the _thing_. This is obviously monopolistic, anti-social and regressive behaviour. Literally the exact opposite of the ideals behind free and open source software.)

* **Built in telemetry data in Windows** (from Alexa bundled with XP, to the call home features built into 10. Personally identifiable data is big business now. Fortunately, the GDPR goes some way towards thwarting the collection of this data. Although it does not go far enough in it's protections. Unfortunately, Americans and the rest of the world outside of Europe have no such protection. Data collection of any kind without consent is both ethically and morally wrong. The industry practice of doing so will end in good time and not only because it is a matter of national security for every single nation in the world.)

In short, Microsoft's current behaviour may seem positive _now_ but due to their very extensive chequered past and the fact that the CEO could change at anytime, it does not make any sense to place any trust in Microsoft. They became so anti-competitive and monopolistic that they were found to have broken the law.

A well known saying is that a Leopard doesn't change it's spots. Microsoft are certainly not the exception to that rule.
