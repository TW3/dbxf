

![DBXF Logo](../images/dbxf/dbxf.png)


# DBXF Documentation

## Advanced usage tips

### Topics:

This document will cover the following :

* **Overview**
* **Quick tips**
	* Sound
	* MIDI
* **Floppy image swapping**
* **CD-ROM image swapping**
* **Setup and installation routines**


###  Overview:

This is currently a working document in development.
Please don't take the current contents of this document as a final draft.
This message will be removed once the first draft of the document is complete.

This document is quite sparse. You can help by contributing to it.

Info about this document here.

### Quick tips:

Some quick tips here.

#### Sound

SB16 will work for most games but if you want to use Gravis ultrasound, MT32 or general midi (GM) output, some additional configuration and or files will be required.

#### MIDI

MIXER /LISTMIDI

### Floppy image swapping:

If you are booting from floppy using the BOOT command
(but not yet the IMGMOUNT command), you can use the
SwapFloppy key combination through the mapper to swap
between floppies.

This key combination is CTRL+F4 by default, but can be changed through the mapper interface.

### CD-ROM image swapping:

IMGMOUNT can be used to attach multiple ISO images to
a CD-ROM drive. At first, the first ISO image is presented
to the guest as the CD-ROM drive. To emulate the act of
removing the CD and putting in the next one, use the
SwapCD key combination through the mapper.

This key combination is CTRL+F3 by default, but can be changed through the mapper interface.

To provide a realistic CD change delay, CD switching is
emulated as first the removal of the CD-ROM media from
the drive, followed by a 4 second delay, followed by the
emulation of inserting the new CD and waiting for the
CD-ROM drive to spin up and read it. This delay is
necessary for some OSes like Windows 95 who cannot
detect CD changes if the changeover happens too fast.

### Setup and installation routines:

Some info about setup and installation routines.

#### Appendix:

> \* *Some explanation of something here.*
