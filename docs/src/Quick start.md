

![DBXF Logo](../images/dbxf/dbxf.png)


# DBXF Documentation

## Quick Start Guide

### Topics:

This document will cover the following :

* **Overview**
* **Recommended Specs**
* **Downloading DBXF**
* **Installation**
* **Running DBXF for the first time**

###  Overview:

DBXF is a Windows 10 application for emulating DOS based computer programs such as games, demos and other software.
This document will cover obtaining a copy of DBXF and running the application for the first time.

### Recommended Specs:

To run DBXF; your personal computer should match or exceed the following specifications:

* Dual Core 64 bit or greater modern CPU which supports [AVX2](https://en.wikipedia.org/wiki/Advanced_Vector_Extensions#CPUs_with_AVX2) (Intel or AMD)
* 2 gigabytes or more RAM installed
* 20 MB free hard disc space
* 720p or greater display monitor
* Microsoft Windows 10 operating system (64 bit Home, Pro or Enterprise) not in S mode (scanty mode)

In most cases, a modern desktop or laptop computer which has Windows 10 64 bit installed can run DBXF.

### Downloading DBXF:

DBXF releases are available for download from the Gitlab project page.
Visit the following link :
[https://tw3.gitlab.io/dbxf/](https://tw3.gitlab.io/dbxf/) to obtain the latest version.

### Installation:

Once you have downloaded a copy of the release archive for the latest version of DBXF, extract\* the archive somewhere onto the hard disc drive of your computer.
A recommended location to do this is C:\Programs\DBXF\ but you can save your copy of DBXF anywhere you like.

<div class="page-break"></div>

### Running DBXF for the first time:

Once the release archive has been extracted and the DBXF folder created, navigate into the DBXF folder, double click on the file DBXF.exe. The application should start.

![DBXF Application Window](../images/dbxf/main.PNG)

**That's all there is to it!**

You are now ready to run some DOS based programs using DBXF. To get the most out of DBXF; please also read the user guide.

#### Appendix:

> \* *To extract the archive, either use Windows explorer's built in compressed file viewer to copy and paste the folder into the location on your hard disk or alternatively download and install [7-Zip](https://www.7-zip.org/). You can open the .zip archive using 7-Zip's file manager. With the file manager open, you can extract the DBXF folder using drag and drop.
> With 7-Zip installed, the archive can also be extracted by right clicking on the .zip archive and selecting one of the 7-Zip extraction options which appear in the menu.*
