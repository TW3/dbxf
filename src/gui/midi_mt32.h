
#ifndef DOSBOX_MIDI_MT32_H
#define DOSBOX_MIDI_MT32_H


#include "mt32emu.h"
#include <SDL_thread.h>
#include <SDL_timer.h>
#include "mixer.h"
#include "control.h"

class RingBuffer
{
private:
  static const unsigned int bufferSize = 1024;
  volatile unsigned int startpos;
  volatile unsigned int endpos;
  Bit64u ringBuffer[bufferSize];

public:
    RingBuffer ()
  {
    startpos = 0;
    endpos = 0;
  }

  bool put (Bit32u data)
  {
    unsigned int newEndpos = endpos;
    newEndpos++;
    if (newEndpos == bufferSize)
      newEndpos = 0;
    if (startpos == newEndpos)
      return false;
    ringBuffer[endpos] = data;
    endpos = newEndpos;
    return true;
  }

  Bit32u get ()
  {
    if (startpos == endpos)
      return 0;
    Bit32u data = (Bit32u) ringBuffer[startpos];	/* <- FIXME: Um.... really? */
    startpos++;
    if (startpos == bufferSize)
      startpos = 0;
    return data;
  }
  void reset ()
  {
    startpos = 0;
    endpos = 0;
  }
};

static class MidiHandler_mt32:public MidiHandler
{
private:
  static const Bitu MIXER_BUFFER_SIZE = MIXER_BUFSIZE >> 2;
  MixerChannel *chan;
    MT32Emu::Synth * synth;
  RingBuffer midiBuffer;
  SDL_Thread *thread;
  SDL_mutex *synthMutex;
  SDL_semaphore *procIdleSem, *mixerReqSem;
  Bit16s mixerBuffer[2 * MIXER_BUFFER_SIZE];
  volatile Bitu mixerBufferSize;
  volatile bool stopProcessing;
  bool open, noise, reverseStereo, renderInThread;
  Bit32u numPartials;
  static void makeROMPathName (char pathName[], const char romDir[],
			       const char fileName[], bool addPathSeparator);
  class MT32ReportHandler:public MT32Emu::ReportHandler
  {
  protected:
    virtual void onErrorControlROM ()
    {
      LOG (LOG_MISC, LOG_WARN) ("MT32: Couldn't open Control ROM file");
    }

    virtual void onErrorPCMROM ()
    {
      LOG (LOG_MISC, LOG_WARN) ("MT32: Couldn't open PCM ROM file");
    }

    virtual void showLCDMessage (const char *message)
    {
      LOG (LOG_MISC, LOG_DEBUG) ("MT32: LCD-Message: %s", message);
    }

    virtual void printDebug (const char *fmt, va_list list);
  } reportHandler;

  static void mixerCallBack (Bitu len);
  static int processingThread (void *);

public:
MidiHandler_mt32 ():chan (NULL), synth (NULL), thread (NULL), synthMutex (NULL),
    procIdleSem (NULL), mixerReqSem (NULL), open (false)
  {
  }

  ~MidiHandler_mt32 ()
  {
    Close ();
  }

  const char *GetName (void)
  {
    return "mt32";
  }

  void user_romhelp (void)
  {
    // Help the user get the ROMs in place so we can work!
    LOG (LOG_MISC,
	 LOG_WARN)
      ("MT32 emulation cannot work without the PCM and CONTROL ROM files.");
    LOG (LOG_MISC,
	 LOG_WARN)
      ("To eliminate this error message, either change mididevice= to something else, or");
    LOG (LOG_MISC,
	 LOG_WARN)
      ("change the romdir in the midi section of DBXF's configuration file");
    LOG (LOG_MISC,
	 LOG_WARN)
      ("to point to the location of a folder containing the ROM files.");
  }

  bool Open (const char *conf)
  {
    LOG (LOG_MISC, LOG_WARN) ("MT32: Starting mt32 MIDI soft synth");
    Section_prop *section =
      static_cast < Section_prop * >(control->GetSection ("midi"));

    // mt32.verbose

    if (strcmp (section->Get_string ("mt32.verbose"), "on") == 0)
      {
	LOG (LOG_MISC, LOG_WARN) ("MT32: Verbose logging enabled");
	noise = strcmp (section->Get_string ("mt32.verbose"), "on") == 0;	// Verbose logging
      }
    else
      {
	noise = false;
      }

    // mt32.romdir

    const char *romDir = section->Get_string ("mt32.romdir");
    if (romDir == NULL)
      romDir = "./MT32";	// Paranoid NULL-check, should never happen
    size_t romDirLen = strlen (romDir);
    bool addPathSeparator = false;

    if (romDirLen < 1)
      {
	romDir = "./MT32";
      }
    else if (4080 < romDirLen)
      {
	LOG (LOG_MISC,
	     LOG_WARN) ("MT32: mt32.romdir is too long, using the dir MT32.");
	romDir = "./MT32";
      }
    else
      {
	char lastChar = romDir[strlen (romDir) - 1];
	addPathSeparator = lastChar != '/' && lastChar != '\\';
      }

    char pathName[4096];

    MT32Emu::FileStream controlROMFile;
    MT32Emu::FileStream pcmROMFile;

    makeROMPathName (pathName, romDir, "CM32L_CONTROL.ROM", addPathSeparator);
    if (!controlROMFile.open (pathName))
      {
	makeROMPathName (pathName, romDir, "MT32_CONTROL.ROM",
			 addPathSeparator);
	if (!controlROMFile.open (pathName))
	  {
	    LOG (LOG_MISC,
		 LOG_WARN) ("MT32: Could not open the Control ROM file");
	    user_romhelp ();
	    return false;
	  }
	else
	  {
	    if (noise)
	      LOG (LOG_MISC, LOG_WARN) ("MT32: MT32_CONTROL.ROM found");
	  }
      }
    else
      {
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("MT32: CM32L_CONTROL.ROM found");
      }

    makeROMPathName (pathName, romDir, "CM32L_PCM.ROM", addPathSeparator);
    if (!pcmROMFile.open (pathName))
      {
	makeROMPathName (pathName, romDir, "MT32_PCM.ROM", addPathSeparator);
	if (!pcmROMFile.open (pathName))
	  {
	    LOG (LOG_MISC,
		 LOG_WARN) ("MT32: Could not open the PCM ROM file");
	    user_romhelp ();
	    return false;
	  }
	else
	  {
	    if (noise)
	      LOG (LOG_MISC, LOG_WARN) ("MT32: MT32_PCM.ROM found");
	  }
      }
    else
      {
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("MT32: CM32L_PCM.ROM found");
      }

    const MT32Emu::ROMImage * controlROMImage =
      MT32Emu::ROMImage::makeROMImage (&controlROMFile);
    const MT32Emu::ROMImage * pcmROMImage =
      MT32Emu::ROMImage::makeROMImage (&pcmROMFile);

    synth = new MT32Emu::Synth (&reportHandler);

    /* NTS: Read and apply mt32.partials BEFORE opening the synth, because the
       synth assumes an initial number of partials, and allocates PartialManager()
       instances which in turn initialize THAT many partials. If we later
       call setPartialLimit() with the (often lower) number we wanted, then
       a memory leak will occur because the PartialManager() will only free
       the new lower limit and leave the rest in memory. */

    numPartials = section->Get_int ("mt32.partials");
    // if(numPartials>MT32EMU_MAX_PARTIALS) numPartials=MT32EMU_MAX_PARTIALS; // Removed from libmt32emu
    // synth->setPartialLimit(numPartials); // access to setPartialLimit() is now done throught the service interface - which is not being used here

    if (!synth->open (*controlROMImage, *pcmROMImage))
      {
	LOG (LOG_MISC, LOG_WARN) ("MT32: Error  initializing emulation");
	return false;
      }
    else
      {
	LOG (LOG_MISC, LOG_DEBUG) ("MT32: Opened the synth");
      }

    // mt32.reverb.mode mt32.reverb.time mt32.reverb.level

    if (strcmp (section->Get_string ("mt32.reverb.mode"), "auto") != 0)
      {
	Bit8u reverbsysex[] = { 0x10, 0x00, 0x01, 0x00, 0x05, 0x03 };
	reverbsysex[3] =
	  (Bit8u) atoi (section->Get_string ("mt32.reverb.mode"));
	reverbsysex[4] = (Bit8u) section->Get_int ("mt32.reverb.time");
	reverbsysex[5] = (Bit8u) section->Get_int ("mt32.reverb.level");
	synth->writeSysex (16, reverbsysex, 6);
	synth->setReverbOverridden (true);
	LOG (LOG_MISC, LOG_DEBUG) ("MT32: Using custom reverb settings");
      }
    else
      {
	LOG (LOG_MISC, LOG_DEBUG) ("MT32: Using default reverb settings");
      }

    // mt32.dac

    if (atoi (section->Get_string ("mt32.dac")) == 3)
      {
	synth->setDACInputMode ((MT32Emu::DACInputMode) atoi ("3"));
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN)
	    ("MT32: DAC input emulation mode set to GENERATION2");
      }
    else if (atoi (section->Get_string ("mt32.dac")) == 2)
      {
	synth->setDACInputMode ((MT32Emu::DACInputMode) atoi ("2"));
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN)
	    ("MT32: DAC input emulation mode set to GENERATION1");
      }
    else if (atoi (section->Get_string ("mt32.dac")) == 1)
      {
	synth->setDACInputMode ((MT32Emu::DACInputMode) atoi ("1"));
	synth->setReverbOutputGain (0.68f / 2);	// Automatically change the reverb output gain (see dosbox.reference.conf) // PURE mode = 1/2 reverb output
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN) ("MT32: DAC input emulation mode set to Pure");
      }
    else
      {
	synth->setDACInputMode ((MT32Emu::DACInputMode) atoi ("0"));	// The default DAC setting
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN) ("MT32: DAC input emulation mode set to Nice");
      }

    // mt32.reverse.stereo

    if (strcmp (section->Get_string ("mt32.reverse.stereo"), "on") == 0)
      {
	synth->setReversedStereoEnabled (true);
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("MT32: Reverse stereo enabled");
      }
    else
      {
	synth->setReversedStereoEnabled (false);
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("MT32: Reverse stereo disabled");
      }

    // mt32.niceampramp - NEW!

    if (strcmp (section->Get_string ("mt32.niceampramp"), "off") == 0)
      {
	synth->setNiceAmpRampEnabled (false);
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN) ("MT32: Nice amplifier ramp mode disabled");

      }
    else
      {
	synth->setNiceAmpRampEnabled (true);
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("MT32: Nice amplifier ramp mode enabled");
      }

    // mt32.thread

    if (strcmp (section->Get_string ("mt32.thread"), "on") == 0)
      {
	renderInThread = true;
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN)
	    ("MT32: Rendering audio in a separate thread enabled");
      }
    else
      {
	renderInThread = false;
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN)
	    ("MT32: Rendering audio in a separate thread disabled");
      }

    // mt32.rate

    int sampleRate = section->Get_int ("mt32.rate");
    chan = MIXER_AddChannel (mixerCallBack, sampleRate, "MT32");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("MT32: Added mixer channel at sample rate %d",
				sampleRate);

    if (renderInThread)
      {
	mixerBufferSize = 0;
	stopProcessing = false;
	synthMutex = SDL_CreateMutex ();
	procIdleSem = SDL_CreateSemaphore (0);
	mixerReqSem = SDL_CreateSemaphore (0);
	thread = SDL_CreateThread (processingThread, NULL);
	//if (thread == NULL || synthMutex == NULL || sleepMutex == NULL) renderInThread = false;
      }

    chan->Enable (true);
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("MT32: Opening the mixer channel...");
    open = true;
    return true;
  }

  void Close (void)
  {
    if (!open)
      return;
    chan->Enable (false);
    if (renderInThread)
      {
	stopProcessing = true;
	SDL_SemPost (mixerReqSem);
	SDL_WaitThread (thread, NULL);
	thread = NULL;
	SDL_DestroyMutex (synthMutex);
	synthMutex = NULL;
	SDL_DestroySemaphore (procIdleSem);
	procIdleSem = NULL;
	SDL_DestroySemaphore (mixerReqSem);
	mixerReqSem = NULL;
      }
    MIXER_DelChannel (chan);
    chan = NULL;
    synth->close ();
    delete synth;
    synth = NULL;
    open = false;
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("MT32: Closed the mixer channel");
  }

  void PlayMsg (Bit8u * msg)
  {
    //if (!midiBuffer.put(*(Bit32u *)msg | (Bit64u(playPos + AUDIO_BUFFER_SIZE) << 32))) LOG_MSG("MT32: Playback buffer full!");
    if (!midiBuffer.put (*(Bit32u *) msg))
      {
      }				//LOG_MSG("MT32: Playback buffer full!");
  }

  void PlaySysex (Bit8u * sysex, Bitu len)
  {
    if (renderInThread)
      SDL_LockMutex (synthMutex);
    synth->playSysex (sysex, len);
    if (renderInThread)
      SDL_UnlockMutex (synthMutex);
  }

  MT32Emu::Synth * GetSynth ()
  {
    return synth;
  }

  void Reset ()
  {

    midiBuffer.reset ();
    if (renderInThread)
      SDL_LockMutex (synthMutex);
    mixerBufferSize = 0;
    if (renderInThread)
      SDL_UnlockMutex (synthMutex);
  }

private:
  void render (Bitu len, Bit16s * buf)
  {
    Bit32u msg = midiBuffer.get ();
    if (msg != 0)
      synth->playMsg (msg);
    synth->render (buf, len);
    if (reverseStereo)
      {
	Bit16s *revBuf = buf;
	for (Bitu i = 0; i < len; i++)
	  {
	    Bit16s left = revBuf[0];
	    Bit16s right = revBuf[1];
	    *revBuf++ = right;
	    *revBuf++ = left;
	  }
      }
    chan->AddSamples_s16 (len, buf);
  }
}

midiHandlerMt32Run;

void
MidiHandler_mt32::makeROMPathName (char pathName[], const char romDir[],
				   const char fileName[],
				   bool addPathSeparator)
{
  strcpy (pathName, romDir);
  if (addPathSeparator)
    {
      strcat (pathName, "/");
    }
  strcat (pathName, fileName);
}

void
MidiHandler_mt32::MT32ReportHandler::printDebug (const char *fmt,
						 va_list list)
{
  if (midiHandlerMt32Run.noise)
    {
      char
	s[1024];
      strcpy (s, "MT32: ");
      vsnprintf (s + 6, 1017, fmt, list);
      LOG_MSG ("%s", s);
    }
}

void
MidiHandler_mt32::mixerCallBack (Bitu len)
{
  if (midiHandlerMt32Run.renderInThread)
    {
      SDL_SemWait (midiHandlerMt32Run.procIdleSem);
      midiHandlerMt32Run.mixerBufferSize += len;
      SDL_SemPost (midiHandlerMt32Run.mixerReqSem);
    }
  else
    {
      midiHandlerMt32Run.render (len, (Bit16s *) MixTemp);
    }
}

int
MidiHandler_mt32::processingThread (void *)
{
  while (!midiHandlerMt32Run.stopProcessing)
    {
      SDL_SemPost (midiHandlerMt32Run.procIdleSem);
      SDL_SemWait (midiHandlerMt32Run.mixerReqSem);
      for (;;)
	{
	  Bitu samplesToRender = midiHandlerMt32Run.mixerBufferSize;
	  if (samplesToRender == 0)
	    break;
	  SDL_LockMutex (midiHandlerMt32Run.synthMutex);
	  midiHandlerMt32Run.render (samplesToRender,
				     midiHandlerMt32Run.mixerBuffer);
	  SDL_UnlockMutex (midiHandlerMt32Run.synthMutex);
	  midiHandlerMt32Run.mixerBufferSize -= samplesToRender;
	}
    }
  return 0;
}

#endif /* DOSBOX_MIDI_MT32_H */
