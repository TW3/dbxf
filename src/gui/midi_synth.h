/*
*  Copyright (C) 2002-2013  The DOSBox Team
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Library General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
#ifdef C_FLUIDSYNTH
#ifndef DOSBOX_MIDI_FLUIDSYNTH_H
#define DOSBOX_MIDI_FLUIDSYNTH_H

#include "fluidsynth.h"
#include <math.h>

#ifndef FLUID_OK
#define FLUID_OK   (0)
#define FLUID_FREE(_p)               free(_p)
#define FLUID_NEW(_t)                (_t*)malloc(sizeof(_t))
#endif
#include <SDL_thread.h>
#include <SDL_timer.h>
// Protect against multiple inclusions
#ifndef MIXER_BUFSIZE
#include "mixer.h"
#endif

#include "control.h"

// http://www.fluidsynth.org/api/index.html | https://sourceforge.net/p/fluidsynth/wiki/FluidSettings/
// This softsynth output module was tested using fluidsynth version 1.1.10

class synthRingBuffer
{
private:
  static const unsigned int bufferSize = 256;
  volatile unsigned int startpos;
  volatile unsigned int endpos;
  Bit64u ringBuffer[bufferSize];

public:
    synthRingBuffer ()
  {
    startpos = 0;
    endpos = 0;
  }

  bool put (Bit8u data)
  {
    unsigned int newEndpos = endpos;
    newEndpos++;
    if (newEndpos == bufferSize)
      newEndpos = 0;
    if (startpos == newEndpos)
      return false;
    ringBuffer[endpos] = data;
    endpos = newEndpos;
    return true;
  }

  Bit8u get ()
  {
    if (startpos == endpos)
      return false;
    Bit8u data = (Bit8u) ringBuffer[startpos];	// <- FIXME: Um.... really? - PLEASE EXPLAIN THIS COMMENT.
    startpos++;
    if (startpos == bufferSize)
      startpos = 0;
    return data;
  }

  void reset ()
  {
    startpos = 0;
    endpos = 0;
  }

};

static class MidiHandler_synth:public MidiHandler
{
private:
  static const Bitu MIXER_BUFFER_SIZE = MIXER_BUFSIZE >> 2;
  MixerChannel *synthchan;
  fluid_settings_t *settings;
  fluid_synth_t *synth_soft;
  synthRingBuffer midiBuffer;
  SDL_Thread *thread;
  SDL_mutex *synthMutex;
  SDL_semaphore *procIdleSem, *mixerReqSem;
  int sfont_id;
  bool open, noise, useBuffer, renderInThread;
  Bit16s mixerBuffer[2 * MIXER_BUFFER_SIZE];
  volatile Bitu mixerBufferSize;
  volatile bool stopProcessing;
  static void synth_log (int level, char *message, void *data);
  static void synth_CallBack (Bitu len);
  static int processingThread (void *);

public:
    MidiHandler_synth ():sfont_id (NULL), synth_soft (NULL), settings (NULL),
    synthchan (NULL), thread (NULL), synthMutex (NULL), procIdleSem (NULL),
    mixerReqSem (NULL), open (false), useBuffer (false)
  {
  };
  const char *GetName (void)
  {
    return "synth";
  };

  bool Open (const char *conf)
  {
    LOG (LOG_MISC, LOG_NORMAL) ("SYNTH: Starting MIDI soft synth");
    Section_prop *mixsection = static_cast < Section_prop * >(control->GetSection ("mixer"));	// Get the settings for the sample rate
    int synthsamplerate = mixsection->Get_int ("rate");

    if (synthsamplerate == 0)
      {
	synthsamplerate = 44100;
      }

    Section_prop *section = static_cast < Section_prop * >(control->GetSection ("midi"));	// Get the settings for the synth

    if (strcmp (section->Get_string ("synth.verbose"), "on") == 0)
      {
	fluid_settings_setstr (settings, "synth.verbose", "yes");
	LOG (LOG_MISC, LOG_WARN) ("SYNTH: Verbose logging enabled");
	noise = true;		// Verbose logging
      }
    else
      {
	noise = false;
      }
    // Sound font file required
    if (strcmp (section->Get_string ("synth.soundfont"), "") == 0)
      {
	LOG_MSG
	  ("SYNTH: ERROR! Specify a .SF2 sound font file using the synth.soundfont= config option");
	return false;
      }

    // Pull the settings and optionally push the values out to the log

    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: sample rate set to %lu",
				(int) synthsamplerate);
    int synthpoly = section->Get_int ("synth.polyphony");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: sample polyphony set to %lu",
				(int) synthpoly);
    const char *synthbank = section->Get_string ("synth.bank");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth bank selection \"%s\"",
				synthbank);
    double synthgain = section->Get_double ("synth.gain");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth gain set to %lu",
				(double) synthgain);
    double reverbroom = section->Get_double ("synth.reverb.room");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth reverb room size set to %lu",
				(double) reverbroom);
    double reverbwet = section->Get_double ("synth.reverb.wet");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth reverb wet amount set to %lu",
				(double) reverbwet);
    double reverbwidth = section->Get_double ("synth.reverb.width");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth reverb width set to %lu",
				(double) reverbwidth);
    double reverblevel = section->Get_double ("synth.reverb.level");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth reverb level set to %lu",
				(double) reverblevel);
    int chorusn = section->Get_int ("synth.chorus.n");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth chorus N set to %lu",
				(int) chorusn);
    double choruslevel = section->Get_double ("synth.chorus.level");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth chorus level set to %lu",
				(double) choruslevel);
    double chorusspeed = section->Get_double ("synth.chorus.speed");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth chorus speed set to %lu",
				(double) chorusspeed);
    double chorusdepth = section->Get_double ("synth.chorus.depth");
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth chorus depth set to %lu",
				(double) chorusdepth);
    int chorusmod = 0;		// sine

    if (strcmp (section->Get_string ("synth.chorus.mod"), "triangle") == 0)
      {
	chorusmod = 1;
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN) ("SYNTH: Synth chorus modulation set to triangle");
      }
    else
      {
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN) ("SYNTH: Synth chorus modulation set to sine");
      }

    if (strcmp (section->Get_string ("synth.buffer"), "off") == 0)
      {
	useBuffer = false;
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth MIDI buffer is off");
      }
    else
      {
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: Synth MIDI buffer is on");
      }

    void fluid_log_config ();	// Not part of public API? // FIXME?? - Seems to work without
    fluid_set_log_function (FLUID_PANIC, synth_log, NULL);
    fluid_set_log_function (FLUID_ERR, synth_log, NULL);
    fluid_set_log_function (FLUID_WARN, synth_log, NULL);
    fluid_set_log_function (FLUID_INFO, synth_log, NULL);
    if (noise)
      fluid_set_log_function (FLUID_DBG, synth_log, NULL);

    // Create the settings
    settings = new_fluid_settings ();
    if (settings == NULL)
      {
	LOG (LOG_MISC,
	     LOG_WARN) ("SYNTH: Error allocating MIDI soft synth settings");
	return false;
      }
    else
      {
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN) ("SYNTH: MIDI soft synth settings created");
      }

    fluid_settings_setint (settings, "synth.polyphony", synthpoly);
    fluid_settings_setstr (settings, "synth.midi-bank-select", synthbank);	// gm, gs, xg, mma
    fluid_settings_setstr (settings, "audio.sample-format", "16bits");
    fluid_settings_setnum (settings, "synth.sample-rate",
			   (double) synthsamplerate);
    fluid_settings_setnum (settings, "synth.gain", (double) synthgain);
/*
		// Alternative way of turning on the reverb and chorus filters

		if (strcmp(section->Get_string("synth.reverb"), "on") == 0) {
			fluid_settings_setstr(settings, "synth.reverb.active", "yes");
		if (noise) LOG(LOG_MISC, LOG_WARN)("SYNTH: Reverb enabled");
		}
		else {
			fluid_settings_setstr(settings, "synth.reverb.active", "no");
		}

		if (strcmp(section->Get_string("synth.reverb"), "on") == 0) {
			fluid_settings_setstr(settings, "synth.chorus.active", "yes");
		if (noise) LOG(LOG_MISC, LOG_WARN)("SYNTH: Reverb enabled");
		}
		else {
			fluid_settings_setstr(settings, "synth.chorus.active", "no");
		}
*/
    // Create the synthesizer.
    synth_soft = new_fluid_synth (settings);

    if (synth_soft == NULL)
      {
	LOG (LOG_MISC,
	     LOG_WARN) ("SYNTH: Error initialising MIDI soft synth");
	delete_fluid_settings (settings);
	return false;
      }
    else
      {
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: MIDI soft synth settings loaded");
      }

    const char *sndfont = section->Get_string ("synth.soundfont");

    // Load the SoundFont
    if (fluid_is_soundfont (sndfont))
      {
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: Sound font file is \"%s\"",
				    sndfont);
	// sfont_id = fluid_synth_sfload(synth_soft, sndfont, 0); // All MIDI channel bank and program numbers will not be refreshed
	sfont_id = fluid_synth_sfload (synth_soft, sndfont, 1);	// All MIDI channel bank and program numbers will be refreshed
      }
    else
      {
	LOG_MSG ("SYNTH: Failed to load MIDI sound font file \"%s\"",
		 sndfont);
	delete_fluid_synth (synth_soft);
	delete_fluid_settings (settings);
	return false;
      }

    if (sfont_id == -1)
      {				// Should never reach here
	LOG_MSG ("SYNTH: Failed to load MIDI sound font file \"%s\"",
		 sndfont);
	delete_fluid_synth (synth_soft);
	delete_fluid_settings (settings);
	return false;
      }
    else
      {
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN) ("SYNTH: MIDI soft synth soundfont loaded");
      }

    // Change the settings of the open synth synth_soft

    // Reverb
    if (strcmp (section->Get_string ("synth.reverb"), "on") == 0)
      {
	fluid_synth_set_reverb_on (synth_soft, 1);
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: Reverb enabled");
	// fluid_synth_set_reverb(synth_soft, 0.3, 0, 2.0, 0.6); // This is here to show default values
	fluid_synth_set_reverb (synth_soft, reverbroom, reverbwet,
				reverbwidth, reverblevel);
      }
    else if (strcmp (section->Get_string ("synth.reverb"), "auto") == 0)
      {
	fluid_synth_set_reverb_on (synth_soft, 1);
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: Reverb enabled");
      }
    else
      {
	fluid_synth_set_reverb_on (synth_soft, 0);
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: Reverb disabled");
      }
    // Chorus
    if (strcmp (section->Get_string ("synth.chorus"), "on") == 0)
      {
	fluid_synth_set_chorus_on (synth_soft, 1);
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: Chorus enabled");
	// fluid_synth_set_chorus(synth_soft, 15, 0.12, 0.31, 7.8, 0); // This is here to show default values
	fluid_synth_set_chorus (synth_soft, chorusn, choruslevel, chorusspeed,
				chorusdepth, chorusmod);
      }
    else if (strcmp (section->Get_string ("synth.chorus"), "auto") == 0)
      {
	fluid_synth_set_chorus_on (synth_soft, 1);
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: Chorus enabled");
      }
    else
      {
	fluid_synth_set_chorus_on (synth_soft, 0);
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: Chorus disabled");
      }
    // Thread
    if (strcmp (section->Get_string ("synth.thread"), "on") == 0)
      {
	renderInThread = true;
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN)
	    ("SYNTH: Rendering audio in a separate thread enabled");
      }
    else
      {
	renderInThread = false;
	if (noise)
	  LOG (LOG_MISC,
	       LOG_WARN)
	    ("SYNTH: Rendering audio in a separate thread disabled");
      }

    synthchan = MIXER_AddChannel (synth_CallBack, synthsamplerate, "SYNTH");
    if (noise)
      LOG (LOG_MISC,
	   LOG_WARN) ("SYNTH: MIDI soft synth mixer channel created");

    synthchan->Enable (true);
    if (noise)
      LOG (LOG_MISC, LOG_WARN) ("SYNTH: MIDI soft synth mixer channel ready");

    if (renderInThread)
      {
	mixerBufferSize = 0;
	stopProcessing = false;
	synthMutex = SDL_CreateMutex ();
	procIdleSem = SDL_CreateSemaphore (0);
	mixerReqSem = SDL_CreateSemaphore (0);
	thread = SDL_CreateThread (processingThread, NULL);
      }

    open = true;
    return true;
  };

  void Reset ()
  {

    midiBuffer.reset ();
    if (renderInThread)
      SDL_LockMutex (synthMutex);
    mixerBufferSize = 0;
    if (renderInThread)
      SDL_UnlockMutex (synthMutex);
  };

  void Close (void)
  {

    if (!open)
      return;
    synthchan->Enable (false);
    if (renderInThread)
      {
	stopProcessing = true;
	SDL_SemPost (mixerReqSem);
	SDL_WaitThread (thread, NULL);
	thread = NULL;
	SDL_DestroyMutex (synthMutex);
	synthMutex = NULL;
	SDL_DestroySemaphore (procIdleSem);
	procIdleSem = NULL;
	SDL_DestroySemaphore (mixerReqSem);
	mixerReqSem = NULL;
      }
    MIXER_DelChannel (synthchan);
    synthchan = NULL;
    delete_fluid_synth (synth_soft);
    delete_fluid_settings (settings);
    synth_soft = NULL;
    settings = NULL;
    Reset ();
    open = false;

    LOG (LOG_MISC, LOG_NORMAL) ("SYNTH: MIDI soft synth closed");

  };

  // Fluidsynth midiMessageParser based on PlayEvent by heftig based on GStreamer's fluidsynthmidi plugin
  // https://github.com/GStreamer/gst-plugins-bad/blob/master/ext/fluidsynth/gstfluiddec.c#L390-L470
  // https://github.com/joncampbell123/dosbox-x/pull/646/commits/792ae67f87b0ede2c27bc45272f95363c4281192

  void midiMessageParse (Bit8u * msg, Bitu len)
  {
    Bit8u event = msg[0], channel, p1, p2;

    switch (event)
      {
      case 0xf0:
      case 0xf7:
	if (noise)
	  LOG (LOG_MISC, LOG_DEBUG) ("SYNTH: sysex 0x%02x len %lu",
				     (int) event, (long unsigned) len);
	fluid_synth_sysex (synth_soft, (char *) (msg + 1), len - 1, NULL,
			   NULL, NULL, 0);
	return;
      case 0xf9:
	if (noise)
	  LOG (LOG_MISC, LOG_DEBUG) ("SYNTH: midi tick");
	return;
      case 0xff:
	if (noise)
	  LOG (LOG_MISC, LOG_DEBUG) ("SYNTH: system reset");
	fluid_synth_system_reset (synth_soft);
	return;
      case 0xf1:
      case 0xf2:
      case 0xf3:
      case 0xf4:
      case 0xf5:
      case 0xf6:
      case 0xf8:
      case 0xfa:
      case 0xfb:
      case 0xfc:
      case 0xfd:
      case 0xfe:
	if (noise)
	  LOG (LOG_MISC, LOG_WARN) ("SYNTH: unhandled event 0x%02x",
				    (int) event);
	return;
      }

    channel = event & 0xf;
    p1 = len > 1 ? msg[1] : 0;
    p2 = len > 2 ? msg[2] : 0;

    if (noise)
      LOG (LOG_MISC,
	   LOG_DEBUG) ("SYNTH: event 0x%02x channel %d, 0x%02x 0x%02x",
		       (int) event, (int) channel, (int) p1, (int) p2);

    switch (event & 0xf0)
      {
      case 0x80:
	fluid_synth_noteoff (synth_soft, channel, p1);
	break;
      case 0x90:
	fluid_synth_noteon (synth_soft, channel, p1, p2);
	break;
      case 0xb0:
	fluid_synth_cc (synth_soft, channel, p1, p2);
	break;
      case 0xc0:
	fluid_synth_program_change (synth_soft, channel, p1);
	break;
      case 0xd0:
	fluid_synth_channel_pressure (synth_soft, channel, p1);
	break;
      case 0xe0:
	fluid_synth_pitch_bend (synth_soft, channel, (p2 << 7) | p1);
	break;
      }
  };

  void hopper (Bit8u * msg, Bitu len)
  {

    if (renderInThread)
      SDL_LockMutex (synthMutex);
    // Patches to improve the buffer and parser code welcome
    Bitu i;
    if (useBuffer)
      {
	// TODO: Maybe add an option to step the buffer forward one before sending output? Downside is that would add latency to the audio.
	if (!midiBuffer.put (*(Bit8u *) msg))
	  {
	    if (noise)
	      LOG (LOG_MISC, LOG_WARN) ("SYNTH: Playback buffer full!");
	  }

	Bit8u *rBuff = msg;
	*msg = midiBuffer.get ();

	if (msg != 0)
	  {

	    len = MIDI_evt_len[*msg];
	    for (i = 0; i < len; i++)
	      {
		midiMessageParse (msg, len);	// let the parser convert the data into events
		// if (noise) LOG(LOG_MISC, LOG_WARN)("SYNTH: Parsed MIDI message array"); // Disabled because this introduces a lot of noise into the logs - Only enable it if you _really _ need to.
	      }
	  }
      }
    else
      {
	if (msg != 0)
	  {
	    // let the parser convert the data into events
	    for (i = 0; i < len; i++)
	      {
		midiMessageParse (msg, len);
	      }
	  }
      }

    if (renderInThread)
      SDL_UnlockMutex (synthMutex);

  };

  void PlayMsg (Bit8u * msg)
  {

    Bitu len;
    len = MIDI_evt_len[*msg];
    hopper (msg, len);

  };

  void PlaySysex (Bit8u * sysex, Bitu len)
  {
    if (renderInThread)
      SDL_LockMutex (synthMutex);
    hopper (sysex, len);
    if (renderInThread)
      SDL_UnlockMutex (synthMutex);
  };

private:
  void render (Bitu len, Bit16s * buf)
  {
    fluid_synth_write_s16 (synth_soft, len, buf, 0, 2, buf, 1, 2);
    synthchan->AddSamples_s16 (len, buf);
  }

} midiHandlerSynthRun;

void
MidiHandler_synth::synth_log (int level, char *message, void *data)
{				// return true?
  switch (level)
    {
    case FLUID_PANIC:
    case FLUID_ERR:
      LOG (LOG_ALL, LOG_ERROR) (message);
      break;

    case FLUID_WARN:
      LOG (LOG_ALL, LOG_WARN) (message);
      break;

    case FLUID_DBG:
      LOG (LOG_ALL, LOG_DEBUG) (message);
      break;

    default:
      LOG (LOG_ALL, LOG_NORMAL) (message);
      break;
    }
}

void
MidiHandler_synth::synth_CallBack (Bitu len)
{
  if (midiHandlerSynthRun.synth_soft != NULL)
    {
      if (midiHandlerSynthRun.renderInThread)
	{
	  SDL_SemWait (midiHandlerSynthRun.procIdleSem);
	  midiHandlerSynthRun.mixerBufferSize += len;
	  SDL_SemPost (midiHandlerSynthRun.mixerReqSem);
	}
      else
	{
	  midiHandlerSynthRun.render (len, (Bit16s *) MixTemp);
	}
    }
}

int
MidiHandler_synth::processingThread (void *)
{
  while (!midiHandlerSynthRun.stopProcessing)
    {
      SDL_SemPost (midiHandlerSynthRun.procIdleSem);
      SDL_SemWait (midiHandlerSynthRun.mixerReqSem);
      for (;;)
	{
	  Bitu samplesToRender = midiHandlerSynthRun.mixerBufferSize;
	  if (samplesToRender == 0)
	    break;
	  SDL_LockMutex (midiHandlerSynthRun.synthMutex);
	  midiHandlerSynthRun.render (samplesToRender,
				      midiHandlerSynthRun.mixerBuffer);
	  SDL_UnlockMutex (midiHandlerSynthRun.synthMutex);
	  midiHandlerSynthRun.mixerBufferSize -= samplesToRender;
	}
    }
  return 0;
}

#endif /* DOSBOX_MIDI_FLUIDSYNTH_H */
#endif /* C_FLUIDSYNTH */
