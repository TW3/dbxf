/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2012 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

#ifndef _SDL_mutex_h
#define _SDL_mutex_h

/** @file SDL_mutex.h
 *  Functions to provide thread synchronization primitives
 *
 *  @note These are independent of the other SDL routines.
 */

#include "SDL_stdinc.h"
#include "SDL_error.h"

/** 
 *  @def DECLSPEC
 *  Some compilers use a special export keyword
 */
#ifndef DECLSPEC
# if defined(__BEOS__) || defined(__HAIKU__)
#  if defined(__GNUC__)
#   define DECLSPEC
#  else
#   define DECLSPEC	__declspec(export)
#  endif
# elif defined(__WIN32__)
#  ifdef __BORLANDC__
#   ifdef BUILD_SDL
#    define DECLSPEC
#   else
#    define DECLSPEC	__declspec(dllimport)
#   endif
#  else
#   define DECLSPEC	__declspec(dllexport)
#  endif
# elif defined(__OS2__)
#  ifdef __WATCOMC__
#   ifdef BUILD_SDL
#    define DECLSPEC	__declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  elif defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   ifdef BUILD_SDL
#    define DECLSPEC    __declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  else
#   define DECLSPEC
#  endif
# else
#  if defined(__GNUC__) && __GNUC__ >= 4
#   define DECLSPEC	__attribute__ ((visibility("default")))
#  else
#   define DECLSPEC
#  endif
# endif
#endif

/** 
 *  @def SDLCALL
 *  By default SDL uses the C calling convention
 */
#ifndef SDLCALL
# if defined(__WIN32__) && !defined(__GNUC__)
#  define SDLCALL __cdecl
# elif defined(__OS2__)
#  if defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   define SDLCALL _cdecl
#  else
#				/* On other compilers on OS/2, we use the _System calling convention */
#				/* to be compatible with every compiler */
#   define SDLCALL _System
#  endif
# else
#  define SDLCALL
# endif
#endif // SDLCALL

#ifdef __SYMBIAN32__
#ifndef EKA2
#undef DECLSPEC
#define DECLSPEC
#elif !defined(__WINS__)
#undef DECLSPEC
#define DECLSPEC __declspec(dllexport)
#endif // !EKA2
#endif // __SYMBIAN32__

/**
 *  Force structure packing at 4 byte alignment.
 *  This is necessary if the header is included in code which has structure
 *  packing set to an alternate value, say for loading structures from disk.
 *  The packing is reset to the previous value in close_code.h 
 */
#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#ifdef _M_X64
/* Use 8-byte alignment on 64-bit architectures, so pointers are aligned */
#pragma pack(push,8)
#else
#pragma pack(push,4)
#endif
#elif (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=mac68k4byte
#pragma enumsalwaysint on
#endif // Compiler needs structure packing set

/**
 *  @def SDL_INLINE_OKAY
 *  Set up compiler-specific options for inlining functions
 */
#ifndef SDL_INLINE_OKAY
#ifdef __GNUC__
#define SDL_INLINE_OKAY
#else
/* Add any special compiler-specific cases here */
#if defined(_MSC_VER) || defined(__BORLANDC__) || \
    defined(__DMC__) || defined(__SC__) || \
    defined(__WATCOMC__) || defined(__LCC__) || \
    defined(__DECC) || defined(__EABI__)
#ifndef __inline__
#define __inline__	__inline
#endif
#define SDL_INLINE_OKAY
#else
#if !defined(__MRC__) && !defined(_SGI_SOURCE)
#ifndef __inline__
#define __inline__ inline
#endif
#define SDL_INLINE_OKAY
#endif // Not a funky compiler
#endif // Visual C++
#endif // GNU C
#endif // SDL_INLINE_OKAY

/**
 *  @def __inline__
 *  If inlining isn't supported, remove "__inline__", turning static
 *  inlined functions into static functions (resulting in code bloat
 *  in all files which include the offending header files)
 */
#ifndef SDL_INLINE_OKAY
#define __inline__
#endif

/**
 *  @def NULL
 *  Apparently this is needed by several Windows compilers
 */
#if !defined(__MACH__)
#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif // NULL
#endif // ! Mac OS X - breaks precompiled headers

/* Set up for C function definitions, even when using C++ */
#ifdef __cplusplus
extern "C"
{
#endif

/** Synchronization functions which can time out return this value
 *  if they time out.
 */
#define SDL_MUTEX_TIMEDOUT	1

/** This is the timeout value which corresponds to never time out */
#define SDL_MUTEX_MAXWAIT	(~(Uint32)0)


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  /** @name Mutex functions                                        *//*@{ */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** The SDL mutex structure, defined in SDL_mutex.c */
  struct SDL_mutex;
  typedef struct SDL_mutex SDL_mutex;

/** Create a mutex, initialized unlocked */
  extern DECLSPEC SDL_mutex *SDLCALL SDL_CreateMutex (void);

#define SDL_LockMutex(m)	SDL_mutexP(m)
/** Lock the mutex
 *  @return 0, or -1 on error
 */
  extern DECLSPEC int SDLCALL SDL_mutexP (SDL_mutex * mutex);

#define SDL_UnlockMutex(m)	SDL_mutexV(m)
/** Unlock the mutex
 *  @return 0, or -1 on error
 *
 *  It is an error to unlock a mutex that has not been locked by
 *  the current thread, and doing so results in undefined behavior.
 */
  extern DECLSPEC int SDLCALL SDL_mutexV (SDL_mutex * mutex);

/** Destroy a mutex */
  extern DECLSPEC void SDLCALL SDL_DestroyMutex (SDL_mutex * mutex);

/*@}*/

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  /** @name Semaphore functions                                    *//*@{ */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** The SDL semaphore structure, defined in SDL_sem.c */
  struct SDL_semaphore;
  typedef struct SDL_semaphore SDL_sem;

/** Create a semaphore, initialized with value, returns NULL on failure. */
  extern DECLSPEC SDL_sem *SDLCALL SDL_CreateSemaphore (Uint32 initial_value);

/** Destroy a semaphore */
  extern DECLSPEC void SDLCALL SDL_DestroySemaphore (SDL_sem * sem);

/**
 * This function suspends the calling thread until the semaphore pointed 
 * to by sem has a positive count. It then atomically decreases the semaphore
 * count.
 */
  extern DECLSPEC int SDLCALL SDL_SemWait (SDL_sem * sem);

/** Non-blocking variant of SDL_SemWait().
 *  @return 0 if the wait succeeds,
 *  SDL_MUTEX_TIMEDOUT if the wait would block, and -1 on error.
 */
  extern DECLSPEC int SDLCALL SDL_SemTryWait (SDL_sem * sem);

/** Variant of SDL_SemWait() with a timeout in milliseconds, returns 0 if
 *  the wait succeeds, SDL_MUTEX_TIMEDOUT if the wait does not succeed in
 *  the allotted time, and -1 on error.
 *
 *  On some platforms this function is implemented by looping with a delay
 *  of 1 ms, and so should be avoided if possible.
 */
  extern DECLSPEC int SDLCALL SDL_SemWaitTimeout (SDL_sem * sem, Uint32 ms);

/** Atomically increases the semaphore's count (not blocking).
 *  @return 0, or -1 on error.
 */
  extern DECLSPEC int SDLCALL SDL_SemPost (SDL_sem * sem);

/** Returns the current count of the semaphore */
  extern DECLSPEC Uint32 SDLCALL SDL_SemValue (SDL_sem * sem);

/*@}*/

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  /** @name Condition_variable_functions                           *//*@{ */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*@{*/
/** The SDL condition variable structure, defined in SDL_cond.c */
  struct SDL_cond;
  typedef struct SDL_cond SDL_cond;
/*@}*/

/** Create a condition variable */
  extern DECLSPEC SDL_cond *SDLCALL SDL_CreateCond (void);

/** Destroy a condition variable */
  extern DECLSPEC void SDLCALL SDL_DestroyCond (SDL_cond * cond);

/** Restart one of the threads that are waiting on the condition variable,
 *  @return 0 or -1 on error.
 */
  extern DECLSPEC int SDLCALL SDL_CondSignal (SDL_cond * cond);

/** Restart all threads that are waiting on the condition variable,
 *  @return 0 or -1 on error.
 */
  extern DECLSPEC int SDLCALL SDL_CondBroadcast (SDL_cond * cond);

/** Wait on the condition variable, unlocking the provided mutex.
 *  The mutex must be locked before entering this function!
 *  The mutex is re-locked once the condition variable is signaled.
 *  @return 0 when it is signaled, or -1 on error.
 */
  extern DECLSPEC int SDLCALL SDL_CondWait (SDL_cond * cond, SDL_mutex * mut);

/** Waits for at most 'ms' milliseconds, and returns 0 if the condition
 *  variable is signaled, SDL_MUTEX_TIMEDOUT if the condition is not
 *  signaled in the allotted time, and -1 on error.
 *  On some platforms this function is implemented by looping with a delay
 *  of 1 ms, and so should be avoided if possible.
 */
  extern DECLSPEC int SDLCALL SDL_CondWaitTimeout (SDL_cond * cond,
						   SDL_mutex * mutex,
						   Uint32 ms);

/*@}*/

/* Ends C function definitions when using C++ */
#ifdef __cplusplus
}
#endif

#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__WATCOMC__)  || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#if (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=reset
#pragma enumsalwaysint reset
#else
#pragma pack(pop)
#endif
#endif				// Compiler needs structure packing set

#endif				/* _SDL_mutex_h */
