/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2012 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

/** @file SDL_joystick.h
 *  Include file for SDL joystick event handling
 */

#ifndef _SDL_joystick_h
#define _SDL_joystick_h

#include "SDL_stdinc.h"
#include "SDL_error.h"

/** 
 *  @def DECLSPEC
 *  Some compilers use a special export keyword
 */
#ifndef DECLSPEC
# if defined(__BEOS__) || defined(__HAIKU__)
#  if defined(__GNUC__)
#   define DECLSPEC
#  else
#   define DECLSPEC	__declspec(export)
#  endif
# elif defined(__WIN32__)
#  ifdef __BORLANDC__
#   ifdef BUILD_SDL
#    define DECLSPEC
#   else
#    define DECLSPEC	__declspec(dllimport)
#   endif
#  else
#   define DECLSPEC	__declspec(dllexport)
#  endif
# elif defined(__OS2__)
#  ifdef __WATCOMC__
#   ifdef BUILD_SDL
#    define DECLSPEC	__declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  elif defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   ifdef BUILD_SDL
#    define DECLSPEC    __declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  else
#   define DECLSPEC
#  endif
# else
#  if defined(__GNUC__) && __GNUC__ >= 4
#   define DECLSPEC	__attribute__ ((visibility("default")))
#  else
#   define DECLSPEC
#  endif
# endif
#endif

/** 
 *  @def SDLCALL
 *  By default SDL uses the C calling convention
 */
#ifndef SDLCALL
# if defined(__WIN32__) && !defined(__GNUC__)
#  define SDLCALL __cdecl
# elif defined(__OS2__)
#  if defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   define SDLCALL _cdecl
#  else
#				/* On other compilers on OS/2, we use the _System calling convention */
#				/* to be compatible with every compiler */
#   define SDLCALL _System
#  endif
# else
#  define SDLCALL
# endif
#endif // SDLCALL

#ifdef __SYMBIAN32__
#ifndef EKA2
#undef DECLSPEC
#define DECLSPEC
#elif !defined(__WINS__)
#undef DECLSPEC
#define DECLSPEC __declspec(dllexport)
#endif // !EKA2
#endif // __SYMBIAN32__

/**
 *  Force structure packing at 4 byte alignment.
 *  This is necessary if the header is included in code which has structure
 *  packing set to an alternate value, say for loading structures from disk.
 *  The packing is reset to the previous value in close_code.h 
 */
#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#ifdef _M_X64
/* Use 8-byte alignment on 64-bit architectures, so pointers are aligned */
#pragma pack(push,8)
#else
#pragma pack(push,4)
#endif
#elif (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=mac68k4byte
#pragma enumsalwaysint on
#endif // Compiler needs structure packing set

/**
 *  @def SDL_INLINE_OKAY
 *  Set up compiler-specific options for inlining functions
 */
#ifndef SDL_INLINE_OKAY
#ifdef __GNUC__
#define SDL_INLINE_OKAY
#else
/* Add any special compiler-specific cases here */
#if defined(_MSC_VER) || defined(__BORLANDC__) || \
    defined(__DMC__) || defined(__SC__) || \
    defined(__WATCOMC__) || defined(__LCC__) || \
    defined(__DECC) || defined(__EABI__)
#ifndef __inline__
#define __inline__	__inline
#endif
#define SDL_INLINE_OKAY
#else
#if !defined(__MRC__) && !defined(_SGI_SOURCE)
#ifndef __inline__
#define __inline__ inline
#endif
#define SDL_INLINE_OKAY
#endif // Not a funky compiler
#endif // Visual C++
#endif // GNU C
#endif // SDL_INLINE_OKAY

/**
 *  @def __inline__
 *  If inlining isn't supported, remove "__inline__", turning static
 *  inlined functions into static functions (resulting in code bloat
 *  in all files which include the offending header files)
 */
#ifndef SDL_INLINE_OKAY
#define __inline__
#endif

/**
 *  @def NULL
 *  Apparently this is needed by several Windows compilers
 */
#if !defined(__MACH__)
#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif // NULL
#endif // ! Mac OS X - breaks precompiled headers

/* Set up for C function definitions, even when using C++ */
#ifdef __cplusplus
extern "C"
{
#endif

/** @file SDL_joystick.h
 *  @note In order to use these functions, SDL_Init() must have been called
 *        with the SDL_INIT_JOYSTICK flag.  This causes SDL to scan the system
 *        for joysticks, and load appropriate drivers.
 */

/** The joystick structure used to identify an SDL joystick */
  struct _SDL_Joystick;
  typedef struct _SDL_Joystick SDL_Joystick;

/* Function prototypes */
/**
 * Count the number of joysticks attached to the system
 */
  extern DECLSPEC int SDLCALL SDL_NumJoysticks (void);

/**
 * Get the implementation dependent name of a joystick.
 *
 * This can be called before any joysticks are opened.
 * If no name can be found, this function returns NULL.
 */
  extern DECLSPEC const char *SDLCALL SDL_JoystickName (int device_index);

/**
 * Open a joystick for use.
 *
 * @param[in] device_index
 * The index passed as an argument refers to
 * the N'th joystick on the system.  This index is the value which will
 * identify this joystick in future joystick events.
 *
 * @return This function returns a joystick identifier, or NULL if an error occurred.
 */
  extern DECLSPEC SDL_Joystick *SDLCALL SDL_JoystickOpen (int device_index);

/**
 * Returns 1 if the joystick has been opened, or 0 if it has not.
 */
  extern DECLSPEC int SDLCALL SDL_JoystickOpened (int device_index);

/**
 * Get the device index of an opened joystick.
 */
  extern DECLSPEC int SDLCALL SDL_JoystickIndex (SDL_Joystick * joystick);

/**
 * Get the number of general axis controls on a joystick
 */
  extern DECLSPEC int SDLCALL SDL_JoystickNumAxes (SDL_Joystick * joystick);

/**
 * Get the number of trackballs on a joystick
 *
 * Joystick trackballs have only relative motion events associated
 * with them and their state cannot be polled.
 */
  extern DECLSPEC int SDLCALL SDL_JoystickNumBalls (SDL_Joystick * joystick);

/**
 * Get the number of POV hats on a joystick
 */
  extern DECLSPEC int SDLCALL SDL_JoystickNumHats (SDL_Joystick * joystick);

/**
 * Get the number of buttons on a joystick
 */
  extern DECLSPEC int SDLCALL SDL_JoystickNumButtons (SDL_Joystick *
						      joystick);

/**
 * Update the current state of the open joysticks.
 *
 * This is called automatically by the event loop if any joystick
 * events are enabled.
 */
  extern DECLSPEC void SDLCALL SDL_JoystickUpdate (void);

/**
 * Enable/disable joystick event polling.
 *
 * If joystick events are disabled, you must call SDL_JoystickUpdate()
 * yourself and check the state of the joystick when you want joystick
 * information.
 *
 * @param[in] state The state can be one of SDL_QUERY, SDL_ENABLE or SDL_IGNORE.
 */
  extern DECLSPEC int SDLCALL SDL_JoystickEventState (int state);

/**
 * Get the current state of an axis control on a joystick
 *
 * @param[in] axis The axis indices start at index 0.
 *
 * @return The state is a value ranging from -32768 to 32767.
 */
  extern DECLSPEC Sint16 SDLCALL SDL_JoystickGetAxis (SDL_Joystick * joystick,
						      int axis);

/**
 *  @name Hat Positions
 *  The return value of SDL_JoystickGetHat() is one of the following positions:
 */
/*@{*/
#define SDL_HAT_CENTERED	0x00
#define SDL_HAT_UP		0x01
#define SDL_HAT_RIGHT		0x02
#define SDL_HAT_DOWN		0x04
#define SDL_HAT_LEFT		0x08
#define SDL_HAT_RIGHTUP		(SDL_HAT_RIGHT|SDL_HAT_UP)
#define SDL_HAT_RIGHTDOWN	(SDL_HAT_RIGHT|SDL_HAT_DOWN)
#define SDL_HAT_LEFTUP		(SDL_HAT_LEFT|SDL_HAT_UP)
#define SDL_HAT_LEFTDOWN	(SDL_HAT_LEFT|SDL_HAT_DOWN)
/*@}*/

/** 
 *  Get the current state of a POV hat on a joystick
 *
 *  @param[in] hat The hat indices start at index 0.
 */
  extern DECLSPEC Uint8 SDLCALL SDL_JoystickGetHat (SDL_Joystick * joystick,
						    int hat);

/**
 * Get the ball axis change since the last poll
 *
 * @param[in] ball The ball indices start at index 0.
 *
 * @return This returns 0, or -1 if you passed it invalid parameters.
 */
  extern DECLSPEC int SDLCALL SDL_JoystickGetBall (SDL_Joystick * joystick,
						   int ball, int *dx,
						   int *dy);

/**
 * Get the current state of a button on a joystick
 *
 * @param[in] button The button indices start at index 0.
 */
  extern DECLSPEC Uint8 SDLCALL SDL_JoystickGetButton (SDL_Joystick *
						       joystick, int button);

/**
 * Close a joystick previously opened with SDL_JoystickOpen()
 */
  extern DECLSPEC void SDLCALL SDL_JoystickClose (SDL_Joystick * joystick);


/* Ends C function definitions when using C++ */
#ifdef __cplusplus
}
#endif

#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__WATCOMC__)  || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#if (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=reset
#pragma enumsalwaysint reset
#else
#pragma pack(pop)
#endif
#endif				// Compiler needs structure packing set

#endif				/* _SDL_joystick_h */
