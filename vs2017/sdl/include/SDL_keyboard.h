/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2012 Sam Lantinga
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
    Sam Lantinga
    slouken@libsdl.org
*/

/** @file SDL_keyboard.h
 *  Include file for SDL keyboard event handling
 */

#ifndef _SDL_keyboard_h
#define _SDL_keyboard_h

#include "SDL_config.h"
#include "SDL_stdinc.h"
#include "SDL_error.h"
#include "SDL_keysym.h"

/** 
 *  @def DECLSPEC
 *  Some compilers use a special export keyword
 */
#ifndef DECLSPEC
# if defined(__BEOS__) || defined(__HAIKU__)
#  if defined(__GNUC__)
#   define DECLSPEC
#  else
#   define DECLSPEC	__declspec(export)
#  endif
# elif defined(__WIN32__)
#  ifdef __BORLANDC__
#   ifdef BUILD_SDL
#    define DECLSPEC
#   else
#    define DECLSPEC	__declspec(dllimport)
#   endif
#  else
#   define DECLSPEC	__declspec(dllexport)
#  endif
# elif defined(__OS2__)
#  ifdef __WATCOMC__
#   ifdef BUILD_SDL
#    define DECLSPEC	__declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  elif defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   ifdef BUILD_SDL
#    define DECLSPEC    __declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  else
#   define DECLSPEC
#  endif
# else
#  if defined(__GNUC__) && __GNUC__ >= 4
#   define DECLSPEC	__attribute__ ((visibility("default")))
#  else
#   define DECLSPEC
#  endif
# endif
#endif

/** 
 *  @def SDLCALL
 *  By default SDL uses the C calling convention
 */
#ifndef SDLCALL
# if defined(__WIN32__) && !defined(__GNUC__)
#  define SDLCALL __cdecl
# elif defined(__OS2__)
#  if defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   define SDLCALL _cdecl
#  else
#				/* On other compilers on OS/2, we use the _System calling convention */
#				/* to be compatible with every compiler */
#   define SDLCALL _System
#  endif
# else
#  define SDLCALL
# endif
#endif // SDLCALL

#ifdef __SYMBIAN32__
#ifndef EKA2
#undef DECLSPEC
#define DECLSPEC
#elif !defined(__WINS__)
#undef DECLSPEC
#define DECLSPEC __declspec(dllexport)
#endif // !EKA2
#endif // __SYMBIAN32__

/**
 *  Force structure packing at 4 byte alignment.
 *  This is necessary if the header is included in code which has structure
 *  packing set to an alternate value, say for loading structures from disk.
 *  The packing is reset to the previous value in close_code.h 
 */
#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#ifdef _M_X64
/* Use 8-byte alignment on 64-bit architectures, so pointers are aligned */
#pragma pack(push,8)
#else
#pragma pack(push,4)
#endif
#elif (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=mac68k4byte
#pragma enumsalwaysint on
#endif // Compiler needs structure packing set

/**
 *  @def SDL_INLINE_OKAY
 *  Set up compiler-specific options for inlining functions
 */
#ifndef SDL_INLINE_OKAY
#ifdef __GNUC__
#define SDL_INLINE_OKAY
#else
/* Add any special compiler-specific cases here */
#if defined(_MSC_VER) || defined(__BORLANDC__) || \
    defined(__DMC__) || defined(__SC__) || \
    defined(__WATCOMC__) || defined(__LCC__) || \
    defined(__DECC) || defined(__EABI__)
#ifndef __inline__
#define __inline__	__inline
#endif
#define SDL_INLINE_OKAY
#else
#if !defined(__MRC__) && !defined(_SGI_SOURCE)
#ifndef __inline__
#define __inline__ inline
#endif
#define SDL_INLINE_OKAY
#endif // Not a funky compiler
#endif // Visual C++
#endif // GNU C
#endif // SDL_INLINE_OKAY

/**
 *  @def __inline__
 *  If inlining isn't supported, remove "__inline__", turning static
 *  inlined functions into static functions (resulting in code bloat
 *  in all files which include the offending header files)
 */
#ifndef SDL_INLINE_OKAY
#define __inline__
#endif

/**
 *  @def NULL
 *  Apparently this is needed by several Windows compilers
 */
#if !defined(__MACH__)
#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif // NULL
#endif // ! Mac OS X - breaks precompiled headers

/* Set up for C function definitions, even when using C++ */
#ifdef __cplusplus
extern "C"
{
#endif

/** Keysym structure
 *
 *  - The scancode is hardware dependent, and should not be used by general
 *    applications.  If no hardware scancode is available, it will be 0.
 *
 *  - The 'unicode' translated character is only available when character
 *    translation is enabled by the SDL_EnableUNICODE() API.  If non-zero,
 *    this is a UNICODE character corresponding to the keypress.  If the
 *    high 9 bits of the character are 0, then this maps to the equivalent
 *    ASCII character:
 *      @code
 *	char ch;
 *	if ( (keysym.unicode & 0xFF80) == 0 ) {
 *		ch = keysym.unicode & 0x7F;
 *	} else {
 *		An international character..
 *	}
 *      @endcode
 */
  typedef struct SDL_keysym
  {
    Uint8 scancode;			/**< hardware specific scancode */
    SDLKey sym;				/**< SDL virtual keysym */
    SDLMod mod;				/**< current key modifiers */
    Uint16 unicode;			/**< translated character */
#if defined(_WIN32)
    Uint32 win32_vk;		/**< Windows virtual key */
#endif
#if defined(SDL_VIDEO_DRIVER_X11)
    Uint32 x11_sym;	/**< X11 sym */
#endif
  } SDL_keysym;

/** This is the mask which refers to all hotkey bindings */
#define SDL_ALL_HOTKEYS		0xFFFFFFFF

/* Function prototypes */
/**
 * Enable/Disable UNICODE translation of keyboard input.
 *
 * This translation has some overhead, so translation defaults off.
 *
 * @param[in] enable
 * If 'enable' is 1, translation is enabled.
 * If 'enable' is 0, translation is disabled.
 * If 'enable' is -1, the translation state is not changed.
 *
 * @return It returns the previous state of keyboard translation.
 */
  extern DECLSPEC int SDLCALL SDL_EnableUNICODE (int enable);

#define SDL_DEFAULT_REPEAT_DELAY	500
#define SDL_DEFAULT_REPEAT_INTERVAL	30
/**
 * Enable/Disable keyboard repeat.  Keyboard repeat defaults to off.
 *
 *  @param[in] delay
 *  'delay' is the initial delay in ms between the time when a key is
 *  pressed, and keyboard repeat begins.
 *
 *  @param[in] interval
 *  'interval' is the time in ms between keyboard repeat events.
 *
 *  If 'delay' is set to 0, keyboard repeat is disabled.
 */
  extern DECLSPEC int SDLCALL SDL_EnableKeyRepeat (int delay, int interval);
  extern DECLSPEC void SDLCALL SDL_GetKeyRepeat (int *delay, int *interval);

/**
 * Get a snapshot of the current state of the keyboard.
 * Returns an array of keystates, indexed by the SDLK_* syms.
 * Usage:
 *	@code
 * 	Uint8 *keystate = SDL_GetKeyState(NULL);
 *	if ( keystate[SDLK_RETURN] ) //... \<RETURN> is pressed.
 *	@endcode
 */
  extern DECLSPEC Uint8 *SDLCALL SDL_GetKeyState (int *numkeys);

/**
 * Get the current key modifier state
 */
  extern DECLSPEC SDLMod SDLCALL SDL_GetModState (void);

/**
 * Set the current key modifier state.
 * This does not change the keyboard state, only the key modifier flags.
 */
  extern DECLSPEC void SDLCALL SDL_SetModState (SDLMod modstate);

/**
 * Get the name of an SDL virtual keysym
 */
  extern DECLSPEC char *SDLCALL SDL_GetKeyName (SDLKey key);


/* Ends C function definitions when using C++ */
#ifdef __cplusplus
}
#endif

#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__WATCOMC__)  || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#if (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=reset
#pragma enumsalwaysint reset
#else
#pragma pack(pop)
#endif
#endif				// Compiler needs structure packing set

#endif				/* _SDL_keyboard_h */
