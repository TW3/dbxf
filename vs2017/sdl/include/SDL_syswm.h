/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2012 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

/** @file SDL_syswm.h
 *  Include file for SDL custom system window manager hooks
 */

#ifndef _SDL_syswm_h
#define _SDL_syswm_h

#include "SDL_stdinc.h"
#include "SDL_error.h"
#include "SDL_version.h"

/** 
 *  @def DECLSPEC
 *  Some compilers use a special export keyword
 */
#ifndef DECLSPEC
# if defined(__BEOS__) || defined(__HAIKU__)
#  if defined(__GNUC__)
#   define DECLSPEC
#  else
#   define DECLSPEC	__declspec(export)
#  endif
# elif defined(__WIN32__)
#  ifdef __BORLANDC__
#   ifdef BUILD_SDL
#    define DECLSPEC
#   else
#    define DECLSPEC	__declspec(dllimport)
#   endif
#  else
#   define DECLSPEC	__declspec(dllexport)
#  endif
# elif defined(__OS2__)
#  ifdef __WATCOMC__
#   ifdef BUILD_SDL
#    define DECLSPEC	__declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  elif defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   ifdef BUILD_SDL
#    define DECLSPEC    __declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  else
#   define DECLSPEC
#  endif
# else
#  if defined(__GNUC__) && __GNUC__ >= 4
#   define DECLSPEC	__attribute__ ((visibility("default")))
#  else
#   define DECLSPEC
#  endif
# endif
#endif

/** 
 *  @def SDLCALL
 *  By default SDL uses the C calling convention
 */
#ifndef SDLCALL
# if defined(__WIN32__) && !defined(__GNUC__)
#  define SDLCALL __cdecl
# elif defined(__OS2__)
#  if defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   define SDLCALL _cdecl
#  else
#				/* On other compilers on OS/2, we use the _System calling convention */
#				/* to be compatible with every compiler */
#   define SDLCALL _System
#  endif
# else
#  define SDLCALL
# endif
#endif // SDLCALL

#ifdef __SYMBIAN32__
#ifndef EKA2
#undef DECLSPEC
#define DECLSPEC
#elif !defined(__WINS__)
#undef DECLSPEC
#define DECLSPEC __declspec(dllexport)
#endif // !EKA2
#endif // __SYMBIAN32__

/**
 *  Force structure packing at 4 byte alignment.
 *  This is necessary if the header is included in code which has structure
 *  packing set to an alternate value, say for loading structures from disk.
 *  The packing is reset to the previous value in close_code.h 
 */
#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#ifdef _M_X64
/* Use 8-byte alignment on 64-bit architectures, so pointers are aligned */
#pragma pack(push,8)
#else
#pragma pack(push,4)
#endif
#elif (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=mac68k4byte
#pragma enumsalwaysint on
#endif // Compiler needs structure packing set

/**
 *  @def SDL_INLINE_OKAY
 *  Set up compiler-specific options for inlining functions
 */
#ifndef SDL_INLINE_OKAY
#ifdef __GNUC__
#define SDL_INLINE_OKAY
#else
/* Add any special compiler-specific cases here */
#if defined(_MSC_VER) || defined(__BORLANDC__) || \
    defined(__DMC__) || defined(__SC__) || \
    defined(__WATCOMC__) || defined(__LCC__) || \
    defined(__DECC) || defined(__EABI__)
#ifndef __inline__
#define __inline__	__inline
#endif
#define SDL_INLINE_OKAY
#else
#if !defined(__MRC__) && !defined(_SGI_SOURCE)
#ifndef __inline__
#define __inline__ inline
#endif
#define SDL_INLINE_OKAY
#endif // Not a funky compiler
#endif // Visual C++
#endif // GNU C
#endif // SDL_INLINE_OKAY

/**
 *  @def __inline__
 *  If inlining isn't supported, remove "__inline__", turning static
 *  inlined functions into static functions (resulting in code bloat
 *  in all files which include the offending header files)
 */
#ifndef SDL_INLINE_OKAY
#define __inline__
#endif

/**
 *  @def NULL
 *  Apparently this is needed by several Windows compilers
 */
#if !defined(__MACH__)
#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif // NULL
#endif // ! Mac OS X - breaks precompiled headers

/* Set up for C function definitions, even when using C++ */
#ifdef __cplusplus
extern "C"
{
#endif

/** @file SDL_syswm.h
 *  Your application has access to a special type of event 'SDL_SYSWMEVENT',
 *  which contains window-manager specific information and arrives whenever
 *  an unhandled window event occurs.  This event is ignored by default, but
 *  you can enable it with SDL_EventState()
 */
#ifdef SDL_PROTOTYPES_ONLY
  struct SDL_SysWMinfo;
  typedef struct SDL_SysWMinfo SDL_SysWMinfo;
#else

/* This is the structure for custom window manager events */
#if defined(SDL_VIDEO_DRIVER_X11)
#if defined(__APPLE__) && defined(__MACH__)
/* conflicts with Quickdraw.h */
#define Cursor X11Cursor
#endif

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#if defined(__APPLE__) && defined(__MACH__)
/* matches the re-define above */
#undef Cursor
#endif

/** These are the various supported subsystems under UNIX */
  typedef enum
  {
    SDL_SYSWM_X11
  } SDL_SYSWM_TYPE;

/** The UNIX custom event structure */
  struct SDL_SysWMmsg
  {
    SDL_version version;
    SDL_SYSWM_TYPE subsystem;
    union
    {
      XEvent xevent;
    } event;
  };

/** The UNIX custom window manager information structure.
 *  When this structure is returned, it holds information about which
 *  low level system it is using, and will be one of SDL_SYSWM_TYPE.
 */
  typedef struct SDL_SysWMinfo
  {
    SDL_version version;
    SDL_SYSWM_TYPE subsystem;
    union
    {
      struct
      {
	Display *display;		/**< The X11 display */
	Window window;			/**< The X11 display window */
		/** These locking functions should be called around
                 *  any X11 functions using the display variable, 
                 *  but not the gfxdisplay variable.
                 *  They lock the event thread, so should not be
		 *  called around event functions or from event filters.
		 */
	/*@{ */
	void (*lock_func) (void);
	void (*unlock_func) (void);
	/*@} */

		/** @name Introduced in SDL 1.0.2 */
	/*@{ */
	Window fswindow;		/**< The X11 fullscreen window */
	Window wmwindow;		/**< The X11 managed input window */
	/*@} */

		/** @name Introduced in SDL 1.2.12 */
	/*@{ */
	Display *gfxdisplay;		/**< The X11 display to which rendering is done */
	/*@} */
      } x11;
    } info;
  } SDL_SysWMinfo;

#elif defined(SDL_VIDEO_DRIVER_NANOX)
#include <microwin/nano-X.h>

/** The generic custom event structure */
  struct SDL_SysWMmsg
  {
    SDL_version version;
    int data;
  };

/** The windows custom window manager information structure */
  typedef struct SDL_SysWMinfo
  {
    SDL_version version;
    GR_WINDOW_ID window;	/* The display window */
  } SDL_SysWMinfo;

#elif defined(SDL_VIDEO_DRIVER_WINDIB) || defined(SDL_VIDEO_DRIVER_DDRAW) || defined(SDL_VIDEO_DRIVER_GAPI)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

/** The windows custom event structure */
  struct SDL_SysWMmsg
  {
    SDL_version version;
    HWND hwnd;				/**< The window for the message */
    UINT msg;				/**< The type of message */
    WPARAM wParam;			/**< WORD message parameter */
    LPARAM lParam;			/**< LONG message parameter */
  };

/** The windows custom window manager information structure */
  typedef struct SDL_SysWMinfo
  {
    SDL_version version;
    HWND window;			/**< The Win32 display window */
    HWND child_window;			/**< The Win32 surface window (generally, child of window) */
    HGLRC hglrc;			/**< The OpenGL context, if any */
  } SDL_SysWMinfo;

#elif defined(SDL_VIDEO_DRIVER_RISCOS)

/** RISC OS custom event structure */
  struct SDL_SysWMmsg
  {
    SDL_version version;
    int eventCode;		/**< The window for the message */
    int pollBlock[64];
  };

/** The RISC OS custom window manager information structure */
  typedef struct SDL_SysWMinfo
  {
    SDL_version version;
    int wimpVersion;	    /**< Wimp version running under */
    int taskHandle;	    /**< The RISC OS task handle */
    int window;			/**< The RISC OS display window */
  } SDL_SysWMinfo;

#elif defined(SDL_VIDEO_DRIVER_PHOTON)
#include <sys/neutrino.h>
#include <Ph.h>

/** The QNX custom event structure */
  struct SDL_SysWMmsg
  {
    SDL_version version;
    int data;
  };

/** The QNX custom window manager information structure */
  typedef struct SDL_SysWMinfo
  {
    SDL_version version;
    int data;
  } SDL_SysWMinfo;

#else

/** The generic custom event structure */
  struct SDL_SysWMmsg
  {
    SDL_version version;
    int data;
  };

/** The generic custom window manager information structure */
  typedef struct SDL_SysWMinfo
  {
    SDL_version version;
    int data;
  } SDL_SysWMinfo;

#endif				/* video driver type */

#endif				/* SDL_PROTOTYPES_ONLY */

/* Function prototypes */
/**
 * This function gives you custom hooks into the window manager information.
 * It fills the structure pointed to by 'info' with custom information and
 * returns 0 if the function is not implemented, 1 if the function is 
 * implemented and no error occurred, and -1 if the version member of
 * the 'info' structure is not filled in or not supported.
 *
 * You typically use this function like this:
 * @code
 * SDL_SysWMinfo info;
 * SDL_VERSION(&info.version);
 * if ( SDL_GetWMInfo(&info) ) { ... }
 * @endcode
 */
  extern DECLSPEC int SDLCALL SDL_GetWMInfo (SDL_SysWMinfo * info);


/* Ends C function definitions when using C++ */
#ifdef __cplusplus
}
#endif

#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__WATCOMC__)  || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#if (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=reset
#pragma enumsalwaysint reset
#else
#pragma pack(pop)
#endif
#endif				// Compiler needs structure packing set

#endif				/* _SDL_syswm_h */
