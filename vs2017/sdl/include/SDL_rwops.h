/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2012 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

/** @file SDL_rwops.h
 *  This file provides a general interface for SDL to read and write
 *  data sources.  It can easily be extended to files, memory, etc.
 */

#ifndef _SDL_rwops_h
#define _SDL_rwops_h

#include "SDL_stdinc.h"
#include "SDL_error.h"

/** 
 *  @def DECLSPEC
 *  Some compilers use a special export keyword
 */
#ifndef DECLSPEC
# if defined(__BEOS__) || defined(__HAIKU__)
#  if defined(__GNUC__)
#   define DECLSPEC
#  else
#   define DECLSPEC	__declspec(export)
#  endif
# elif defined(__WIN32__)
#  ifdef __BORLANDC__
#   ifdef BUILD_SDL
#    define DECLSPEC
#   else
#    define DECLSPEC	__declspec(dllimport)
#   endif
#  else
#   define DECLSPEC	__declspec(dllexport)
#  endif
# elif defined(__OS2__)
#  ifdef __WATCOMC__
#   ifdef BUILD_SDL
#    define DECLSPEC	__declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  elif defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   ifdef BUILD_SDL
#    define DECLSPEC    __declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  else
#   define DECLSPEC
#  endif
# else
#  if defined(__GNUC__) && __GNUC__ >= 4
#   define DECLSPEC	__attribute__ ((visibility("default")))
#  else
#   define DECLSPEC
#  endif
# endif
#endif

/** 
 *  @def SDLCALL
 *  By default SDL uses the C calling convention
 */
#ifndef SDLCALL
# if defined(__WIN32__) && !defined(__GNUC__)
#  define SDLCALL __cdecl
# elif defined(__OS2__)
#  if defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   define SDLCALL _cdecl
#  else
#				/* On other compilers on OS/2, we use the _System calling convention */
#				/* to be compatible with every compiler */
#   define SDLCALL _System
#  endif
# else
#  define SDLCALL
# endif
#endif // SDLCALL

#ifdef __SYMBIAN32__
#ifndef EKA2
#undef DECLSPEC
#define DECLSPEC
#elif !defined(__WINS__)
#undef DECLSPEC
#define DECLSPEC __declspec(dllexport)
#endif // !EKA2
#endif // __SYMBIAN32__

/**
 *  Force structure packing at 4 byte alignment.
 *  This is necessary if the header is included in code which has structure
 *  packing set to an alternate value, say for loading structures from disk.
 *  The packing is reset to the previous value in close_code.h 
 */
#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#ifdef _M_X64
/* Use 8-byte alignment on 64-bit architectures, so pointers are aligned */
#pragma pack(push,8)
#else
#pragma pack(push,4)
#endif
#elif (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=mac68k4byte
#pragma enumsalwaysint on
#endif // Compiler needs structure packing set

/**
 *  @def SDL_INLINE_OKAY
 *  Set up compiler-specific options for inlining functions
 */
#ifndef SDL_INLINE_OKAY
#ifdef __GNUC__
#define SDL_INLINE_OKAY
#else
/* Add any special compiler-specific cases here */
#if defined(_MSC_VER) || defined(__BORLANDC__) || \
    defined(__DMC__) || defined(__SC__) || \
    defined(__WATCOMC__) || defined(__LCC__) || \
    defined(__DECC) || defined(__EABI__)
#ifndef __inline__
#define __inline__	__inline
#endif
#define SDL_INLINE_OKAY
#else
#if !defined(__MRC__) && !defined(_SGI_SOURCE)
#ifndef __inline__
#define __inline__ inline
#endif
#define SDL_INLINE_OKAY
#endif // Not a funky compiler
#endif // Visual C++
#endif // GNU C
#endif // SDL_INLINE_OKAY

/**
 *  @def __inline__
 *  If inlining isn't supported, remove "__inline__", turning static
 *  inlined functions into static functions (resulting in code bloat
 *  in all files which include the offending header files)
 */
#ifndef SDL_INLINE_OKAY
#define __inline__
#endif

/**
 *  @def NULL
 *  Apparently this is needed by several Windows compilers
 */
#if !defined(__MACH__)
#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif // NULL
#endif // ! Mac OS X - breaks precompiled headers

/* Set up for C function definitions, even when using C++ */
#ifdef __cplusplus
extern "C"
{
#endif

/** This is the read/write operation structure -- very basic */

  typedef struct SDL_RWops
  {
	/** Seek to 'offset' relative to whence, one of stdio's whence values:
	 *	SEEK_SET, SEEK_CUR, SEEK_END
	 *  Returns the final offset in the data source.
	 */
    int (SDLCALL * seek) (struct SDL_RWops * context, int offset, int whence);

	/** Read up to 'maxnum' objects each of size 'size' from the data
	 *  source to the area pointed at by 'ptr'.
	 *  Returns the number of objects read, or -1 if the read failed.
	 */
    int (SDLCALL * read) (struct SDL_RWops * context, void *ptr, int size,
			  int maxnum);

	/** Write exactly 'num' objects each of size 'objsize' from the area
	 *  pointed at by 'ptr' to data source.
	 *  Returns 'num', or -1 if the write failed.
	 */
    int (SDLCALL * write) (struct SDL_RWops * context, const void *ptr,
			   int size, int num);

	/** Close and free an allocated SDL_FSops structure */
    int (SDLCALL * close) (struct SDL_RWops * context);

    Uint32 type;
    union
    {
#if defined(__WIN32__) && !defined(__SYMBIAN32__)
      struct
      {
	int append;
	void *h;
	struct
	{
	  void *data;
	  int size;
	  int left;
	} buffer;
      } win32io;
#endif
#ifdef HAVE_STDIO_H
      struct
      {
	int autoclose;
	FILE *fp;
      } stdio;
#endif
      struct
      {
	Uint8 *base;
	Uint8 *here;
	Uint8 *stop;
      } mem;
      struct
      {
	void *data1;
      } unknown;
    } hidden;

  } SDL_RWops;


/** @name Functions to create SDL_RWops structures from various data sources */
/*@{*/

  extern DECLSPEC SDL_RWops *SDLCALL SDL_RWFromFile (const char *file,
						     const char *mode);

#ifdef HAVE_STDIO_H
  extern DECLSPEC SDL_RWops *SDLCALL SDL_RWFromFP (FILE * fp, int autoclose);
#endif

  extern DECLSPEC SDL_RWops *SDLCALL SDL_RWFromMem (void *mem, int size);
  extern DECLSPEC SDL_RWops *SDLCALL SDL_RWFromConstMem (const void *mem,
							 int size);

  extern DECLSPEC SDL_RWops *SDLCALL SDL_AllocRW (void);
  extern DECLSPEC void SDLCALL SDL_FreeRW (SDL_RWops * area);

/*@}*/

/** @name Seek Reference Points */
/*@{*/
#define RW_SEEK_SET	0	/**< Seek from the beginning of data */
#define RW_SEEK_CUR	1	/**< Seek relative to current read point */
#define RW_SEEK_END	2	/**< Seek relative to the end of data */
/*@}*/

/** @name Macros to easily read and write from an SDL_RWops structure */
/*@{*/
#define SDL_RWseek(ctx, offset, whence)	(ctx)->seek(ctx, offset, whence)
#define SDL_RWtell(ctx)			(ctx)->seek(ctx, 0, RW_SEEK_CUR)
#define SDL_RWread(ctx, ptr, size, n)	(ctx)->read(ctx, ptr, size, n)
#define SDL_RWwrite(ctx, ptr, size, n)	(ctx)->write(ctx, ptr, size, n)
#define SDL_RWclose(ctx)		(ctx)->close(ctx)
/*@}*/

/** @name Read an item of the specified endianness and return in native format */
/*@{*/
  extern DECLSPEC Uint16 SDLCALL SDL_ReadLE16 (SDL_RWops * src);
  extern DECLSPEC Uint16 SDLCALL SDL_ReadBE16 (SDL_RWops * src);
  extern DECLSPEC Uint32 SDLCALL SDL_ReadLE32 (SDL_RWops * src);
  extern DECLSPEC Uint32 SDLCALL SDL_ReadBE32 (SDL_RWops * src);
  extern DECLSPEC Uint64 SDLCALL SDL_ReadLE64 (SDL_RWops * src);
  extern DECLSPEC Uint64 SDLCALL SDL_ReadBE64 (SDL_RWops * src);
/*@}*/

/** @name Write an item of native format to the specified endianness */
/*@{*/
  extern DECLSPEC int SDLCALL SDL_WriteLE16 (SDL_RWops * dst, Uint16 value);
  extern DECLSPEC int SDLCALL SDL_WriteBE16 (SDL_RWops * dst, Uint16 value);
  extern DECLSPEC int SDLCALL SDL_WriteLE32 (SDL_RWops * dst, Uint32 value);
  extern DECLSPEC int SDLCALL SDL_WriteBE32 (SDL_RWops * dst, Uint32 value);
  extern DECLSPEC int SDLCALL SDL_WriteLE64 (SDL_RWops * dst, Uint64 value);
  extern DECLSPEC int SDLCALL SDL_WriteBE64 (SDL_RWops * dst, Uint64 value);
/*@}*/

/* Ends C function definitions when using C++ */
#ifdef __cplusplus
}
#endif

#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__WATCOMC__)  || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#if (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=reset
#pragma enumsalwaysint reset
#else
#pragma pack(pop)
#endif
#endif				// Compiler needs structure packing set

#endif				/* _SDL_rwops_h */
