/*
    SDL - Simple DirectMedia Layer
    Copyright (C) 1997-2012 Sam Lantinga

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    Sam Lantinga
    slouken@libsdl.org
*/

/**
 *  @file SDL_endian.h
 *  Functions for reading and writing endian-specific values
 */

#ifndef _SDL_endian_h
#define _SDL_endian_h

#include "SDL_stdinc.h"

/** @name SDL_ENDIANs
 *  The two types of endianness 
 */
/*@{*/
#define SDL_LIL_ENDIAN	1234
#define SDL_BIG_ENDIAN	4321
/*@}*/

#ifndef SDL_BYTEORDER		/* Not defined in SDL_config.h? */
#ifdef __linux__
#include <endian.h>
#define SDL_BYTEORDER  __BYTE_ORDER
#else /* __linux __ */
#if defined(__hppa__) || \
    defined(__m68k__) || defined(mc68000) || defined(_M_M68K) || \
    (defined(__MIPS__) && defined(__MISPEB__)) || \
    defined(__ppc__) || defined(__POWERPC__) || defined(_M_PPC) || \
    defined(__sparc__)
#define SDL_BYTEORDER	SDL_BIG_ENDIAN
#else
#define SDL_BYTEORDER	SDL_LIL_ENDIAN
#endif
#endif /* __linux __ */
#endif /* !SDL_BYTEORDER */

/** 
 *  @def DECLSPEC
 *  Some compilers use a special export keyword
 */
#ifndef DECLSPEC
# if defined(__BEOS__) || defined(__HAIKU__)
#  if defined(__GNUC__)
#   define DECLSPEC
#  else
#   define DECLSPEC	__declspec(export)
#  endif
# elif defined(__WIN32__)
#  ifdef __BORLANDC__
#   ifdef BUILD_SDL
#    define DECLSPEC
#   else
#    define DECLSPEC	__declspec(dllimport)
#   endif
#  else
#   define DECLSPEC	__declspec(dllexport)
#  endif
# elif defined(__OS2__)
#  ifdef __WATCOMC__
#   ifdef BUILD_SDL
#    define DECLSPEC	__declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  elif defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   ifdef BUILD_SDL
#    define DECLSPEC    __declspec(dllexport)
#   else
#    define DECLSPEC
#   endif
#  else
#   define DECLSPEC
#  endif
# else
#  if defined(__GNUC__) && __GNUC__ >= 4
#   define DECLSPEC	__attribute__ ((visibility("default")))
#  else
#   define DECLSPEC
#  endif
# endif
#endif

/** 
 *  @def SDLCALL
 *  By default SDL uses the C calling convention
 */
#ifndef SDLCALL
# if defined(__WIN32__) && !defined(__GNUC__)
#  define SDLCALL __cdecl
# elif defined(__OS2__)
#  if defined (__GNUC__) && __GNUC__ < 4
#				/* Added support for GCC-EMX <v4.x */
#				/* this is needed for XFree86/OS2 developement */
#				/* F. Ambacher(anakor@snafu.de) 05.2008 */
#   define SDLCALL _cdecl
#  else
#				/* On other compilers on OS/2, we use the _System calling convention */
#				/* to be compatible with every compiler */
#   define SDLCALL _System
#  endif
# else
#  define SDLCALL
# endif
#endif // SDLCALL

#ifdef __SYMBIAN32__
#ifndef EKA2
#undef DECLSPEC
#define DECLSPEC
#elif !defined(__WINS__)
#undef DECLSPEC
#define DECLSPEC __declspec(dllexport)
#endif // !EKA2
#endif // __SYMBIAN32__

/**
 *  Force structure packing at 4 byte alignment.
 *  This is necessary if the header is included in code which has structure
 *  packing set to an alternate value, say for loading structures from disk.
 *  The packing is reset to the previous value in close_code.h 
 */
#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#ifdef _M_X64
/* Use 8-byte alignment on 64-bit architectures, so pointers are aligned */
#pragma pack(push,8)
#else
#pragma pack(push,4)
#endif
#elif (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=mac68k4byte
#pragma enumsalwaysint on
#endif // Compiler needs structure packing set

/**
 *  @def SDL_INLINE_OKAY
 *  Set up compiler-specific options for inlining functions
 */
#ifndef SDL_INLINE_OKAY
#ifdef __GNUC__
#define SDL_INLINE_OKAY
#else
/* Add any special compiler-specific cases here */
#if defined(_MSC_VER) || defined(__BORLANDC__) || \
    defined(__DMC__) || defined(__SC__) || \
    defined(__WATCOMC__) || defined(__LCC__) || \
    defined(__DECC) || defined(__EABI__)
#ifndef __inline__
#define __inline__	__inline
#endif
#define SDL_INLINE_OKAY
#else
#if !defined(__MRC__) && !defined(_SGI_SOURCE)
#ifndef __inline__
#define __inline__ inline
#endif
#define SDL_INLINE_OKAY
#endif // Not a funky compiler
#endif // Visual C++
#endif // GNU C
#endif // SDL_INLINE_OKAY

/**
 *  @def __inline__
 *  If inlining isn't supported, remove "__inline__", turning static
 *  inlined functions into static functions (resulting in code bloat
 *  in all files which include the offending header files)
 */
#ifndef SDL_INLINE_OKAY
#define __inline__
#endif

/**
 *  @def NULL
 *  Apparently this is needed by several Windows compilers
 */
#if !defined(__MACH__)
#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif // NULL
#endif // ! Mac OS X - breaks precompiled headers

/* Set up for C function definitions, even when using C++ */
#ifdef __cplusplus
extern "C"
{
#endif

/**
 *  @name SDL_Swap Functions
 *  Use inline functions for compilers that support them, and static
 *  functions for those that do not.  Because these functions become
 *  static for compilers that do not support inline functions, this
 *  header should only be included in files that actually use them.
 */
/*@{*/
#if defined(__GNUC__) && defined(__i386__) && \
   !(__GNUC__ == 2 && __GNUC_MINOR__ <= 95 /* broken gcc version */)
  static __inline__ Uint16 SDL_Swap16 (Uint16 x)
  {
  __asm__ ("xchgb %b0,%h0": "=q" (x):"0" (x));
    return x;
  }
#elif defined(__GNUC__) && defined(__x86_64__)
  static __inline__ Uint16 SDL_Swap16 (Uint16 x)
  {
  __asm__ ("xchgb %b0,%h0": "=Q" (x):"0" (x));
    return x;
  }
#elif defined(__GNUC__) && (defined(__powerpc__) || defined(__ppc__))
  static __inline__ Uint16 SDL_Swap16 (Uint16 x)
  {
    int result;

      __asm__ ("rlwimi %0,%2,8,16,23":"=&r" (result):"0" (x >> 8), "r" (x));
      return (Uint16) result;
  }
#elif defined(__GNUC__) && (defined(__m68k__) && !defined(__mcoldfire__))
  static __inline__ Uint16 SDL_Swap16 (Uint16 x)
  {
  __asm__ ("rorw #8,%0": "=d" (x): "0" (x):"cc");
    return x;
  }
#else
  static __inline__ Uint16 SDL_Swap16 (Uint16 x)
  {
    return SDL_static_cast (Uint16, ((x << 8) | (x >> 8)));
  }
#endif

#if defined(__GNUC__) && defined(__i386__) && \
   !(__GNUC__ == 2 && __GNUC_MINOR__ <= 95 /* broken gcc version */)
  static __inline__ Uint32 SDL_Swap32 (Uint32 x)
  {
  __asm__ ("bswap %0": "=r" (x):"0" (x));
    return x;
  }
#elif defined(__GNUC__) && defined(__x86_64__)
  static __inline__ Uint32 SDL_Swap32 (Uint32 x)
  {
  __asm__ ("bswapl %0": "=r" (x):"0" (x));
    return x;
  }
#elif defined(__GNUC__) && (defined(__powerpc__) || defined(__ppc__))
  static __inline__ Uint32 SDL_Swap32 (Uint32 x)
  {
    Uint32 result;

  __asm__ ("rlwimi %0,%2,24,16,23": "=&r" (result):"0" (x >> 24),
	     "r" (x));
  __asm__ ("rlwimi %0,%2,8,8,15": "=&r" (result):"0" (result), "r" (x));
  __asm__ ("rlwimi %0,%2,24,0,7": "=&r" (result):"0" (result), "r" (x));
    return result;
  }
#elif defined(__GNUC__) && (defined(__m68k__) && !defined(__mcoldfire__))
  static __inline__ Uint32 SDL_Swap32 (Uint32 x)
  {
  __asm__ ("rorw #8,%0\n\tswap %0\n\trorw #8,%0": "=d" (x): "0" (x):"cc");
    return x;
  }
#else
  static __inline__ Uint32 SDL_Swap32 (Uint32 x)
  {
    return SDL_static_cast (Uint32,
			    ((x << 24) | ((x << 8) & 0x00FF0000) |
			     ((x >> 8) & 0x0000FF00) | (x >> 24)));
  }
#endif

#ifdef SDL_HAS_64BIT_TYPE
#if defined(__GNUC__) && defined(__i386__) && \
   !(__GNUC__ == 2 && __GNUC_MINOR__ <= 95 /* broken gcc version */)
  static __inline__ Uint64 SDL_Swap64 (Uint64 x)
  {
    union
    {
      struct
      {
	Uint32 a, b;
      } s;
      Uint64 u;
    } v;
    v.u = x;
  __asm__ ("bswapl %0 ; bswapl %1 ; xchgl %0,%1": "=r" (v.s.a), "=r" (v.s.b):"0" (v.s.a),
	     "1" (v.s.
		  b));
    return v.u;
  }
#elif defined(__GNUC__) && defined(__x86_64__)
  static __inline__ Uint64 SDL_Swap64 (Uint64 x)
  {
  __asm__ ("bswapq %0": "=r" (x):"0" (x));
    return x;
  }
#else
  static __inline__ Uint64 SDL_Swap64 (Uint64 x)
  {
    Uint32 hi, lo;

    /* Separate into high and low 32-bit values and swap them */
    lo = SDL_static_cast (Uint32, x & 0xFFFFFFFF);
    x >>= 32;
    hi = SDL_static_cast (Uint32, x & 0xFFFFFFFF);
    x = SDL_Swap32 (lo);
    x <<= 32;
    x |= SDL_Swap32 (hi);
    return (x);
  }
#endif
#else
/* This is mainly to keep compilers from complaining in SDL code.
 * If there is no real 64-bit datatype, then compilers will complain about
 * the fake 64-bit datatype that SDL provides when it compiles user code.
 */
#define SDL_Swap64(X)	(X)
#endif /* SDL_HAS_64BIT_TYPE */
/*@}*/

/**
 *  @name SDL_SwapLE and SDL_SwapBE Functions
 *  Byteswap item from the specified endianness to the native endianness
 */
/*@{*/
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
#define SDL_SwapLE16(X)	(X)
#define SDL_SwapLE32(X)	(X)
#define SDL_SwapLE64(X)	(X)
#define SDL_SwapBE16(X)	SDL_Swap16(X)
#define SDL_SwapBE32(X)	SDL_Swap32(X)
#define SDL_SwapBE64(X)	SDL_Swap64(X)
#else
#define SDL_SwapLE16(X)	SDL_Swap16(X)
#define SDL_SwapLE32(X)	SDL_Swap32(X)
#define SDL_SwapLE64(X)	SDL_Swap64(X)
#define SDL_SwapBE16(X)	(X)
#define SDL_SwapBE32(X)	(X)
#define SDL_SwapBE64(X)	(X)
#endif
/*@}*/

/* Ends C function definitions when using C++ */
#ifdef __cplusplus
}
#endif

#if defined(_MSC_VER) || defined(__MWERKS__) || defined(__WATCOMC__)  || defined(__BORLANDC__)
#ifdef __BORLANDC__
#pragma nopackwarning
#endif
#if (defined(__MWERKS__) && defined(__MACOS__))
#pragma options align=reset
#pragma enumsalwaysint reset
#else
#pragma pack(pop)
#endif
#endif // Compiler needs structure packing set

#endif /* _SDL_endian_h */
