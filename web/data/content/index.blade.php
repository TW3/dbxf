@extends('_includes.base')
@section('body')

    <div class="welcome">
        <div class="wrapper">
            <h1>{{ $siteName }}</h1>
            <header>
                <span>{{ $siteDescription }}</span>
            </header>
        </div>
    </div>
	<br />
    <div class="left-side"><main>
        @markdown
![DBXF Screen Capture](./assets/images/main.PNG)

## Overview

DBXF is an acronym of **Dos Box X Fork**.

Many of the DOS era games and demos which arose from the hay day of DOS era computing are important pieces of historical artwork.
DBXF aims to be a part of the preservation of these important examples of early home computing. And present the emulation of these software programs as best as possible to the user; so that they may fully enjoy them.

### Project Goals

- Create an easy to maintain and build version of [DOSBox](dosbox.com) for Windows 10
- Make Windows 10 (64 bit) the main target for the compiled application
- Preserve [FluidSynth](http://www.fluidsynth.org) & [MT32](https://github.com/munt/munt/tree/master/mt32emu) emulation as working primary built-in features
- Properly document all aspects of the application along with it's build process
- Prioritise optimisation and dependency updates before adding any new features

### Documentation

- [Screenshots](https://gitlab.com/TW3/dbxf/blob/master/docs/Screenshots.pdf)
- [Quick Start Guide](https://gitlab.com/TW3/dbxf/blob/master/docs/Quick%20start.pdf)
- [User Guide](https://gitlab.com/TW3/dbxf/blob/master/docs/User%20guide.pdf)

        @endmarkdown
    </main></div>

@stop