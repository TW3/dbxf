<section>
    <h3>Download</h3>
    <aside>
        <span>Grab the latest version here:</span>
        <br /><br />

        <div class="popover popover-bottom">
            <form style="display: inline" action="@url('assets/dl/'){{ $appArchive }}" method="get"><button class="btn btn-success">Download DBXF <i class="icon icon-link"></i></button></form>
            <div class="popover-container">
                <div class="card bg-dark">
                    <div class="card-header">
                        <h3 class="text-light">Warning:</h3>
                    </div>
                    <div class="card-body"><span class="text-warning bg-dark">DBXF runs on 64 bit Microsoft Windows® only.</span><br /><br /><span class="text-gray">Please <a href="https://gitlab.com/TW3/dbxf/issues" class="external-link">report</a> any bugs you find. <b>Thank You!</b></span></div>
                    <div class="card-footer"><i class="icon icon-download text-gray"></i></div>
                </div>
            </div>
        </div>

        <br /><br />
        <span>DBXF is licensed under the terms of the <a href="https://gitlab.com/TW3/dbxf/blob/master/LICENSE" class="external-link">GNU General Public License v2</a></span>
        <br />
        <span>The sha256 checksum for the file {{ $appArchive }} is:</span>
        <br /><br />
        <div class="bg-dark" id="check-sum"><div class="text-bold"><span class="text-gray"><small>{{ $appChecksum }}</small></span></div></div>
        <br /><hr /><br />

        <div class="container">
            <div class="columns">
                <div class="column col-3">
		            <div class="card" id="card-support-av">
			            <div class="card-header">
				            <div class="card-title h5">Built by</div>
			            </div>
			            <div class="card-image col-mx-auto"><a href="https://ci.appveyor.com/project/TonyW/dbxf" class="external-link"><img src="https://ci.appveyor.com/api/projects/status/8gtr7x800avqj09v?retina=true" class="img-responsive width-128" alt="Appveyor Build Status" /></a></div>
			            <div class="card-footer">Appveyor</div>
		            </div>
	            </div>
            </div>
        </div>

        <br />
        <span class="text-dark"><a href="https://www.appveyor.com/" class="external-link">Appveyor</a> provide continuous integration tools for Windows® developers.</span>
        <br />
    </aside>
</section>