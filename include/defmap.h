/*

	All of the default keys in one place.
	The keys have been moved to one single file to prevent upstream patches from changing them.
	Please do not change these mappings. They are documented and set in stone.

	*** WARNING: DO NOT CHANGE THESE KEY MAPPINGS. ***


*/

#ifdef KEYMAP_MAPPER		// sdl_mapper.cpp

MAPPER_AddHandler (&MAPPER_Run, MK_f1, MMOD1, "mapper", "Mapper");

#endif

#ifdef KEYMAP_MAIN		// sdlmain.cpp

#if defined(__WIN32__) && !defined(C_SDL2)
MAPPER_AddHandler (ToggleMenu, MK_return, MMOD1 | MMOD2, "togglemenu",
		   "ToggleMenu");
#endif // WIN32
MAPPER_AddHandler (ResetSystem, MK_pause, MMOD1 | MMOD2, "reset", "Reset");	// Host+R (Host+CTRL+R acts funny on my Linux system)
MAPPER_AddHandler (KillSwitch, MK_f9, MMOD1, "shutdown", "ShutDown");
MAPPER_AddHandler (CaptureMouse, MK_f10, MMOD1, "capmouse", "Cap Mouse");
MAPPER_AddHandler (SwitchFullScreen, MK_return, MMOD2, "fullscr",
		   "Fullscreen");
MAPPER_AddHandler (Restart, MK_nothing, 0, "restart", "Restart");	// This is less useful, and now has no default binding
MAPPER_AddHandler (PasteClipboard, MK_nothing, 0, "paste", "Paste Clipboard");	//end emendelson
#if !defined(C_SDL2)
MAPPER_AddHandler (&GUI_Run, MK_nothing, 0, "gui", "ShowGUI");
MAPPER_AddHandler (&GUI_ResetResize, MK_nothing, 0, "resetsize", "ResetSize");
#endif
#if (C_DYNAMIC_X86)
MAPPER_AddHandler (CPU_ToggleDynamicCore, MK_nothing, 0, "dynamic",
		   "DynCore");
#endif
#if C_DEBUG
		// Pause binds with activate-debugger
MAPPER_AddHandler (&PauseDOSBox, MK_pause, MMOD1, "pause", "Pause");
#else
MAPPER_AddHandler (&PauseDOSBox, MK_pause, MMOD2, "pause", "Pause");
#endif

#endif

#ifdef KEYMAP_DBXF		// dosbox.cpp

MAPPER_AddHandler (DOSBOX_UnlockSpeed, MK_f12, MMOD2, "speedlock",
		   "Speedlock");
MAPPER_AddHandler (DOSBOX_UnlockSpeed2, MK_nothing, 0, "speedlock2", "Speedlock2");	// alt-F11 turbo?

#endif

#ifdef KEYMAP_CPU		// cpu.cpp

MAPPER_AddHandler (CPU_CycleDecrease, MK_f11, MMOD1, "cycledown",
		   "Dec Cycles");
MAPPER_AddHandler (CPU_CycleIncrease, MK_f12, MMOD1, "cycleup", "Inc Cycles");
MAPPER_AddHandler (CPU_ToggleAutoCycles, MK_nothing, 0, "cycauto",
		   "AutoCycles");
MAPPER_AddHandler (CPU_ToggleNormalCore, MK_nothing, 0, "normal",
		   "NormalCore");
MAPPER_AddHandler (CPU_ToggleFullCore, MK_nothing, 0, "full", "Full Core");
MAPPER_AddHandler (CPU_ToggleSimpleCore, MK_nothing, 0, "simple",
		   "SimpleCore");

#endif

#ifdef KEYMAP_DEBUG		// debug.cpp
#if defined(MACOSX)
		// OSX NOTE: ALT-F12 to launch debugger. pause maps to F16 on macOS,
		// which is not easy to input on a modern mac laptop
MAPPER_AddHandler (DEBUG_Enable, MK_f12, MMOD2, "debugger", "Debugger");
#else
MAPPER_AddHandler (DEBUG_Enable, MK_pause, MMOD2, "debugger", "Debugger");
#endif
#endif

#ifdef KEYMAP_DRIVES		// drives.cpp

	// MAPPER_AddHandler(&CycleDisk, MK_f3, MMOD1, "cycledisk", "Cycle Disk"); // CycleDisk - undeclared identifier - depreciated?
	// MAPPER_AddHandler(&CycleDrive, MK_f3, MMOD2, "cycledrive", "Cycle Drv"); // CycleDrive - undeclared identifier - depreciated?

#endif

#ifdef KEYMAP_MIDI		// midi.cpp

	// MAPPER_AddHandler(MIDI_SaveRawEvent,MK_f8,MMOD1|MMOD2,"caprawmidi","Cap MIDI");

#endif

#ifdef KEYMAP_RENDER		// render.cpp

MAPPER_AddHandler (DecreaseFrameSkip, MK_nothing, 0, "decfskip", "Dec Fskip");
MAPPER_AddHandler (IncreaseFrameSkip, MK_nothing, 0, "incfskip", "Inc Fskip");

#endif

#ifdef KEYMAP_ADLIB		// adlib.cpp

MAPPER_AddHandler (OPL_SaveRawEvent, MK_nothing, 0, "caprawopl", "Cap OPL");

#endif

#ifdef KEYMAP_HARDWARE		// hardware.cpp

MAPPER_AddHandler (CAPTURE_WaveEvent, MK_f6, MMOD1, "recwave", "Rec Wave");
MAPPER_AddHandler (CAPTURE_MTWaveEvent, MK_nothing, 0, "recmtwave",
		   "Rec MTWav");
MAPPER_AddHandler (CAPTURE_MidiEvent, MK_f8, MMOD1 | MMOD2, "caprawmidi",
		   "Cap MIDI");
#if (C_SSHOT)
MAPPER_AddHandler (CAPTURE_ScreenShotEvent, MK_f5, MMOD1, "scrshot",
		   "Screenshot");
		// MAPPER_AddHandler(CAPTURE_VideoEvent,MK_v,MMOD3|MMODHOST,"video","Video"); // Unsupported
#endif

#endif

#ifdef KEYMAP_MIXER		// mixer.cpp

MAPPER_AddHandler (MAPPER_VolumeUp, MK_kpplus, MMOD1, "volup", "VolUp");
MAPPER_AddHandler (MAPPER_VolumeDown, MK_kpminus, MMOD1, "voldown",
		   "VolDown");
MAPPER_AddHandler (MAPPER_RecVolumeUp, MK_nothing, 0, "recvolup", "RecVolUp");
MAPPER_AddHandler (MAPPER_RecVolumeDown, MK_nothing, 0, "recvoldown",
		   "RecVolDn");

#endif

#ifdef KEYMAP_BIOS		// bios.cpp

MAPPER_AddHandler (swapInNextDisk, MK_f4, MMOD1, "swapimg", "SwapFloppy");	// Originally "Swap Image" but this version does not swap CDs
MAPPER_AddHandler (swapInNextCD, MK_f3, MMOD1, "swapcd", "SwapCD");	// Variant of "Swap Image" for CDs

#endif
