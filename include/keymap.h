// Some of DOSBox-X's key-map handling
#ifndef __DOSBOX_X_KEYMAP
#define __DOSBOX_X_KEYMAP

/*
	These enumerations are meant to represent the host OS keyboard map,
	as well as the key-map used by the mapper interface.
*/
enum
{
  // #0
  DKM_US = 0,			// US keyboard layout
  DKM_UK,			// United Kingdom keyboard layout
  DKM_DEU,			// German keyboard layout
  DKM_JPN_PC98,			// Japanese PC98 keyboard layout (for PC-98 emulation)
  DKM_JPN,			// Japanese keyboard layout

  DKM_MAX
};

extern unsigned int mapper_keyboard_layout;
extern unsigned int host_keyboard_layout;
const char *DKM_to_string (const unsigned int dkm);
const char *DKM_to_descriptive_string (const unsigned int dkm);

#endif //__DOSBOX_X_KEYMAP
